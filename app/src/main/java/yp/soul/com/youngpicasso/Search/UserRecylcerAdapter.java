package yp.soul.com.youngpicasso.Search;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.api.defaults.DefaultBootstrapBrand;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.UserProfile.ProfileActivity;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper;
import yp.soul.com.youngpicasso.models.Comment;
import yp.soul.com.youngpicasso.models.Like;
import yp.soul.com.youngpicasso.models.User;

import static yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper.getInstance;

public class UserRecylcerAdapter extends RecyclerView.Adapter<UserRecylcerAdapter.ViewHolder> {


    public final SharedPreferenceHelper sharedPreferences;
    private final FirebaseHelper mFirebaseHelper;

    private List<User> mUsersList;
    private Context context;


    private List<Like> mLikeList;
    private List<Comment> comment;


    public UserRecylcerAdapter(Context context, List<User> mUsersList) {

        this.context = context;
        this.mUsersList = mUsersList;


        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(context);
        sharedPreferences = getInstance(context);

    }

    @NonNull
    @Override
    public UserRecylcerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext())
                .inflate(R.layout.layout_user_listitem, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    private String getUserTypeString(long userType, ViewHolder holder) {
        if (userType == 0) {
            holder.userType.setBootstrapBrand(DefaultBootstrapBrand.DANGER);
            return "Admin";
        }
        if (userType == 1) {
            holder.userType.setBootstrapBrand(DefaultBootstrapBrand.DANGER);
            return "Staff";
        }
        if (userType == 2) {
            holder.userType.setBootstrapBrand(DefaultBootstrapBrand.WARNING);
            return "Student";
        }
        if (userType == 3) {
            holder.userType.setBootstrapBrand(DefaultBootstrapBrand.INFO);
            return "Unregistered";
        }
        return "Unregistered";
    }

    @Override
    public void onBindViewHolder(@NonNull final UserRecylcerAdapter.ViewHolder holder, final int position) {
        final User user = mUsersList.get(holder.getAdapterPosition());


        holder.username.setText(user.getUsername());
        long userType = user.getUser_type();

        holder.userType.setText(getUserTypeString(userType, holder));

        holder.email.setText(user.getEmail());

        ImageLoader imageLoader = ImageLoader.getInstance();

        imageLoader.displayImage(user.getAvatar_img_link(),
                holder.profileImage);


    }


    @Override
    public int getItemCount() {
        if (mUsersList != null) {
            return mUsersList.size();
        }
        return 0;
    }


    class ViewHolder extends RecyclerView.ViewHolder {


        BootstrapButton userType;
        TextView username, email;
        CircleImageView profileImage;


        public ViewHolder(@NonNull final View itemView) {
            super(itemView);

            username = (TextView) itemView.findViewById(R.id.username);
            userType = itemView.findViewById(R.id.userType);
            email = (TextView) itemView.findViewById(R.id.email);
            profileImage = (CircleImageView) itemView.findViewById(R.id.profile_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        Intent intent = new Intent(context, ProfileActivity.class);
                        intent.putExtra(context.getString(R.string.activity_profile), mUsersList.get(position));
                        context.startActivity(intent);
                    }
                }
            });


        }


    }

}
