package yp.soul.com.youngpicasso.Events;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.api.defaults.DefaultBootstrapBrand;
import com.github.thunder413.datetimeutils.DateTimeUnits;
import com.github.thunder413.datetimeutils.DateTimeUtils;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.TextDataUtils;
import yp.soul.com.youngpicasso.Utils.UniversalImageLoader;
import yp.soul.com.youngpicasso.Utils.UserTypes;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.helpers.OnEventClickListener;
import yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper;
import yp.soul.com.youngpicasso.models.Comment;
import yp.soul.com.youngpicasso.models.Event;
import yp.soul.com.youngpicasso.models.Like;
import yp.soul.com.youngpicasso.models.User;

import static yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper.getInstance;

public class EventsRecyclerAdapter extends RecyclerView.Adapter<EventsRecyclerAdapter.ViewHolder> {


    public final SharedPreferenceHelper sharedPreferences;
    private final FirebaseHelper mFirebaseHelper;
    private List<Event> mEventsList;
    private Context context;

    private OnEventClickListener onEventClickListener;
    private OnItemSelectedListener itemSelectedListener;
    private List<Like> mLikeList;
    private List<Comment> comment;


    public EventsRecyclerAdapter(Context context, List<Event> mEventsList, OnEventClickListener listener,
                                 OnItemSelectedListener itemSelectedListener) {

        this.context = context;
        this.mEventsList = mEventsList;

        this.onEventClickListener = listener;
        this.itemSelectedListener = itemSelectedListener;

        mFirebaseHelper = new FirebaseHelper(context);
        sharedPreferences = getInstance(context);

    }

    @NonNull
    @Override
    public EventsRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_events, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final EventsRecyclerAdapter.ViewHolder holder, final int position) {
        final Event event = mEventsList.get(holder.getAdapterPosition());
        holder.post_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventClickListener.onEventClickListener(event);
            }
        });
        holder.relEventTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventClickListener.onEventClickListener(event);
            }
        });

        UniversalImageLoader.setImage(event.getEvent_picture(),
                holder.post_image, holder.progressBar, "");
        holder.event_title.setText(event.getEvent_title());

//        String duration_in_date = getDurationDate(event.getEvent_start_date(),
//                event.getEvent_end_date());
        int dateDiff = Math.abs(DateTimeUtils.getDateDiff(event.getEvent_start_date(),
                event.getEvent_end_date(), DateTimeUnits.DAYS));
        int diff;
        if (dateDiff == 0) {
            diff = 1;
        } else {
            diff = dateDiff + 1;
        }
        String duration_in_date = String.format("Duration:\n %s \n--\n %s" +
                        "\n For %s day(s)",
                event.getStart_time(),
                event.getEnd_time(),
                diff);

        holder.event_date.setText(duration_in_date);

        String event_days_left = TextDataUtils.getTimestampDifference2(event.getEvent_end_date());

        if (event_days_left.contains("TODAY")) {

            holder.event_days_left.setBootstrapBrand(DefaultBootstrapBrand.DANGER);
            holder.event_days_left.setText(event_days_left);
        } else if (event_days_left.contains("AGO")) {
            holder.event_days_left.setBootstrapBrand(DefaultBootstrapBrand.INFO);
            holder.event_days_left.setText(event_days_left);
        } else {

            holder.event_days_left.setBootstrapBrand(DefaultBootstrapBrand.REGULAR);
            holder.event_days_left.setText(event_days_left);
        }


        prepareLikes(holder, position, event);
        prepareComments(holder, position, event);
    }

    private void prepareComments(ViewHolder holder, final int position, final Event event) {
        //prepare comments count
        comment = event.getComment_list();

        if (comment.size() != 0) {
            int comment_size = comment.size();
            holder.tv_comment_count.setText(String.valueOf(comment_size));

        } else {
            holder.tv_comment_count.setText("0");

        }


        //handle click events
        holder.relComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onEventClickListener.onEventClickListener(event);
            }
        });


    }

    private void prepareLikes(final ViewHolder holder, final int position, final Event event) {
        //check likes and prepare likes string
        Boolean mLikedByCurrentUser = false;
        StringBuilder mUsersName = new StringBuilder();
        mLikeList = event.getlikeList();
        //check if there are no likes for that event
        if (mLikeList.size() != 0) {

            holder.tv_likes_count.setText(String.format("%d", event.getlikeList().size()));
            for (Like like :
                    event.getlikeList()) {
                if (mFirebaseHelper.getAuth().getCurrentUser().getUid().equals(like.getUser_id())) {
                    mLikedByCurrentUser = true;
                }
            }
            if (mLikedByCurrentUser) {
                holder.image_heart_red.setVisibility(View.VISIBLE);
                holder.image_heart.setVisibility(View.GONE);
                holder.tv_likes_count.setTextColor(ContextCompat.getColor(context, R.color.red_normal));
            } else {
                holder.image_heart_red.setVisibility(View.GONE);
                holder.image_heart.setVisibility(View.VISIBLE);
                holder.tv_likes_count.setTextColor(ContextCompat.getColor(context, R.color.black));
            }

        } else {
            mLikeList = new ArrayList<>();
            holder.tv_likes_count.setText("0");
            holder.image_heart_red.setVisibility(View.GONE);
            holder.image_heart.setVisibility(View.VISIBLE);
        }


        // handle likes on click events

        holder.image_heart_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //database access and if complete then toggle red heart to normal
                holder.image_heart_red.setClickable(false);
                holder.image_heart.setClickable(false);

                Like userLike = new Like();
                for (Like like :
                        event.getlikeList()) {
                    if (like.getUser_id().equals(mFirebaseHelper.getAuth().getCurrentUser().getUid())) {
                        userLike = like;
                        break;
                    }
                }
                final Like usertemp = userLike;
                mFirebaseHelper.getMyRef().child(context.getString(R.string.db_events))
                        .child(event.getEvent_id())
                        .child(context.getString(R.string.db_field_likes))
                        .child(usertemp.getLike_id())
                        .removeValue(new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                holder.image_heart.setClickable(true);
                                holder.image_heart_red.setVisibility(View.GONE);
                                holder.image_heart.setVisibility(View.VISIBLE);
                                event.getlikeList().remove(usertemp);
                                holder.tv_likes_count.setTextColor(ContextCompat.getColor(context, R.color.black));
                                holder.tv_likes_count.setText(String.format("%d",
                                        event.getlikeList().size()));

                                StringBuilder mUsersName = new StringBuilder();
                                for (Like like :
                                        event.getlikeList()) {
                                    mUsersName.append(like.getUsername());
                                    mUsersName.append(",");
                                }
                            }
                        });

            }
        });

        holder.image_heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //database access and if complete then toggle normal heart to red
                holder.image_heart_red.setClickable(false);
                holder.image_heart.setClickable(false);
                String keyId = mFirebaseHelper.getMyRef().push().getKey();
                User user = sharedPreferences.getUserInfo();
                final Like like = new Like(user.getUser_id(),
                        user.getUsername(),
                        keyId
                );

                mFirebaseHelper.getMyRef().child(context.getString(R.string.db_events))
                        .child(event.getEvent_id())
                        .child(context.getString(R.string.db_field_likes))
                        .child(keyId)
                        .setValue(like, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                event.getlikeList().add(like);
                                holder.image_heart_red.setVisibility(View.VISIBLE);
                                holder.image_heart.setVisibility(View.GONE);
                                holder.tv_likes_count.setTextColor(ContextCompat.getColor(context, R.color.red_normal));
                                holder.tv_likes_count.setText(String.format("%d",
                                        event.getlikeList().size()));

                                StringBuilder mUsersName = new StringBuilder();
                                for (Like like :
                                        event.getlikeList()) {
                                    mUsersName.append(like.getUsername());
                                    mUsersName.append(",");
                                }

                                holder.image_heart_red.setClickable(true);

                            }
                        });

            }
        });

    }

    private String getDurationDate(String event_start_date, String event_end_date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String dateDuration = "";
        try {
            Date start_date = sdf.parse(event_start_date);
            Date end_date = sdf.parse(event_end_date);
            dateDuration = String.format("FROM: %s TO: %s", start_date.toString(), end_date.toString());
        } catch (ParseException e) {
            Toast.makeText(context, "Date parsing went wrong.", Toast.LENGTH_SHORT).show();
        }
        return dateDuration;
    }

    @Override
    public int getItemCount() {
        if (mEventsList != null) {
            return mEventsList.size();
        }
        return 0;
    }

    public interface OnItemSelectedListener {

        void onMenuAction(Event event, MenuItem item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout relEventTitle, relComment;

        TextView event_title, event_date;
        ImageView post_image, speech_bubble;
        TextView tv_likes_count, tv_comment_count;
        ImageView btn_more, image_heart_red, image_heart;
        TextView image_likes;
        BootstrapButton event_days_left;
        ProgressBar progressBar;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);

            progressBar = itemView.findViewById(R.id.progressBar);
            relEventTitle = itemView.findViewById(R.id.relLayout1);
            relComment = itemView.findViewById(R.id.relComment);

            btn_more = itemView.findViewById(R.id.btn_more);
            event_title = itemView.findViewById(R.id.event_title);
            post_image = itemView.findViewById(R.id.post_image);


            image_heart_red = itemView.findViewById(R.id.image_heart_red);
            image_heart = itemView.findViewById(R.id.image_heart);
            tv_likes_count = itemView.findViewById(R.id.tv_likes_count);
            speech_bubble = itemView.findViewById(R.id.speech_bubble);
            tv_comment_count = itemView.findViewById(R.id.tv_comment_count);

            image_likes = itemView.findViewById(R.id.image_likes);
            event_date = itemView.findViewById(R.id.event_date);
            event_days_left = itemView.findViewById(R.id.event_days_left);


            if (sharedPreferences.getUserType() == UserTypes.ADMIN) {
                btn_more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopupMenu popup = new PopupMenu(btn_more.getContext(), btn_more);
                        popup.inflate(R.menu.news_options_menu);
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                Event object = mEventsList.get(getAdapterPosition());
                                itemSelectedListener.onMenuAction(object, item);

                                return false;
                            }
                        });
                        // here you can inflate your menu

                        popup.show();
                    }
                });

            } else {
                btn_more.setVisibility(View.GONE);
            }
        }


    }
}
