package yp.soul.com.youngpicasso.Gallery;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.github.thunder413.datetimeutils.DateTimeUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import id.zelory.compressor.Compressor;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.FilePaths;
import yp.soul.com.youngpicasso.Utils.Permissions;
import yp.soul.com.youngpicasso.Utils.RotateBitmap;
import yp.soul.com.youngpicasso.Utils.TextDataUtils;
import yp.soul.com.youngpicasso.Utils.UniversalImageLoader;
import yp.soul.com.youngpicasso.Utils.UserTypes;
import yp.soul.com.youngpicasso.Utils.ValidationUtil;
import yp.soul.com.youngpicasso.Utils.VerifyPermissions;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.models.ArtPost;
import yp.soul.com.youngpicasso.models.User;

public class ArtPostAddActivity extends AppCompatActivity {
    public static final String TEMP_IMAGE = "https://firebasestorage.googleapis.com/v0/b/young-picasso.appspot.com/o/Event%2F10636111_317544761750405_4932389066159121159_n.jpg?alt=media&token=7d11e9d5-de97-42e6-94a3-e934a1ba8a12";
    private static final String TAG = "EventAddActivity";
    private static final String ADD_POST = "Add Post";
    public static final int MAX_FILE_SIZE_IN_BYTES = 10485760;
    private Context mContext = ArtPostAddActivity.this;
    private EditText titleEditText, descriptionEditText;
    private String event_title, event_desc;
    private FirebaseHelper mFirebaseHelper;


    private boolean isEditPost = false;
    private ArtPost artPost;
    private String keyId;
    private ImageView imageView;
    private BootstrapButton btn_post;
    private Uri imageUri;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_art_post_add);

        setupFirebase();
        setupToolbar();
        setupWidgets();

        checkCallingIntent();

        initGetImageFromGallery();
        initPostEvent();


    }

    private void checkCallingIntent() {
        Intent in = getIntent();
        if (in.hasExtra(mContext.getString(R.string.calling_art_gallery_intent))) {

            artPost = in.getParcelableExtra(mContext.getString(R.string.calling_art_gallery_intent));
            titleEditText.setText(artPost.getTitle());
            descriptionEditText.setText(artPost.getDescription());


            UniversalImageLoader.setImage(artPost.getImagePath(), imageView, null, "");

            isEditPost = true;
        }
    }

    private void setupFirebase() {
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
    }


    private void initGetImageFromGallery() {
        imageView.setOnClickListener(v -> {
            VerifyPermissions verifyPermissions = new VerifyPermissions(mContext, ArtPostAddActivity.this);
            if (verifyPermissions.checkPermissionsArray(Permissions.PERMISSIONS)) {
                Log.d(TAG, "onClick: permission granted");
                CropImage.startPickImageActivity(ArtPostAddActivity.this);
            } else {
                Log.d(TAG, "onClick: permission not granted, verifying..");
                verifyPermissions.verifyPermissionsArray(Permissions.PERMISSIONS);
//                CropImage.startPickImageActivity(ArtPostAddActivity.this);
            }

        });

    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

//        if (resultCode == RESULT_OK) {
//            Uri targetUri = data.getData();
//            event_picture = targetUri.toString();
//            try {
//                Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(targetUri));
//                imageView.setImageBitmap(bitmap);
//            } catch (Exception e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//        }

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            if (isImageFileValid(imageUri)) {
                this.imageUri = imageUri;
            }

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            } else {
                // no permissions required or already grunted
                onImagePikedAction();
            }
        }
    }


    private void onImagePikedAction() {
        //load image from uri to imageview
//        UniversalImageLoader.setImage(imageUri.toString(), imageView, null, "");
        Bitmap bitmap = null;
        RotateBitmap rotateBitmap = new RotateBitmap();
        try {
            bitmap = rotateBitmap.HandleSamplingAndRotationBitmap(this, imageUri);
        } catch (IOException e) {
            e.printStackTrace();
        }

        imageView.setImageBitmap(bitmap);
    }


    public boolean isImageFileValid(Uri imageUri) {
        int message = R.string.error_general;
        boolean result = false;

        if (imageUri != null) {
            if (ValidationUtil.isImage(imageUri, mContext)) {
                File imageFile = new File(imageUri.getPath());
                if (imageFile.length() > MAX_FILE_SIZE_IN_BYTES) {
                    message = R.string.error_bigger_file;
                } else {
                    result = true;
                }
            } else {
                message = R.string.error_incorrect_file_type;
            }
        }

        if (!result) {
            int finalMessage = message;
            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();

        }

        return result;
    }


    private void initPostEvent() {
        btn_post.setOnClickListener(view -> {
            event_title = titleEditText.getText().toString();
            event_desc = descriptionEditText.getText().toString();
            if (validateForm()) {
                showProgress();
                saveEventPost(imageUri.toString());
            } else {
                Toast.makeText(mContext, "Some fields are not set correctly.", Toast.LENGTH_SHORT).show();
            }

        });
    }

    private void saveEventPost(final String imageUrl) {
        //check if user is allowed to post
        mFirebaseHelper.getMyRef().child("users")
                .child(mFirebaseHelper.getAuth().getCurrentUser().getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            long userType = dataSnapshot.getValue(User.class).getUser_type();
                            if (userType != UserTypes.NORMAL) {
                                postEventToDB();
                            }

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        hideProgress();
                        Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();
                    }
                });


    }

    private void postEventToDB() {
        //check if it is new or old
        if (isEditPost) {
            keyId = artPost.getId();
        } else {
            keyId = mFirebaseHelper.getMyRef().push().getKey();
        }
        ArtPost artPost = new ArtPost(keyId,
                event_title,
                event_desc,
                TEMP_IMAGE,
                mFirebaseHelper.getAuth().getCurrentUser().getUid(),
                mFirebaseHelper.getSharedPreference().getUserInfo().getUsername(),
                mFirebaseHelper.getSharedPreference().getUserType() == 0 ? UserTypes.ADMIN : UserTypes.STUDENT);
        //check if contains image is checked if not checked then put temp image url

        final String imageTitle = "image" + keyId + DateTimeUtils.formatDate(new Date());
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024) // 50 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app
                .build();
        imageLoader.init(config);
        Bitmap actualImage = imageLoader.loadImageSync(imageUri.toString());
        File file = new File(imageUri.getPath());
        UploadTask uploadTask = null;
        try {
            File compressedImageFile = new Compressor(this).compressToFile(file);
            Uri uri = Uri.fromFile(compressedImageFile);

            uploadTask = mFirebaseHelper.uploadImage(uri, imageTitle);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (uploadTask != null) {
            uploadTask.addOnFailureListener(exception -> {
                // Handle unsuccessful uploads
                Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();

            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                    progressDialog.setMessage("Creating post...(" + progress + " % complete)");

                }
            }).addOnSuccessListener(taskSnapshot -> {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.

                mFirebaseHelper.getmStorageReference().child(FilePaths.FIREBASE_ART_STORAGE + imageTitle)
                        .getDownloadUrl().addOnSuccessListener(uri -> {
                    artPost.setImagePath(String.valueOf(uri));
                    artPost.setImageTitle(imageTitle);
                    Log.d(TAG, "successful upload image, image url: " + String.valueOf(uri));
                    hideProgress();
                    mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_art_posts))
                            .child(keyId)
                            .setValue(artPost, (databaseError, databaseReference) -> {
                                mFirebaseHelper.increasePostCount();
                                Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
                                finish();
                            });
                });


            });
        }

    }

    private boolean validateForm() {
        boolean valid = true;

        if (TextDataUtils.isEmpty(event_title)) {
            valid = false;
        }


        if (TextDataUtils.isEmpty(event_desc)) {
            valid = false;
        }

        if (imageView.getDrawable() == null) {

            valid = false;
        }


        return valid;
    }

    public void showProgress() {
        hideProgress();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Creating post..");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void setupWidgets() {
        titleEditText = findViewById(R.id.titleEditText);
        descriptionEditText = findViewById(R.id.descriptionEditText);

        imageView = findViewById(R.id.imageView);


        btn_post = findViewById(R.id.btn_post);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(ADD_POST);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

}
