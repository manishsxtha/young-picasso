package yp.soul.com.youngpicasso.Notice;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.pavlospt.roundedletterview.RoundedLetterView;

import java.util.List;

import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.UserTypes;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.helpers.OnNoticeClickListener;
import yp.soul.com.youngpicasso.models.Notice;

public class NoticesRecyclerAdapter extends RecyclerView.Adapter<NoticesRecyclerAdapter.ViewHolder> {


    private final FirebaseHelper mFirebaseHelper;
    private NoticeFragment.OnNoticeMenuClickListener noticeMenuClickListener;
    private List<Notice> mNotices;
    private Context context;

    private OnNoticeClickListener onNoticeClickListener;

    public NoticesRecyclerAdapter(Context context, List<Notice> mNotices, OnNoticeClickListener listener
            , NoticeFragment.OnNoticeMenuClickListener noticeMenuClickListener) {

        this.context = context;
        this.mNotices = mNotices;
        this.onNoticeClickListener = listener;
        this.noticeMenuClickListener = noticeMenuClickListener;
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(context);

    }

    @NonNull
    @Override
    public NoticesRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_notices, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final NoticesRecyclerAdapter.ViewHolder holder, final int position) {


        holder.rlv_name_view.setTitleText(mNotices.get(position).getNotice_title().substring(0, 1));

        holder.rlv_name_view.setBackgroundColor(getRandomMaterialColor("400"));

        holder.tv_notice_title.setText(mNotices.get(position).getNotice_title());
        holder.tv_notice_date.setText(mNotices.get(position).getNotice_date());
        holder.tv_notice_desc.setText(mNotices.get(position).getNotice_desc());
    }

    @Override
    public int getItemCount() {
        if (mNotices != null) {
            return mNotices.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CardView mCardView;
        RoundedLetterView rlv_name_view;
        private TextView tv_notice_title, tv_notice_date, tv_notice_desc;
        ImageView btn_more;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mCardView = itemView.findViewById(R.id.cardview);

            rlv_name_view = itemView.findViewById(R.id.rlv_name_view);
            tv_notice_title = itemView.findViewById(R.id.notice_title);
            tv_notice_date = itemView.findViewById(R.id.notice_date);
            tv_notice_desc = itemView.findViewById(R.id.notice_desc);

            btn_more = itemView.findViewById(R.id.btn_more);

            if (mFirebaseHelper.getSharedPreference().getUserInfo().getUser_type() != UserTypes.ADMIN) {
                btn_more.setVisibility(View.GONE);
            } else {
                btn_more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopupMenu popup = new PopupMenu(btn_more.getContext(), btn_more);
                        popup.inflate(R.menu.news_options_menu);
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                Notice object = mNotices.get(getAdapterPosition());
                                noticeMenuClickListener.onMenuClick(object, item, getAdapterPosition());

                                return false;
                            }
                        });
                        // here you can inflate your menu

                        popup.show();
                    }
                });
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onNoticeClickListener.onNoticeClickListener(mNotices.get(getAdapterPosition()));
                }
            });


        }

    }

    private int getRandomMaterialColor(String typeColor) {
        int returnColor = Color.GRAY;
        int arrayId = context.getResources().getIdentifier("mdcolor_" + typeColor,
                "array", context.getPackageName());

        if (arrayId != 0) {
            TypedArray colors = context.getResources().obtainTypedArray(arrayId);
            int index = (int) (Math.random() * colors.length());
            returnColor = colors.getColor(index, Color.GRAY);
            colors.recycle();
        }
        return returnColor;
    }
}
