package yp.soul.com.youngpicasso.Gallery;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.thunder413.datetimeutils.DateTimeUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;
import yp.soul.com.youngpicasso.Gallery.Detail.ArtPostDetailActivity;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.AdminUID;
import yp.soul.com.youngpicasso.Utils.UserTypes;
import yp.soul.com.youngpicasso.helpers.BaseFragment;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.models.ArtPost;
import yp.soul.com.youngpicasso.models.Comment;
import yp.soul.com.youngpicasso.models.Like;


public class FreshArtPostFragment extends BaseFragment {


    private static final String TAG = "UpcomingEventsFragment";


    private View view;
    private Context mContext;

    private List<ArtPost> mArtPostList;

    private ValueEventListener eventListener;
    private RecyclerView recyclerView;

    private WaveSwipeRefreshLayout refreshLayout;

    private LinearLayoutManager manager;

    private FirebaseHelper mFirebaseHelper;
    private ArtPostsRecyclerAdapter adapter;


    public FreshArtPostFragment() {
        super();
        setArguments(new Bundle());
    }

    public static Fragment newInstance() {
        FreshArtPostFragment fragment = new FreshArtPostFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recycler_view_container, container, false);
        mContext = getContext();

        setupFirebase();
        setupWidgets();

        setupAdapter();
        setupArtPostList();

//        setupTempData();
        return view;
    }


    private void setupTempData() {
        String keyId = mFirebaseHelper.getMyRef().push().getKey();


        ArtPost artPost = new ArtPost();
        List<Like> like = new ArrayList<>();
        List<Comment> comments = new ArrayList<>();
        artPost.setComment_list(comments);
        artPost.setCreatedDate(DateTimeUtils.formatDate(new Date()));
        artPost.setDescription("desc");
        artPost.setHasComplain(false);
        artPost.setId(keyId);
        artPost.setImagePath(AdminUID.TEMP_IMAGE_URL);
        artPost.setTitle(mContext.getString(R.string.sample_title) + " 1");
        artPost.setUser_id("9LO2xwZckWVRKYeC8HwcO9bSOma2");
        artPost.setUsername("Manish");
        artPost.setWatchersCount(1);
        artPost.setLikeList(like);
        artPost.setItemType(UserTypes.ADMIN);
        artPost.setDescription(mContext.getString(R.string.sample_description));

        mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_art_posts))
                .child(keyId)
                .setValue(artPost);

        mArtPostList.add(artPost);

    }


    private void setupWidgets() {

        refreshLayout = view.findViewById(R.id.refresh);
        refreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);
        refreshLayout.setWaveColor(Color.argb(100, 255, 0, 0));

        refreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Do work to refresh the list here.

                mArtPostList.clear();
                setupArtPostList();
                refreshLayout.setWaveColor(0xFF000000 + new Random().nextInt(0xFFFFFF));
            }
        });
    }

    private void setupArtPostList() {

        mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_art_posts))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        mArtPostList.clear();

                        for (DataSnapshot ds :
                                dataSnapshot.getChildren()) {
                            ArtPost artPost = new ArtPost();
                            Map<String, Object> objectMap = (HashMap<String, Object>) ds.getValue();

                            Objects.requireNonNull(objectMap);
                            if ((Long) objectMap.get("itemType") != UserTypes.STUDENT) {
                                continue;
                            }
                            artPost.setId(objectMap.get("id").toString());
                            artPost.setTitle(objectMap.get("title").toString());
                            artPost.setCreatedDate(objectMap.get("createdDate").toString());
                            artPost.setHasComplain((Boolean) objectMap.get("hasComplain"));
                            artPost.setImagePath(objectMap.get("imagePath").toString());
                            artPost.setImageTitle(objectMap.get("imageTitle").toString());
                            artPost.setUser_id(objectMap.get("user_id").toString());
                            artPost.setUsername(objectMap.get("username").toString());
                            artPost.setWatchersCount((Long) objectMap.get("watchersCount"));
                            artPost.setDescription(objectMap.get("description").toString());
                            artPost.setItemType((Long) objectMap.get("itemType"));


                            List<Comment> comments = new ArrayList<>();
                            for (DataSnapshot sss :
                                    ds.child("comment_list").getChildren()) {
                                Comment comment = new Comment();
                                Map<String, Object> objectMap1 = (HashMap<String, Object>) sss.getValue();
                                comment.setComment_id(objectMap1.get("comment_id").toString());
                                comment.setComment_desc(objectMap1.get("comment_desc").toString());
                                comment.setDate_created(objectMap1.get("date_created").toString());
                                comment.setUser_id(objectMap1.get("user_id").toString());
                                comment.setUsername(objectMap1.get("username").toString());

                                comments.add(comment);

                            }
                            artPost.setComment_list(comments);

                            List<Like> likes = new ArrayList<>();
                            for (DataSnapshot likesSnap :
                                    ds.child("mUserLikes").getChildren()) {
                                Like like = new Like();
                                Map<String, Object> objectMap1 = (HashMap<String, Object>) likesSnap.getValue();
                                like.setUser_id(objectMap1.get("user_id").toString());
                                like.setLike_id(objectMap1.get("like_id").toString());
                                like.setUsername(objectMap1.get("username").toString());

                                likes.add(like);

                            }
                            artPost.setLikeList(likes);
                            mArtPostList.add(artPost);


                        }
                        Collections.sort(mArtPostList, new Comparator<ArtPost>() {
                            @Override
                            public int compare(ArtPost o1, ArtPost o2) {
                                return o2.getCreatedDate().compareTo(o1.getCreatedDate());
                            }
                        });
                        adapter.notifyDataSetChanged();
//                        manager.scrollToPosition(mArtPostList.size() - 1);
                        if (refreshLayout.isRefreshing()) {

                            refreshLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void popupForDelete(final ArtPost artPost) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialog);
        builder.setTitle("Delete news " + artPost.getTitle() + " ?");
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mFirebaseHelper.getMyRef().child("events")
                        .child(artPost.getId())
                        .removeValue(new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                mArtPostList.remove(artPost);
                                mFirebaseHelper.decreasePostsCount();
                                adapter.notifyDataSetChanged();
                                Toast.makeText(mContext, "Deleted news: " + artPost.getTitle(), Toast.LENGTH_SHORT).show();
                            }
                        });

                dialog.cancel();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }


    private void setupAdapter() {
        mArtPostList = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
        manager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);


        recyclerView.setLayoutManager(manager);

        adapter = new ArtPostsRecyclerAdapter(mContext, mArtPostList, new ArtPostsRecyclerAdapter.OnArtPostClickListener() {
            @Override
            public void onArtPostClickListener(ArtPost artPost) {
                Intent intent = new Intent(mContext, ArtPostDetailActivity.class);
                intent.putExtra(mContext.getString(R.string.calling_art_gallery_intent), artPost);
                startActivity(intent);
            }
        }, new ArtPostsRecyclerAdapter.OnItemSelectedListener() {
            @Override
            public void onMenuAction(ArtPost artPost, MenuItem item) {
                Log.e(TAG, "onMenuAction: event:" + artPost);
                switch (item.getItemId()) {
                    case R.id.menu_edit:
                        Intent intent = new Intent(mContext, ArtPostAddActivity.class);
                        intent.putExtra(mContext.getString(R.string.calling_event_intent), artPost);
                        startActivity(intent);
                        break;
                    case R.id.menu_delete:
                        popupForDelete(artPost);


//                setupObjectList();
                        break;
                }
            }
        });

        recyclerView.setAdapter(adapter);


    }


    private void setupFirebase() {
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
    }

    @Override
    public String getTitle() {
        return "Upcoming Events";
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        mArtPostList.clear();
    }
}
