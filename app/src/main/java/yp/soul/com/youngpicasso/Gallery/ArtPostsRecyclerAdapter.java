package yp.soul.com.youngpicasso.Gallery;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.thunder413.datetimeutils.DateTimeStyle;
import com.github.thunder413.datetimeutils.DateTimeUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.UniversalImageLoader;
import yp.soul.com.youngpicasso.Utils.UserTypes;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.helpers.OnEventClickListener;
import yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper;
import yp.soul.com.youngpicasso.models.ArtPost;
import yp.soul.com.youngpicasso.models.Comment;
import yp.soul.com.youngpicasso.models.Like;
import yp.soul.com.youngpicasso.models.User;

import static yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper.getInstance;

public class ArtPostsRecyclerAdapter extends RecyclerView.Adapter<ArtPostsRecyclerAdapter.ViewHolder> {


    public final SharedPreferenceHelper sharedPreferences;
    private final FirebaseHelper mFirebaseHelper;
    private OnArtPostClickListener onClickListener;

    private List<ArtPost> mArtPostList;
    private Context context;

    private OnEventClickListener onEventClickListener;
    private OnItemSelectedListener itemSelectedListener;
    private List<Like> mLikeList;
    private List<Comment> comment;


    public ArtPostsRecyclerAdapter(Context context, List<ArtPost> mArtPostList, OnArtPostClickListener listener,
                                   OnItemSelectedListener itemSelectedListener) {

        this.context = context;
        this.mArtPostList = mArtPostList;

        this.onClickListener = listener;
        this.itemSelectedListener = itemSelectedListener;

        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(context);
        sharedPreferences = getInstance(context);

    }

    @NonNull
    @Override
    public ArtPostsRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_art_post, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ArtPostsRecyclerAdapter.ViewHolder holder, final int position) {
        final ArtPost artPost = mArtPostList.get(holder.getAdapterPosition());
//        holder.post_image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onEventClickListener.onEventClickListener(event);
//            }
//        });
//        holder.relEventTitle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onEventClickListener.onEventClickListener(event);
//            }
//        });

        UniversalImageLoader.setImage(artPost.getImagePath(),
                holder.postImageView, holder.progressBar, "");

        holder.titleTextView.setText(artPost.getTitle());
        holder.detailsTextView.setText(artPost.getDescription());
        holder.dateTextView.setText(DateTimeUtils.formatWithStyle(artPost.getCreatedDate(), DateTimeStyle.AGO_FULL_STRING));
        holder.watcherCounterTextView.setText(String.valueOf(artPost.getWatchersCount()));


        getAuthorImage(holder.authorImageView, artPost.getUser_id());
        prepareLikes(holder, position, artPost);
        prepareComments(holder, position, artPost);

    }

    private void getAuthorImage(final ImageView authorImageView, String user_id) {
        mFirebaseHelper.getMyRef().child("users")
                .child(user_id)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            User user = dataSnapshot.getValue(User.class);
                            UniversalImageLoader.setImage(user.getAvatar_img_link(),
                                    authorImageView, null, "");


                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void prepareComments(ViewHolder holder, final int position, final ArtPost event) {
        //prepare comments count
        comment = event.getComment_list();

        if (comment.size() != 0) {
            int comment_size = comment.size();
            holder.commentsCountTextView.setText(String.valueOf(comment_size));

        } else {
            holder.commentsCountTextView.setText("0");

        }


    }

    private void prepareLikes(final ViewHolder holder, final int position, final ArtPost artPost) {
        //check likes and prepare likes string
        Boolean mLikedByCurrentUser = false;
        StringBuilder mUsersName = new StringBuilder();
        mLikeList = artPost.getLikeList();
        //check if there are no likes for that event
        if (mLikeList.size() != 0) {

            holder.likeCounterTextView.setText(String.format("%d", artPost.getLikeList().size()));
            for (Like like :
                    artPost.getLikeList()) {
                mUsersName.append(like.getUsername());
                mUsersName.append(",");
                if (mFirebaseHelper.getAuth().getCurrentUser().getUid().equals(like.getUser_id())) {
                    mLikedByCurrentUser = true;
                }
            }
            if (mLikedByCurrentUser) {
                holder.likesImageView.setVisibility(View.VISIBLE);
                holder.likesOffImageView.setVisibility(View.GONE);
                holder.likeCounterTextView.setTextColor(ContextCompat.getColor(context, R.color.red_normal));
            } else {
                holder.likesImageView.setVisibility(View.GONE);
                holder.likesOffImageView.setVisibility(View.VISIBLE);
                holder.likeCounterTextView.setTextColor(ContextCompat.getColor(context, R.color.black));
            }
            holder.text_image_likes.setText(getLikesString(mUsersName));
        } else {
            mLikeList = new ArrayList<>();
            holder.likeCounterTextView.setText("0");
            holder.likesImageView.setVisibility(View.GONE);
            holder.likesOffImageView.setVisibility(View.VISIBLE);
            holder.text_image_likes.setText(getLikesString(mUsersName));
        }


        // handle likes on click events

        holder.likeViewGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //database access and if complete then toggle red heart to normal
                if (holder.likesImageView.getVisibility() == View.VISIBLE) {
                    holder.likesImageView.setClickable(false);
                    holder.likesOffImageView.setClickable(false);

                    Like userLike = new Like();
                    for (Like like :
                            artPost.getLikeList()) {
                        if (like.getUser_id().equals(mFirebaseHelper.getAuth().getCurrentUser().getUid())) {
                            userLike = like;
                            break;
                        }
                    }
                    final Like usertemp = userLike;
                    mFirebaseHelper.getMyRef().child(context.getString(R.string.db_art_posts))
                            .child(artPost.getId())
                            .child(context.getString(R.string.db_field_likes))
                            .child(usertemp.getLike_id())
                            .removeValue(new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                    holder.likesOffImageView.setClickable(true);
                                    holder.likesImageView.setVisibility(View.GONE);
                                    holder.likesOffImageView.setVisibility(View.VISIBLE);
                                    artPost.getLikeList().remove(usertemp);
                                    holder.likeCounterTextView.setTextColor(ContextCompat.getColor(context, R.color.black));
                                    holder.likeCounterTextView.setText(String.format("%d",
                                            artPost.getLikeList().size()));

                                    StringBuilder mUsersName = new StringBuilder();
                                    for (Like like :
                                            artPost.getLikeList()) {
                                        mUsersName.append(like.getUsername());
                                        mUsersName.append(",");
                                    }
                                    holder.text_image_likes.setText(getLikesString(mUsersName));
                                }
                            });

                } else {
                    holder.likesImageView.setClickable(false);
                    holder.likesOffImageView.setClickable(false);
                    String keyId = mFirebaseHelper.getMyRef().push().getKey();
                    User user = sharedPreferences.getUserInfo();
                    final Like like = new Like(user.getUser_id(),
                            user.getUsername(),
                            keyId
                    );

                    mFirebaseHelper.getMyRef().child(context.getString(R.string.db_art_posts))
                            .child(artPost.getId())
                            .child(context.getString(R.string.db_field_likes))
                            .child(keyId)
                            .setValue(like, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                    artPost.getLikeList().add(like);
                                    holder.likesImageView.setVisibility(View.VISIBLE);
                                    holder.likesOffImageView.setVisibility(View.GONE);
                                    holder.likeCounterTextView.setTextColor(ContextCompat.getColor(context, R.color.red_normal));
                                    holder.likeCounterTextView.setText(String.format("%d",
                                            artPost.getLikeList().size()));

                                    StringBuilder mUsersName = new StringBuilder();
                                    for (Like like :
                                            artPost.getLikeList()) {
                                        mUsersName.append(like.getUsername());
                                        mUsersName.append(",");
                                    }
                                    holder.text_image_likes.setText(getLikesString(mUsersName));
                                    holder.likesImageView.setClickable(true);

                                }
                            });
                }

            }
        });

//        holder.likesOffImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //database access and if complete then toggle normal heart to red
//
//
//            }
//        });

    }

    private String getLikesString(StringBuilder mUsersName) {
        if (mUsersName.toString().isEmpty()) {
            return context.getString(R.string.no_comment_sample);
        }
        String[] splitUsers = mUsersName.toString().split(",");
        String mLikesString = "";
        int length = splitUsers.length;
        if (length == 0) {
            mLikesString = context.getString(R.string.no_comment_sample);
        } else if (length == 1) {
            mLikesString = "Liked by " + splitUsers[0];
        } else if (length == 2) {
            mLikesString = "Liked by " + splitUsers[0]
                    + " and " + splitUsers[1];
        } else if (length == 3) {
            mLikesString = "Liked by " + splitUsers[0]
                    + ", " + splitUsers[1]
                    + " and " + splitUsers[2];

        } else if (length == 4) {
            mLikesString = "Liked by " + splitUsers[0]
                    + ", " + splitUsers[1]
                    + ", " + splitUsers[2]
                    + " and " + splitUsers[3];
        } else if (length > 4) {
            mLikesString = "Liked by " + splitUsers[0]
                    + ", " + splitUsers[1]
                    + ", " + splitUsers[2]
                    + " and " + (splitUsers.length - 3) + " others";
        }
        return mLikesString;
    }

    @Override
    public int getItemCount() {
        if (mArtPostList != null) {
            return mArtPostList.size();
        }
        return 0;
    }

    public interface OnItemSelectedListener {

        void onMenuAction(ArtPost artPost, MenuItem item);
    }

    public interface OnArtPostClickListener {
        void onArtPostClickListener(ArtPost artPost);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView postImageView;
        private TextView titleTextView;
        private TextView detailsTextView;
        private TextView likeCounterTextView;
        private ImageView likesImageView;
        private ImageView likesOffImageView;
        private TextView commentsCountTextView;
        private TextView watcherCounterTextView;
        private TextView dateTextView;
        private TextView text_image_likes;
        private ImageView authorImageView;
        private ViewGroup likeViewGroup;
        private ProgressBar progressBar;

        private ImageView btn_more;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);

            progressBar = itemView.findViewById(R.id.progressBar);
            btn_more = itemView.findViewById(R.id.btn_more);

            postImageView = itemView.findViewById(R.id.postImageView);
            likeCounterTextView = itemView.findViewById(R.id.likeCounterTextView);
            likesImageView = itemView.findViewById(R.id.likesImageView);
            likesOffImageView = itemView.findViewById(R.id.likesOffImageView);
            commentsCountTextView = itemView.findViewById(R.id.commentsCountTextView);
            watcherCounterTextView = itemView.findViewById(R.id.watcherCounterTextView);
            dateTextView = itemView.findViewById(R.id.dateTextView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            detailsTextView = itemView.findViewById(R.id.detailsTextView);
            authorImageView = itemView.findViewById(R.id.authorImageView);
            likeViewGroup = itemView.findViewById(R.id.likesContainer);
            text_image_likes = itemView.findViewById(R.id.text_image_likes);

//            authorImageView.setVisibility(isAuthorNeeded ? View.VISIBLE : View.GONE);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (onClickListener != null && position != RecyclerView.NO_POSITION) {
                        onClickListener.onArtPostClickListener(mArtPostList.get(position));
                    }
                }
            });


//            authorImageView.setOnClickListener(v -> {
//                int position = getAdapterPosition();
//                if (onClickListener != null && position != RecyclerView.NO_POSITION) {
//                    onClickListener.onAuthorClick(getAdapterPosition(), v);
//                }
//            });


            if (sharedPreferences.getUserType() == UserTypes.ADMIN) {
                btn_more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopupMenu popup = new PopupMenu(btn_more.getContext(), btn_more);
                        popup.inflate(R.menu.news_options_menu);
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                ArtPost object = mArtPostList.get(getAdapterPosition());
                                itemSelectedListener.onMenuAction(object, item);

                                return false;
                            }
                        });
                        // here you can inflate your menu

                        popup.show();
                    }
                });

            } else {
                btn_more.setVisibility(View.GONE);
            }
        }


    }

}
