package yp.soul.com.youngpicasso.Notice;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.github.thunder413.datetimeutils.DateTimeUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import yp.soul.com.youngpicasso.Home.HomeActivity;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.TextDataUtils;
import yp.soul.com.youngpicasso.Utils.UserTypes;
import yp.soul.com.youngpicasso.helpers.BaseFragment;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.helpers.OnNoticeClickListener;
import yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper;
import yp.soul.com.youngpicasso.models.Notice;

import static yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper.getInstance;

public class NoticeFragment extends BaseFragment {

    public static final int CONTEXT_MENU_DELETE = 1;
    public static final int CONTEXT_MENU_EDIT = 2;

    public static final String CONTEXT_MENU_KEY_INTENT_DATA_POS = "pos";

    private ValueEventListener noticeListener;
    private FloatingActionButton fab_options;
    private EditText notice_title, notice_desc;
    private String mNoticeTitle, mNoticeDesc;
    private View sheetView;
    private BottomSheetDialog mBottomSheetDialog;
    private String notice_id = "-1";
    private FrameLayout notice_mainLayout;


    private static final String TAG = "NoticeFragment";
    private View view;
    private Context mContext;

    private List<Notice> mNotices;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout;
    private LinearLayoutManager manager;
    private FirebaseHelper mFirebaseHelper;

    private NoticesRecyclerAdapter adapter;
    private int edit_item_postition = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notices, container, false);
        mContext = getContext();


        setupToolbar();
        setupWidgets();
        setupAdapter();
        setupFirebase();

        setupNoticesList();

        setupFAB();

        return view;
    }

    private void setupToolbar() {
        ((HomeActivity) mContext).getSupportActionBar().setTitle(getTitle());
    }

    private void popupForDelete(Notice notice, int pos) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.Theme_AppCompat_Dialog_Alert);
        builder.setTitle("Delete notice " + notice.getNotice_title() + " ?");

        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mFirebaseHelper.getMyRef().child("notices")
                        .child(notice.getNotice_id())
                        .removeValue(new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                mNotices.remove(pos);
                                adapter.notifyDataSetChanged();
                                Snackbar.make(view.findViewById(R.id.main_layout), "Success", Snackbar.LENGTH_SHORT);
                            }
                        });

                dialog.cancel();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void setupFAB() {
        SharedPreferenceHelper preferenceHelper = getInstance(mContext);

        fab_options = view.findViewById(R.id.fab_filter);
        if ((int) preferenceHelper.getUserInfo().getUser_type() == UserTypes.ADMIN) {
            mBottomSheetDialog = new BottomSheetDialog(getActivity());
            sheetView = getActivity().getLayoutInflater().inflate(R.layout.bottom_sheet_notice_add, null);

            mBottomSheetDialog.setContentView(sheetView);

            fab_options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mBottomSheetDialog.show();
                    notice_id = "-1";
                }
            });

            AppCompatButton btn_post_notice = sheetView.findViewById(R.id.btn_post_notice);
            notice_title = sheetView.findViewById(R.id.input_title);
            notice_desc = sheetView.findViewById(R.id.input_desc);
            final View fragView = view;
            btn_post_notice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mNoticeTitle = notice_title.getText().toString();
                    mNoticeDesc = notice_desc.getText().toString();
                    if (validateForm()) {
                        String keyId;
                        //check if the notice is new or its from edit
                        if (!notice_id.equals("-1")) {

                            keyId = mFirebaseHelper.getMyRef().push().getKey();
                        } else {
                            keyId = notice_id;
                        }
                        final Notice notice = new Notice(
                                keyId,
                                getInstance(mContext).getUserInfo().getUsername(),
                                mNoticeTitle,
                                mNoticeDesc);
                        mFirebaseHelper.getMyRef().child("notices")
                                .child(keyId)
                                .setValue(notice, new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                        Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();

                                        if (notice_id.equals("-1")) {
                                            mNotices.get(edit_item_postition).setNotice_title(mNoticeTitle);
                                            mNotices.get(edit_item_postition).setNotice_title(mNoticeDesc);
                                            mNotices.get(edit_item_postition).setNotice_date(
                                                    DateTimeUtils.formatDate(new Date())
                                            );
                                        } else {

                                            mNotices.add(notice);
                                        }
                                        adapter.notifyDataSetChanged();
                                        mBottomSheetDialog.hide();

                                        recyclerView.smoothScrollToPosition(mNotices.size() - 1);
                                    }
                                });

                    }

                }
            });
        } else {
            fab_options.hide();
        }


    }

    private boolean validateForm() {
        boolean valid = true;

        if (TextDataUtils.isEmpty(mNoticeTitle)) {
            notice_title.setError("Cannot be empty");
            valid = false;
        } else {
            notice_title.setError(null);
        }


        return valid;
    }

    private void setupWidgets() {

        notice_mainLayout = view.findViewById(R.id.notice_mainLayout);
        refreshLayout = view.findViewById(R.id.refresh);
        refreshLayout.setRefreshing(true);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mNotices.clear();
                setupNoticesList();

            }
        });
    }

    private void tempData() {

    }

    private void setupNoticesList() {

        noticeListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mNotices.clear();
                for (DataSnapshot ds :
                        dataSnapshot.getChildren()) {
                    Notice notice = ds.getValue(Notice.class);
                    notice.setNotice_date(getTimestampDifference(notice.getNotice_date()));
                    mNotices.add(notice);
                }

                Collections.sort(mNotices, new Comparator<Notice>() {
                    @Override
                    public int compare(Notice o1, Notice o2) {
                        return o2.getNotice_date().compareTo(o1.getNotice_date());
                    }
                });
                adapter.notifyDataSetChanged();
                manager.scrollToPosition(mNotices.size() - 1);
                if (refreshLayout.isRefreshing()) {

                    refreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(mContext, "Error occured. Please try again", Toast.LENGTH_SHORT).show();
                refreshLayout.setRefreshing(false);
            }
        };
        mFirebaseHelper.getMyRef().child("notices")
                .addValueEventListener(noticeListener);
    }

    private void tempAddNotices() {
        String keyId = mFirebaseHelper.getMyRef().push().getKey();
        mFirebaseHelper.getMyRef().child("notices")
                .child(keyId)
                .setValue(new Notice(keyId, "manis", "Hello", "this is a test"));

    }

    /**
     * Returns a string representing the number of days ago the post was made
     *
     * @return
     */
    private String getTimestampDifference(String date) {
        Log.d(TAG, "getTimestampDifference: getting timestamp difference.");

        String difference = "";
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'",
                Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kathmandu"));
        Date today = c.getTime();
        sdf.format(today);
        Date timestamp;

        try {
            timestamp = sdf.parse(date);
            difference = String.valueOf(Math.round(((today.getTime() - timestamp.getTime()) / 1000 / 60 / 60 / 24)));
        } catch (ParseException e) {
            Log.e(TAG, "getTimestampDifference: ParseException: " + e.getMessage());
            difference = "0";
        }

        String days = "";
        if (difference.equals("0")) {
            days = "TODAY";
        } else {
            days = difference + " DAYS AGO";
        }
        return days;
    }

    private void setupAdapter() {
        mNotices = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
        manager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);


        recyclerView.setLayoutManager(manager);

        adapter = new NoticesRecyclerAdapter(mContext, mNotices, new OnNoticeClickListener() {
            @Override
            public void onNoticeClickListener(Notice notice) {

                NoticeDetailsFragment fragment = new NoticeDetailsFragment();
                Bundle bundle = new Bundle();

                bundle.putParcelable(mContext.getString(R.string.fragment_notice), notice);
                fragment.setArguments(bundle);
                ((HomeActivity) mContext).addFragments(fragment);
            }
        }, new OnNoticeMenuClickListener() {
            @Override
            public void onMenuClick(Notice notice, MenuItem item, int pos) {

                switch (item.getItemId()) {
                    case R.id.menu_edit:

                        notice_title.setText(notice.getNotice_title());
                        notice_desc.setText(notice.getNotice_desc());
                        edit_item_postition = pos;
                        mBottomSheetDialog.show();
                        break;
                    case R.id.menu_delete:
                        popupForDelete(notice, pos);


//                setupObjectList();
                        break;
                }
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void setupFirebase() {
        mFirebaseHelper = new FirebaseHelper(mContext);
    }


    @Override
    public String getTitle() {
        return "Notices";
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();


        mFirebaseHelper.getMyRef().removeEventListener(noticeListener);
    }

    public interface OnNoticeMenuClickListener {

        void onMenuClick(Notice notice, MenuItem item, int pos);
    }
}
