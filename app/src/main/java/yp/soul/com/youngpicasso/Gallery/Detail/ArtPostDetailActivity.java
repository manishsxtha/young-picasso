package yp.soul.com.youngpicasso.Gallery.Detail;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.amar.library.ui.StickyScrollView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import yp.soul.com.youngpicasso.Comments.CommentsRecyclerAdapter;
import yp.soul.com.youngpicasso.Gallery.ArtPostAddActivity;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.ComplainActivity;
import yp.soul.com.youngpicasso.Utils.FilePaths;
import yp.soul.com.youngpicasso.Utils.ImageDetailActivity;
import yp.soul.com.youngpicasso.Utils.TextDataUtils;
import yp.soul.com.youngpicasso.Utils.UniversalImageLoader;
import yp.soul.com.youngpicasso.Utils.UserTypes;
import yp.soul.com.youngpicasso.helpers.BaseDetailActivity;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.helpers.OnCommentRemoveListener;
import yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper;
import yp.soul.com.youngpicasso.models.ArtPost;
import yp.soul.com.youngpicasso.models.Comment;
import yp.soul.com.youngpicasso.models.CommentUserMerge;
import yp.soul.com.youngpicasso.models.Like;
import yp.soul.com.youngpicasso.models.User;

import static yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper.getInstance;

public class ArtPostDetailActivity extends BaseDetailActivity implements OnCommentRemoveListener {
    private static final String TAG = "EventDetailActivity";
    //vars
    List<Like> mLikeList;
    List<CommentUserMerge> mCommentList;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    private CommentsRecyclerAdapter adapter;
    private AlertDialog.Builder commentBuilder;
    private RelativeLayout relComments;
    private EditText commentEditText;
    @Nullable
    private ScrollView scrollView;
    private ViewGroup likesContainer;
    private ImageView likesImageView;
    private TextView text_image_likes;
    private TextView commentsLabel;
    private TextView likeCounterTextView;
    private TextView commentsCountTextView;
    private TextView watcherCounterTextView;
    private TextView authorTextView;
    private ImageView likesOffImageView;
    private TextView dateTextView;
    private LinearLayout commentsCounterContainer;
    private ImageView authorImageView;
    private ProgressBar progressBar;
    private ImageView postImageView;
    private TextView titleTextView;
    private TextView descriptionEditText;
    private TextView warningCommentsTextView;
    private Button sendButton;
    private FirebaseHelper mFirebaseHelper;
    private Context mContext = ArtPostDetailActivity.this;
    private SharedPreferenceHelper sharedPreferences;
    private StickyScrollView mScrollView;
    private AppBarLayout appBar;
    private ArtPost artPost;
    private Comment comment;
    private ArrayList<Comment> comments;

    @Override
    public void onCommentRemoveListener(final CommentUserMerge comment) {


        commentBuilder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                CommentUserMerge cm = mCommentsList.get(postion);
//                Comment comment = new Comment(cm.getComment_id(),
//                        cm.getComment_desc(),
//                        cm.getDate_created(),
//                        cm.getUser_id(),
//                        cm.getUsername());
                dialog.cancel();
                mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_news))
                        .child(artPost.getId())
                        .child("comment_list")
                        .child(comment.getComment_id())
                        .removeValue(new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
                                mCommentList.remove(comment);
                                adapter.notifyDataSetChanged();
                            }
                        });

            }
        });
        commentBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        commentBuilder.show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_art_post_detail);

        getDataFromIntent();

        setupWidgets();
        setupFirebase();
        increaseWatcherCount();
        setupAdapter();
        setupCommentsList();
//        tempData();

        initSendComment();

        setupToolbarAndStickyLayout();

        init();

    }

    private void increaseWatcherCount() {
        long watcherIncrement = artPost.getWatchersCount() + 1;
        mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_art_posts))
                .child(artPost.getId())
                .child(mContext.getString(R.string.db_field_art_watchcount))
                .setValue(watcherIncrement);
    }

    private void initSendComment() {
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked add comment btn");
                String textComment = commentEditText.getText().toString();
                if (!textComment.equals("")) {
                    String keyId = mFirebaseHelper.getMyRef().push().getKey();
                    final Comment comment = new Comment(
                            keyId,
                            textComment,
                            mFirebaseHelper.getAuth().getCurrentUser().getUid(),
                            mFirebaseHelper.getSharedPreference().getUserInfo().getUsername()
                    );
                    Toast.makeText(mContext, "sending your comment..", Toast.LENGTH_SHORT).show();
                    mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_art_posts))
                            .child(artPost.getId())
                            .child("comment_list")
                            .child(keyId)
                            .setValue(comment, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {


                                    mCommentList.add(getCommentUserMergeModel(comment, ""));
                                    adapter.notifyDataSetChanged();
                                    mScrollView.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            mScrollView.fullScroll(NestedScrollView.FOCUS_DOWN);
                                        }
                                    }, 200);
                                }
                            });
                    commentEditText.setText("");
                }

                closeKeyboard();
            }
        });
    }

    private void tempData() {
        CommentUserMerge userMerge = new CommentUserMerge(
                "1",
                "This is a comment",
                "1D",
                "1",
                "babu",
                "https://lh5.googleusercontent.com/-q4lp0Z5FV5U/AAAAAAAAAAI/AAAAAAAAABU/zoqmTuUeL1M/s96-c/photo.jpg"

        );

        CommentUserMerge userMerge1 = new CommentUserMerge(
                "1",
                "This is a comment",
                "1D",
                "1",
                "babu",
                "https://lh5.googleusercontent.com/-q4lp0Z5FV5U/AAAAAAAAAAI/AAAAAAAAABU/zoqmTuUeL1M/s96-c/photo.jpg"

        );

        CommentUserMerge userMerge2 = new CommentUserMerge(
                "1",
                "This is a comment",
                "1D",
                "1",
                "babu",
                "https://lh5.googleusercontent.com/-q4lp0Z5FV5U/AAAAAAAAAAI/AAAAAAAAABU/zoqmTuUeL1M/s96-c/photo.jpg"

        );

        mCommentList.add(userMerge);
        mCommentList.add(userMerge1);
        mCommentList.add(userMerge2);

        adapter.notifyDataSetChanged();
    }

    private void setupCommentsList() {
        Log.d(TAG, "setupCommentsList: " + artPost.getComment_list());
        for (final Comment comment :
                artPost.getComment_list()) {
            mFirebaseHelper.getMyRef().child("users")
                    .orderByChild("user_id")
                    .equalTo(comment.getUser_id())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (DataSnapshot s :
                                    dataSnapshot.getChildren()) {

                                mCommentList.add(getCommentUserMergeModel(comment,
                                        s.getValue(User.class).getAvatar_img_link()
                                ));
                            }
                            adapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
        }


    }

    private CommentUserMerge getCommentUserMergeModel(Comment comment, String userImage) {
        if (userImage.isEmpty()) {
            userImage = sharedPreferences.getUserInfo().getAvatar_img_link();
        }
        CommentUserMerge userMerge = new CommentUserMerge(
                comment.getComment_id(),
                comment.getComment_desc(),
                TextDataUtils.getTimestampDifference2(comment.getDate_created()),
                comment.getUser_id(),
                comment.getUsername(),
                userImage

        );
        return userMerge;
    }

    private void setupToolbarAndStickyLayout() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(artPost.getTitle());

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        artPost = intent.getParcelableExtra(getString(R.string.calling_art_gallery_intent));
        if (artPost == null) {
            finish();
        }
    }

    /**
     * for saving the state of the activity on rotation changes
     *
     * @param outState
     */
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntArray("ARTICLE_SCROLL_POSITION",
                new int[]{mScrollView.getScrollX(), mScrollView.getScrollY()});
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final int[] position = savedInstanceState.getIntArray("ARTICLE_SCROLL_POSITION");
        if (position != null)
            appBar.setExpanded(false);
        mScrollView.post(new Runnable() {
            public void run() {
                mScrollView.scrollTo(position[0], position[1]);
            }
        });
    }

    private void setupFirebase() {
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
    }

    private void init() {
        sharedPreferences = getInstance(mContext);
//        UniversalImageLoader.setImage(artPost.getNews_photo(),
//                toolbarExpandedImage, null, "");
        titleTextView.setText(artPost.getTitle());
        watcherCounterTextView.setText(String.valueOf(artPost.getWatchersCount()));
        descriptionEditText.setText(artPost.getDescription());
        UniversalImageLoader.setImage(artPost.getImagePath(),
                postImageView, null, "");
        authorTextView.setText(artPost.getUsername());

        setAuthorImageView();

        dateTextView.setText(
                TextDataUtils.getTimestampDifference2(artPost.getCreatedDate()));

        prepareLikes();
        prepareComments();

        //press comments link
        commentsCounterContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mScrollView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mScrollView.fullScroll(NestedScrollView.FOCUS_DOWN);
                    }
                }, 200);
            }
        });

        commentEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() != 0) {
                    sendButton.setClickable(true);
                } else {
                    sendButton.setClickable(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        //click on image to zoom
        postImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onImagePressed();
            }
        });


    }

    private void setAuthorImageView() {
        mFirebaseHelper.getMyRef().child("users")
                .child(artPost.getUser_id())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {

                            User user = dataSnapshot.getValue(User.class);
                            UniversalImageLoader.setImage(user.getAvatar_img_link(),
                                    authorImageView, null, "");
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void prepareLikes() {
        //check likes and prepare likes string
        Boolean mLikedByCurrentUser = false;
        StringBuilder mUsersName = new StringBuilder();
        mLikeList = artPost.getLikeList();
        //check if there are no likes for that news
        if (mLikeList.size() != 0) {

            likeCounterTextView.setText(String.format("%d", artPost.getLikeList().size()));
            for (Like like :
                    artPost.getLikeList()) {
                mUsersName.append(like.getUsername());
                mUsersName.append(",");
                if (mFirebaseHelper.getAuth().getCurrentUser().getUid().equals(like.getUser_id())) {
                    mLikedByCurrentUser = true;
                }
            }
            if (mLikedByCurrentUser) {
                likesImageView.setVisibility(View.VISIBLE);
                likesOffImageView.setVisibility(View.GONE);
                likeCounterTextView.setTextColor(ContextCompat.getColor(mContext, R.color.red_normal));
            } else {
                likesImageView.setVisibility(View.GONE);
                likesOffImageView.setVisibility(View.VISIBLE);
                likeCounterTextView.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            }

            text_image_likes.setText(getLikesString(mUsersName));
        } else {
            mLikeList = new ArrayList<>();
            likeCounterTextView.setText("0");
            likesImageView.setVisibility(View.GONE);
            likesOffImageView.setVisibility(View.VISIBLE);

            text_image_likes.setText(getLikesString(mUsersName));
        }


        // handle likes on click events

        likesContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //database access and if complete then toggle red heart to normal
                if (likesImageView.getVisibility() == View.VISIBLE) {
                    likesImageView.setClickable(false);
                    likesOffImageView.setClickable(false);

                    Like userLike = new Like();
                    for (Like like :
                            artPost.getLikeList()) {
                        if (like.getUser_id().equals(mFirebaseHelper.getAuth().getCurrentUser().getUid())) {
                            userLike = like;
                            break;
                        }
                    }
                    final Like usertemp = userLike;
                    mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_art_posts))
                            .child(artPost.getId())
                            .child(mContext.getString(R.string.db_field_likes))
                            .child(usertemp.getLike_id())
                            .removeValue(new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                    likesOffImageView.setClickable(true);
                                    likesImageView.setVisibility(View.GONE);
                                    likesOffImageView.setVisibility(View.VISIBLE);
                                    artPost.getLikeList().remove(usertemp);
                                    likeCounterTextView.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                                    likeCounterTextView.setText(String.format("%d",
                                            artPost.getLikeList().size()));

                                    StringBuilder mUsersName = new StringBuilder();
                                    for (Like like :
                                            artPost.getLikeList()) {
                                        mUsersName.append(like.getUsername());
                                        mUsersName.append(",");
                                    }
                                    text_image_likes.setText(getLikesString(mUsersName));
                                }
                            });

                } else {
                    likesImageView.setClickable(false);
                    likesOffImageView.setClickable(false);
                    String keyId = mFirebaseHelper.getMyRef().push().getKey();
                    User user = sharedPreferences.getUserInfo();
                    final Like like = new Like(user.getUser_id(),
                            user.getUsername(),
                            keyId
                    );

                    mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_art_posts))
                            .child(artPost.getId())
                            .child(mContext.getString(R.string.db_field_likes))
                            .child(keyId)
                            .setValue(like, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                    artPost.getLikeList().add(like);
                                    likesImageView.setVisibility(View.VISIBLE);
                                    likesOffImageView.setVisibility(View.GONE);
                                    likeCounterTextView.setTextColor(ContextCompat.getColor(mContext, R.color.red_normal));
                                    likeCounterTextView.setText(String.format("%d",
                                            artPost.getLikeList().size()));

                                    StringBuilder mUsersName = new StringBuilder();
                                    for (Like like :
                                            artPost.getLikeList()) {
                                        mUsersName.append(like.getUsername());
                                        mUsersName.append(",");
                                    }
                                    text_image_likes.setText(getLikesString(mUsersName));
                                    likesImageView.setClickable(true);

                                }
                            });
                }

            }
        });


    }

    private String getLikesString(StringBuilder mUsersName) {
        if (mUsersName.toString().isEmpty()) {
            return mContext.getString(R.string.no_comment_sample);
        }
        String[] splitUsers = mUsersName.toString().split(",");
        String mLikesString = "";
        int length = splitUsers.length;
        if (length == 0) {
            mLikesString = mContext.getString(R.string.no_comment_sample);
        } else if (length == 1) {
            mLikesString = "Liked by " + splitUsers[0];
        } else if (length == 2) {
            mLikesString = "Liked by " + splitUsers[0]
                    + " and " + splitUsers[1];
        } else if (length == 3) {
            mLikesString = "Liked by " + splitUsers[0]
                    + ", " + splitUsers[1]
                    + " and " + splitUsers[2];

        } else if (length == 4) {
            mLikesString = "Liked by " + splitUsers[0]
                    + ", " + splitUsers[1]
                    + ", " + splitUsers[2]
                    + " and " + splitUsers[3];
        } else if (length > 4) {
            mLikesString = "Liked by " + splitUsers[0]
                    + ", " + splitUsers[1]
                    + ", " + splitUsers[2]
                    + " and " + (splitUsers.length - 3) + " others";
        }
        return mLikesString;
    }

    private void prepareComments() {
        //prepare comments count
        List<Comment> comment = artPost.getComment_list();

        if (comment.size() != 0) {
            int comment_size = comment.size();
            commentsCountTextView.setText(String.valueOf(comment_size));

        } else {
            commentsCountTextView.setText("0");

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.post_details_menu, menu);
//        menu.findItem(R.id.edit_post_action).setVisible(false);
//        menu.findItem(R.id.delete_post_action).setVisible(false);
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        //TODO instead of directly deleting post by admin eiher flag the content or send msg of deletion
        if (!artPost.getUser_id().equals(mFirebaseHelper.getAuth().getCurrentUser().getUid())) {
            menu.findItem(R.id.edit_post_action).setVisible(false);
            menu.findItem(R.id.delete_post_action).setVisible(false);
        }
        if (mFirebaseHelper.getSharedPreference().getUserInfo().getUser_type() == UserTypes.ADMIN) {
            menu.findItem(R.id.edit_post_action).setVisible(false);
            menu.findItem(R.id.delete_post_action).setVisible(true);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.complain_action:
                Intent intent = new Intent(this, ComplainActivity.class);
                intent.putExtra(mContext.getString(R.string.calling_complain_intent), artPost);
                startActivity(intent);

                return true;
            case R.id.edit_post_action:
                Intent editIntent = new Intent(this, ArtPostAddActivity.class);
                editIntent.putExtra(mContext.getString(R.string.calling_art_gallery_intent), artPost);
                startActivity(editIntent);

                return true;
            case R.id.delete_post_action:
                popupForDelete();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void popupForDelete() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialog);
        builder.setTitle("Delete post " + artPost.getTitle() + " ?");
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mFirebaseHelper.getmStorageReference().child(FilePaths.FIREBASE_ART_STORAGE + artPost.getImageTitle())
                        .delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_art_posts))
                                .child(artPost.getId())
                                .removeValue(new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                        Toast.makeText(mContext, "Deleted artPost: " + artPost.getTitle(), Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                });

                        dialog.cancel();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_art_posts))
                                .child(artPost.getId())
                                .removeValue(new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                        Toast.makeText(mContext, "Deleted artPost: " + artPost.getTitle(), Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                });

                        dialog.cancel();
                    }
                });


            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void setupWidgets() {

        commentBuilder = new AlertDialog.Builder(mContext);
        commentBuilder.setTitle("Delete comment?");

        titleTextView = findViewById(R.id.titleTextView);
        descriptionEditText = findViewById(R.id.descriptionEditText);
        postImageView = findViewById(R.id.postImageView);
        progressBar = findViewById(R.id.progressBar);

        mScrollView = findViewById(R.id.scrollView);
        commentsLabel = findViewById(R.id.commentsLabel);
        commentEditText = findViewById(R.id.commentEditText);
        likesContainer = findViewById(R.id.likesContainer);
        likesImageView = findViewById(R.id.likesImageView);
        likesOffImageView = findViewById(R.id.likesOffImageView);
        authorImageView = findViewById(R.id.authorImageView);
        authorTextView = findViewById(R.id.authorTextView);
        likeCounterTextView = findViewById(R.id.likeCounterTextView);
        commentsCountTextView = findViewById(R.id.commentsCountTextView);
        watcherCounterTextView = findViewById(R.id.watcherCounterTextView);
        dateTextView = findViewById(R.id.dateTextView);

        warningCommentsTextView = findViewById(R.id.warningCommentsTextView);
        sendButton = findViewById(R.id.sendButton);
        commentsCounterContainer = findViewById(R.id.commentsCounterContainer);

        text_image_likes = findViewById(R.id.text_image_likes);
    }

    private void setupAdapter() {
        mCommentList = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerView);
        manager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, true);

        recyclerView.setNestedScrollingEnabled(false);

        recyclerView.setLayoutManager(manager);

        adapter = new CommentsRecyclerAdapter(mContext, mCommentList, this);

        recyclerView.setAdapter(adapter);


    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void onImagePressed() {
        Intent intent = new Intent(this, ImageDetailActivity.class);
        intent.putExtra(ImageDetailActivity.IMAGE_URL_EXTRA_KEY, artPost.getImagePath());
        startActivity(intent);
    }
}
