package yp.soul.com.youngpicasso.helpers;

import yp.soul.com.youngpicasso.models.News;


public interface OnNewsClickListener {
    void onNewsClickListener(News news);
}
