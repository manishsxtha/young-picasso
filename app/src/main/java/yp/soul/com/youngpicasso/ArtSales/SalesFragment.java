package yp.soul.com.youngpicasso.ArtSales;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.thunder413.datetimeutils.DateTimeUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.AdminUID;
import yp.soul.com.youngpicasso.Utils.UserTypes;
import yp.soul.com.youngpicasso.helpers.BaseFragment;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.models.ArtSales;


public class SalesFragment extends BaseFragment {


    private static final String TAG = "UpcomingEventsFragment";


    private View view;
    private Context mContext;

    private List<ArtSales> mArtSalesList;

    private ValueEventListener eventListener;
    private RecyclerView recyclerView;

    private WaveSwipeRefreshLayout refreshLayout;

    private FirebaseHelper mFirebaseHelper;
    private ArtSalesRecyclerAdapter adapter;
    private GridLayoutManager manager;


    public static Fragment newInstance() {
        SalesFragment fragment = new SalesFragment();

        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recycler_view_container, container, false);
        mContext = getContext();

        setupFirebase();
        setupWidgets();

        setupAdapter();
        setupArtPostList();

//        setupTempData();
        return view;
    }


    private void setupTempData() {
        String keyId = mFirebaseHelper.getMyRef().push().getKey();


        ArtSales artSale = new ArtSales();


        artSale.setCreated_date(DateTimeUtils.formatDate(new Date()));
        artSale.setDescription("desc");
        artSale.setHasComplain(false);
        artSale.setId(keyId);
        artSale.setImagePath(AdminUID.TEMP_IMAGE_URL);
        artSale.setTitle(mContext.getString(R.string.sample_title) + " 1");
        artSale.setUser_id("9LO2xwZckWVRKYeC8HwcO9bSOma2");
        artSale.setUsername("Manish");
        artSale.setPrice("1000");
        artSale.setAvailable(true);

        artSale.setWatchersCount(0);
        artSale.setCommentsCount(0);

        artSale.setItemType(UserTypes.ADMIN);
        artSale.setDescription(mContext.getString(R.string.sample_description));

        mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_art_sales))
                .child(keyId)
                .setValue(artSale);

        mArtSalesList.add(artSale);

    }


    private void setupWidgets() {

        refreshLayout = view.findViewById(R.id.refresh);
        refreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);
        refreshLayout.setWaveColor(Color.argb(100, 255, 0, 0));
        refreshLayout.setRefreshing(true);
        refreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Do work to refresh the list here.

                mArtSalesList.clear();
                setupArtPostList();
                refreshLayout.setWaveColor(0xFF000000 + new Random().nextInt(0xFFFFFF));
            }
        });
    }

    private void setupArtPostList() {

        mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_art_sales))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        mArtSalesList.clear();

                        for (DataSnapshot ds :
                                dataSnapshot.getChildren()) {
                            ArtSales artSales = new ArtSales();
                            Map<String, Object> objectMap = (HashMap<String, Object>) ds.getValue();

                            if ((Long) objectMap.get("itemType") == UserTypes.STUDENT) {
                                continue;
                            }
                            artSales.setId(objectMap.get("id").toString());
                            artSales.setTitle(objectMap.get("title").toString());
                            artSales.setDescription(objectMap.get("description").toString());
                            artSales.setCreated_date(objectMap.get("created_date").toString());
                            artSales.setUser_id(objectMap.get("user_id").toString());
                            artSales.setUsername(objectMap.get("username").toString());
                            artSales.setImagePath(objectMap.get("imagePath").toString());
                            artSales.setImageTitle(objectMap.get("imageTitle").toString());
                            artSales.setPrice(objectMap.get("price").toString());
                            artSales.setAvailable((Boolean) objectMap.get("available"));

                            artSales.setCommentsCount((Long) objectMap.get("commentsCount"));
                            artSales.setHasComplain((Boolean) objectMap.get("hasComplain"));
                            artSales.setWatchersCount((Long) objectMap.get("watchersCount"));
                            artSales.setItemType((Long) objectMap.get("itemType"));

                            mArtSalesList.add(artSales);


                        }

//                Collections.sort(mNewsList, new Comparator<News>() {
//                    @Override
//                    public int compare(News o1, News o2) {
//                        return o2.getDated_added().compareTo(o1.getDated_added());
//                    }
//                });
                        adapter.notifyDataSetChanged();
//                        manager.scrollToPosition(mArtSalesList.size() - 1);
                        if (refreshLayout.isRefreshing()) {

                            refreshLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        refreshLayout.setRefreshing(false);
                        Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void setupAdapter() {
        mArtSalesList = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
//        manager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        manager = new GridLayoutManager(getContext(), 2);

        recyclerView.setLayoutManager(manager);

        adapter = new ArtSalesRecyclerAdapter(mContext, mArtSalesList, new ArtSalesRecyclerAdapter.OnArtSaleClickListener() {
            @Override
            public void onPostClick(ArtSales artSales) {
                Intent intent = new Intent(mContext, ArtSaleDetailActivity.class);
                intent.putExtra(getString(R.string.callingSalesDetail), artSales);
                startActivity(intent);
            }
        });

        recyclerView.setAdapter(adapter);


    }


    private void setupFirebase() {
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
    }

    @Override
    public String getTitle() {
        return "Art Sale";
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        mArtSalesList.clear();
    }
}
