package yp.soul.com.youngpicasso.helpers;

import yp.soul.com.youngpicasso.models.Notice;

public interface OnNoticeClickListener {
    void onNoticeClickListener(Notice notice);
}
