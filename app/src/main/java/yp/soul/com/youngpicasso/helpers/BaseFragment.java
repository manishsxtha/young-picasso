package yp.soul.com.youngpicasso.helpers;

import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment {

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getTitle());
    }

    public abstract String getTitle();
}
