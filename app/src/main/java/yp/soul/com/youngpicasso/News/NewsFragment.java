package yp.soul.com.youngpicasso.News;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;
import yp.soul.com.youngpicasso.Home.HomeActivity;
import yp.soul.com.youngpicasso.News.Detail.NewsDetailActivity;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.helpers.BaseFragment;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.helpers.OnNewsClickListener;
import yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper;
import yp.soul.com.youngpicasso.models.Comment;
import yp.soul.com.youngpicasso.models.Like;
import yp.soul.com.youngpicasso.models.News;
import yp.soul.com.youngpicasso.models.User;

import static yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper.getInstance;

public class NewsFragment extends BaseFragment implements OnNewsClickListener, NewsRecyclerAdapter.OnItemSelectedListener {
    @Override
    public void onMenuAction(News news, MenuItem item) {
        Log.e(TAG, "onMenuAction: news:" + news);
        switch (item.getItemId()) {
            case R.id.menu_edit:
                Intent intent = new Intent(mContext, NewsAddActivity.class);
                intent.putExtra(mContext.getString(R.string.calling_news_intent), news);
                startActivity(intent);
                break;
            case R.id.menu_delete:
                popupForDelete(news);


//                setupObjectList();
                break;
        }
    }


    @Override
    public void onNewsClickListener(News news) {
        Intent intent = new Intent(mContext, NewsDetailActivity.class);
        intent.putExtra(mContext.getString(R.string.calling_news_intent), news);
        startActivity(intent);
    }


    public static final int CONTEXT_MENU_DELETE = 1;
    public static final int CONTEXT_MENU_EDIT = 2;
    public static final String CONTEXT_MENU_KEY_INTENT_DATA_POS = "pos";
    private ValueEventListener noticeListener;

    private FloatingActionButton fab_options;


    private String notice_id = "-1";

    private static final String TAG = "NewsFragment";
    private View view;
    private Context mContext;
    private List<News> mNewsList;
    private List<User> mNewsByUserList;
    private RecyclerView recyclerView;

    private WaveSwipeRefreshLayout refreshLayout;

    private LinearLayoutManager manager;

    private FirebaseHelper mFirebaseHelper;

    private NewsRecyclerAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_admin_news, container, false);
        mContext = getContext();

        setupToolbar();
        setupWidgets();
        setupAdapter();
        setupFirebase();
        setupNewsList();

//
//        setupNewsList();
//
        setupFAB();

        return view;
    }


    private void popupForDelete(final News news) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialog);
        builder.setTitle("Delete news " + news.getNews_title() + " ?");
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mFirebaseHelper.getMyRef().child("news")
                        .child(news.getNews_id())
                        .removeValue(new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                mNewsList.remove(news);
                                adapter.notifyDataSetChanged();
                                Toast.makeText(mContext, "Deleted news: " + news.getNews_title(), Toast.LENGTH_SHORT).show();
                            }
                        });

                dialog.cancel();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void setupToolbar() {
        ((HomeActivity) mContext).getSupportActionBar().setTitle(getTitle());
    }

    private void setupFAB() {
        SharedPreferenceHelper preferenceHelper = getInstance(mContext);

        fab_options = view.findViewById(R.id.fab_filter);
        if ((int) preferenceHelper.getUserInfo().getUser_type() == 0) {

            fab_options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //start the add news activity
                    startActivity(new Intent(mContext, NewsAddActivity.class));
                }
            });


        } else {
            fab_options.hide();
        }


    }


    private void setupWidgets() {

        refreshLayout = view.findViewById(R.id.refresh);
        refreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);
        refreshLayout.setWaveColor(Color.argb(100, 255, 0, 0));

        refreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Do work to refresh the list here.

                mNewsList.clear();
                setupNewsList();
                refreshLayout.setWaveColor(0xFF000000 + new Random().nextInt(0xFFFFFF));
            }
        });
    }

    private void setupAdapter() {
        mNewsList = new ArrayList<>();
        mNewsByUserList = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
        manager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);


        recyclerView.setLayoutManager(manager);

        adapter = new NewsRecyclerAdapter(mContext, mNewsList, mNewsByUserList, this, this);

        recyclerView.setAdapter(adapter);


    }

    private void setupNewsList() {
        refreshLayout.setRefreshing(true);
        noticeListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mNewsByUserList.clear();
                mNewsList.clear();
                for (DataSnapshot ds :
                        dataSnapshot.getChildren()) {
                    News news = new News();
                    Map<String, Object> objectMap = (HashMap<String, Object>) ds.getValue();

                    news.setNews_id(objectMap.get("news_id").toString());
                    news.setNews_title(objectMap.get("news_title").toString());
                    news.setNews_desc(objectMap.get("news_desc").toString());
                    news.setNews_photo(objectMap.get("news_photo").toString());
                    news.setDated_added(objectMap.get("dated_added").toString());
                    news.setUser_id(objectMap.get("user_id").toString());

                    List<Comment> comments = new ArrayList<>();
                    for (DataSnapshot sss :
                            ds.child("comment_list").getChildren()) {
                        Comment comment = new Comment();
                        Map<String, Object> objectMap1 = (HashMap<String, Object>) sss.getValue();
                        comment.setComment_id(objectMap1.get("comment_id").toString());
                        comment.setComment_desc(objectMap1.get("comment_desc").toString());
                        comment.setDate_created(objectMap1.get("date_created").toString());
                        comment.setUser_id(objectMap1.get("user_id").toString());
                        comment.setUsername(objectMap1.get("username").toString());

                        comments.add(comment);

                    }
                    news.setComment_list(comments);

                    List<Like> likes = new ArrayList<>();
                    for (DataSnapshot likesSnap :
                            ds.child("mUserLikes").getChildren()) {
                        Like like = new Like();
                        Map<String, Object> objectMap1 = (HashMap<String, Object>) likesSnap.getValue();
                        like.setUser_id(objectMap1.get("user_id").toString());
                        like.setLike_id(objectMap1.get("like_id").toString());
                        like.setUsername(objectMap1.get("username").toString());

                        likes.add(like);

                    }
                    news.setmUserLikes(likes);
                    mNewsList.add(news);


                }

                Collections.sort(mNewsList, new Comparator<News>() {
                    @Override
                    public int compare(News o1, News o2) {
                        return o2.getDated_added().compareTo(o1.getDated_added());
                    }
                });
                adapter.notifyDataSetChanged();
                manager.scrollToPosition(0);
                if (refreshLayout.isRefreshing()) {

                    refreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_news))
                .addListenerForSingleValueEvent(noticeListener);
    }


    /**
     * Returns a string representing the number of days ago the post was made
     *
     * @return
     */
    private String getTimestampDifference(String date) {
        Log.d(TAG, "getTimestampDifference: getting timestamp difference.");

        String difference = "";
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'",
                Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kathmandu"));
        Date today = c.getTime();
        sdf.format(today);
        Date timestamp;

        try {
            timestamp = sdf.parse(date);
            difference = String.valueOf(Math.round(((today.getTime() - timestamp.getTime()) / 1000 / 60 / 60 / 24)));
        } catch (ParseException e) {
            Log.e(TAG, "getTimestampDifference: ParseException: " + e.getMessage());
            difference = "0";
        }

        String days = "";
        if (difference.equals("0")) {
            days = "TODAY";
        } else {
            days = difference + " DAYS AGO";
        }
        return days;
    }

    private void setupFirebase() {
        mFirebaseHelper = new FirebaseHelper(mContext);
    }


    @Override
    public String getTitle() {
        return "News";
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (noticeListener != null)
            mFirebaseHelper.getMyRef().removeEventListener(noticeListener);
    }
}
