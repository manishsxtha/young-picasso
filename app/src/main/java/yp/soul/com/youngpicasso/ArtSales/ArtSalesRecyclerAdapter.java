package yp.soul.com.youngpicasso.ArtSales;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.UniversalImageLoader;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper;
import yp.soul.com.youngpicasso.models.ArtSales;
import yp.soul.com.youngpicasso.models.Comment;
import yp.soul.com.youngpicasso.models.Like;

import static yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper.getInstance;

public class ArtSalesRecyclerAdapter extends RecyclerView.Adapter<ArtSalesRecyclerAdapter.ViewHolder> {


    public final SharedPreferenceHelper sharedPreferences;
    private final FirebaseHelper mFirebaseHelper;
    private OnArtSaleClickListener onClickListener;

    private List<ArtSales> mArtSalesList;
    private Context context;


    private List<Like> mLikeList;
    private List<Comment> comment;


    public ArtSalesRecyclerAdapter(Context context, List<ArtSales> mArtSalesList, OnArtSaleClickListener listener) {

        this.context = context;
        this.mArtSalesList = mArtSalesList;

        this.onClickListener = listener;

        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(context);
        sharedPreferences = getInstance(context);

    }

    @NonNull
    @Override
    public ArtSalesRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext())
                .inflate(R.layout.layout_shop_item_list, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ArtSalesRecyclerAdapter.ViewHolder holder, final int position) {
        final ArtSales artSales = mArtSalesList.get(holder.getAdapterPosition());


        UniversalImageLoader.setImage(artSales.getImagePath(),
                holder.mImageView, holder.progressBar, "");

        holder.tvTitle.setText(artSales.getTitle());
        holder.tvDesc.setText(artSales.getDescription());
        holder.tvPrice.setText("Rs. " + artSales.getPrice());

        if (artSales.getAvailable()) {
            holder.labelArtSale.setVisibility(View.GONE);
        } else {
            holder.labelArtSale.setVisibility(View.VISIBLE);
        }

//        mFirebaseHelper.getMyRef().child(context.getString(R.string.db_wishlist))
//                .child(mFirebaseHelper.getUserID())
//                .child(mArtSalesList.get(position).getId())
//                .addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
////                        for (DataSnapshot ds :
////                                dataSnapshot.getChildren()) {
//                        if (dataSnapshot.exists()){
//                            WishList wishList = dataSnapshot.getValue(WishList.class);
//
//                            if (wishList.getPost_id().equals(mArtSalesList.get(position).getId())) {
////                                showWishListRed(holder);
//                                holder.img_wishlist.setVisibility(View.GONE);
//                                holder.img_wishlist_red.setVisibility(View.VISIBLE);
//                            } else {
//                                showNotInWishList(holder);
//                            }
//                        }
//
////                        }
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                    }
//                });

        //Set click action for wishlist
//
//        holder.likeViewGroup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //database access and if complete then toggle red heart to normal
//                if (holder.img_wishlist_red.getVisibility() == View.VISIBLE) {
//                    holder.img_wishlist.setClickable(false);
//                    holder.img_wishlist_red.setClickable(false);
//
//                    mFirebaseHelper.getMyRef().child(context.getString(R.string.db_wishlist))
//                            .child(mFirebaseHelper.getUserID())
//                            .child(artSales.getId())
//                            .removeValue(new DatabaseReference.CompletionListener() {
//                                @Override
//                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
//
//                                    holder.img_wishlist_red.setClickable(true);
//                                    showNotInWishList(holder);
//                                }
//                            });
//
//                } else {
//                    holder.img_wishlist.setClickable(false);
//                    holder.img_wishlist_red.setClickable(false);
//                    String keyId = mFirebaseHelper.getMyRef().push().getKey();
//                    User user = sharedPreferences.getUserInfo();
//                    final WishList wishList = new WishList(keyId,
//                            artSales.getId()
//                    );
//
//                    mFirebaseHelper.getMyRef().child(context.getString(R.string.db_wishlist))
//                            .child(mFirebaseHelper.getUserID())
//                            .child(artSales.getId())
//                            .setValue(wishList, new DatabaseReference.CompletionListener() {
//                                @Override
//                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
//
//                                    showWishListRed(holder);
//
//                                    holder.img_wishlist.setClickable(true);
//
//                                }
//                            });
//                }
//
//            }
//        });


    }

//    private void showNotInWishList(ViewHolder holder) {
//        holder.img_wishlist_red.setVisibility(View.GONE);
//        holder.img_wishlist.setVisibility(View.VISIBLE);
//    }
//
//    private void showWishListRed(ViewHolder holder) {
//        holder.img_wishlist.setVisibility(View.GONE);
//        holder.img_wishlist_red.setVisibility(View.VISIBLE);
//    }
//

    @Override
    public int getItemCount() {
        if (mArtSalesList != null) {
            return mArtSalesList.size();
        }
        return 0;
    }

    public interface OnArtSaleClickListener {
        void onPostClick(ArtSales artSales);

    }

    class ViewHolder extends RecyclerView.ViewHolder {


        ImageView mImageView;
        ProgressBar progressBar;
        TextView tvTitle, tvDesc, tvPrice, labelArtSale;

//        RelativeLayout likeViewGroup;
//        ImageView img_wishlist, img_wishlist_red;


        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.image_art_sale);
            progressBar = itemView.findViewById(R.id.progressBar);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDesc = itemView.findViewById(R.id.tvDesc);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            labelArtSale = itemView.findViewById(R.id.labelArtSale);

//            likeViewGroup = itemView.findViewById(R.id.likeViewGroup);
//            img_wishlist = itemView.findViewById(R.id.img_wishlist);
//            img_wishlist_red = itemView.findViewById(R.id.img_wishlist_red);


//            authorImageView.setVisibility(isAuthorNeeded ? View.VISIBLE : View.GONE);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (onClickListener != null && position != RecyclerView.NO_POSITION) {
                        onClickListener.onPostClick(mArtSalesList.get(position));
                    }
                }
            });


//            authorImageView.setOnClickListener(v -> {
//                int position = getAdapterPosition();
//                if (onClickListener != null && position != RecyclerView.NO_POSITION) {
//                    onClickListener.onAuthorClick(getAdapterPosition(), v);
//                }
//            });

        }


    }

}
