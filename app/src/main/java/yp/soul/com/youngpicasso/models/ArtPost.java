package yp.soul.com.youngpicasso.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.github.thunder413.datetimeutils.DateTimeUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArtPost implements Parcelable {

    public static final Creator<ArtPost> CREATOR = new Creator<ArtPost>() {
        @Override
        public ArtPost createFromParcel(Parcel in) {
            return new ArtPost(in);
        }

        @Override
        public ArtPost[] newArray(int size) {
            return new ArtPost[size];
        }
    };
    public String id;
    public String title;
    public String description;
    public String createdDate;
    public String imagePath;
    public String imageTitle;
    public String user_id;
    public String username;
    public long watchersCount;
    public boolean hasComplain;
    public long itemType;
    public List<Like> likeList;

    public ArtPost() {
    }

    public List<Comment> comment_list;

    public ArtPost(String id, String title, String description, String imagePath, String user_id, String username,
                   long itemType) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.createdDate = DateTimeUtils.formatDate(new Date());
        this.imagePath = imagePath;

        this.user_id = user_id;
        this.username = username;
        this.watchersCount = 0;
        this.hasComplain = false;
        this.itemType = itemType;

    }

    protected ArtPost(Parcel in) {
        id = in.readString();
        title = in.readString();
        description = in.readString();
        createdDate = in.readString();
        imagePath = in.readString();
        imageTitle = in.readString();
        user_id = in.readString();
        username = in.readString();
        watchersCount = in.readLong();
        hasComplain = in.readByte() != 0;
        itemType = in.readLong();
        likeList = in.createTypedArrayList(Like.CREATOR);
        comment_list = in.createTypedArrayList(Comment.CREATOR);
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();

        result.put("title", title);
        result.put("description", description);
        result.put("createdDate", createdDate);
        result.put("imagePath", imagePath);
        result.put("imageTitle", imageTitle);
        result.put("user_id", user_id);
        result.put("username", user_id);

        result.put("watchersCount", watchersCount);
        result.put("hasComplain", hasComplain);
        result.put("createdDateText", DateTimeUtils.formatDate(createdDate));
        result.put("comment_list", comment_list);
        result.put("likeList", likeList);

        return result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getWatchersCount() {
        return watchersCount;
    }

    public void setWatchersCount(long watchersCount) {
        this.watchersCount = watchersCount;
    }

    public boolean isHasComplain() {
        return hasComplain;
    }

    public void setHasComplain(boolean hasComplain) {
        this.hasComplain = hasComplain;
    }

    public long getItemType() {
        return itemType;
    }

    public void setItemType(long itemType) {
        this.itemType = itemType;
    }

    public List<Like> getLikeList() {
        return likeList;
    }

    public void setLikeList(List<Like> likeList) {
        this.likeList = likeList;
    }

    public List<Comment> getComment_list() {
        return comment_list;
    }

    public void setComment_list(List<Comment> comment_list) {
        this.comment_list = comment_list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(createdDate);
        parcel.writeString(imagePath);
        parcel.writeString(imageTitle);
        parcel.writeString(user_id);
        parcel.writeString(username);
        parcel.writeLong(watchersCount);
        parcel.writeByte((byte) (hasComplain ? 1 : 0));
        parcel.writeLong(itemType);
        parcel.writeTypedList(likeList);
        parcel.writeTypedList(comment_list);
    }
}
