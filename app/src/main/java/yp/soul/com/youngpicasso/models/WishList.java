package yp.soul.com.youngpicasso.models;

public class WishList {
    public String id;
    public String post_id;

    public WishList() {
    }

    public WishList(String id, String post_id) {

        this.id = id;
        this.post_id = post_id;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }
}
