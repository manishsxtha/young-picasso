package yp.soul.com.youngpicasso.ArtSales;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.api.defaults.DefaultBootstrapBrand;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.hsalf.smilerating.SmileRating;

import java.util.ArrayList;
import java.util.List;

import yp.soul.com.youngpicasso.Comments.CommentsRatingRecyclerAdapter;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.CircularImageView;
import yp.soul.com.youngpicasso.Utils.ComplainActivity;
import yp.soul.com.youngpicasso.Utils.FilePaths;
import yp.soul.com.youngpicasso.Utils.ImageDetailActivity;
import yp.soul.com.youngpicasso.Utils.TextDataUtils;
import yp.soul.com.youngpicasso.Utils.UniversalImageLoader;
import yp.soul.com.youngpicasso.Utils.UserTypes;
import yp.soul.com.youngpicasso.helpers.BaseDetailActivity;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.models.ArtSales;
import yp.soul.com.youngpicasso.models.Comment;
import yp.soul.com.youngpicasso.models.CommentRatingMerge;
import yp.soul.com.youngpicasso.models.User;
import yp.soul.com.youngpicasso.models.WishList;

public class ArtSaleDetailActivity extends BaseDetailActivity {
    private static final String TAG = "EventDetailActivity";
    List<CommentRatingMerge> mCommentList;
    //widgets
    private Toolbar toolbar;
    private AlertDialog.Builder commentBuilder;
    private ImageView image_art_sale;
    private TextView tvTitle;
    private TextView tvPrice;
    private BootstrapButton labelArtSale;
    private LinearLayout layout_wishlist;
    private ImageView imageWishListOff;
    private ImageView imageWishListOn;
    private TextView textWishlist;
    private LinearLayout layout_interested;
    private CircularImageView imageUser;
    private SmileRating smile_rating;
    private TextInputEditText inputReview;
    private BootstrapButton btn_send;
    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    private CommentsRatingRecyclerAdapter adapter;
    //vars
    private FirebaseHelper mFirebaseHelper;
    private Context mContext = ArtSaleDetailActivity.this;
    private ArtSales artSales;
    private Comment comment;
    private ArrayList<Comment> comments;
    private TextView tv_desc;
    private TextView tv_author_name, tvAuthorCreatedDate;
    private ScrollView mScrollView;
    private float userRated = 0;
    private BootstrapButton btnAuthorArtAvail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_art_sale_detail);

        getDataFromIntent();

        setupWidgets();
        setupFirebase();
//        increaseWatcherCount();
        setupAdapter();
        setupCommentsList();
////        tempData();
//
        initSendComment();
        initWishList();
//
        setupToolbarAndStickyLayout();
//
        init();

    }

    private void initWishList() {
        mFirebaseHelper.getMyRef().child(getString(R.string.db_wishlist))
                .child(mFirebaseHelper.getAuth().getCurrentUser().getUid())
                .child(artSales.getId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                        for (DataSnapshot ds :
//                                dataSnapshot.getChildren()) {
                        if (dataSnapshot.exists()) {
                            WishList wishList = dataSnapshot.getValue(WishList.class);

                            if (wishList.getPost_id().equals(artSales.getId())) {
//                                showWishListRed(holder);
                                showWishListRed();
                            } else {
                                showNotInWishList();
                            }
                        }

//                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

        //Set click action for wishlist

        layout_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //database access and if complete then toggle red heart to normal
                if (imageWishListOn.getVisibility() == View.VISIBLE) {
                    imageWishListOff.setClickable(false);
                    imageWishListOn.setClickable(false);

                    mFirebaseHelper.getMyRef().child(getString(R.string.db_wishlist))
                            .child(mFirebaseHelper.getAuth().getCurrentUser().getUid())
                            .child(artSales.getId())
                            .removeValue(new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                    imageWishListOn.setClickable(true);
                                    showNotInWishList();

                                    mFirebaseHelper.decreaseUserWislistCount();
                                }
                            });

                } else {
                    imageWishListOff.setClickable(false);
                    imageWishListOn.setClickable(false);
                    String keyId = mFirebaseHelper.getMyRef().push().getKey();
                    User user = mFirebaseHelper.getSharedPreference().getUserInfo();
                    final WishList wishList = new WishList(keyId,
                            artSales.getId()
                    );

                    mFirebaseHelper.getMyRef().child(getString(R.string.db_wishlist))
                            .child(mFirebaseHelper.getAuth().getCurrentUser().getUid())
                            .child(artSales.getId())
                            .setValue(wishList, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                    showWishListRed();

                                    imageWishListOff.setClickable(true);
                                    mFirebaseHelper.increaseUserWislistCount();

                                }
                            });
                }

            }
        });

    }

    private void showWishListRed() {
        imageWishListOn.setVisibility(View.VISIBLE);
        imageWishListOff.setVisibility(View.GONE);
        textWishlist.setText(getString(R.string.added_to_wishlist));
    }

    private void showNotInWishList() {
        imageWishListOn.setVisibility(View.GONE);
        imageWishListOff.setVisibility(View.VISIBLE);
        textWishlist.setText(getString(R.string.add_to_wishlist));
    }

    //    private void increaseWatcherCount() {
//        long watcherIncrement = artSales.getWatchersCount() + 1;
//        mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_art_posts))
//                .child(artSales.getId())
//                .child(mContext.getString(R.string.db_field_art_watchcount))
//                .setValue(watcherIncrement);
//    }
//
    private void initSendComment() {
        mFirebaseHelper.checkIfRatingExists(artSales, new CheckRatingExistListener() {
            @Override
            public void onResult(Boolean ratingForUserExist, String postId) {
                if (ratingForUserExist) {
                    mFirebaseHelper.getUserRating(postId, new GetUserRatingListener() {
                        @Override
                        public void onResult(int userRating) {
                            Log.d(TAG, "onResult: user rating is " + userRating);

                            smile_rating.setSelectedSmile(userRating - 1);
                            userRated = userRating;
                        }
                    });

                }
            }
        });

        smile_rating.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                // level is from 1 to 5 (0 when none selected)
                // reselected is false when user selects different smiley that previously selected one
                // true when the same smiley is selected.
                // Except if it first time, then the value will be false.
                userRated = level;
            }
        });
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String textComment = inputReview.getText().toString();
                if (userRated != 0) {

                    mFirebaseHelper.checkIfRatingExists(artSales, new CheckRatingExistListener() {
                        @Override
                        public void onResult(Boolean ratingForUserExist, String postId) {
                            String keyId;

                            if (ratingForUserExist) {
                                keyId = postId;
                                mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_comments_rating))
                                        .child(artSales.getId())
                                        .child(keyId)
                                        .child("userRating")
                                        .setValue(userRated, new DatabaseReference.CompletionListener() {
                                            @Override
                                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                                mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_comments_rating))
                                                        .child(artSales.getId())
                                                        .child(keyId)
                                                        .child("comment_desc")
                                                        .setValue(textComment);
                                                if (ratingForUserExist) {
                                                    int i = 0;
                                                    for (CommentRatingMerge cmnt :
                                                            mCommentList) {
                                                        if (cmnt.getUser_id().equals(mFirebaseHelper.getAuth().getCurrentUser().getUid())) {
                                                            mCommentList.get(i).setUserRating(userRated);
                                                            mCommentList.get(i).setComment_desc(textComment);

                                                            break;
                                                        }
                                                        i++;
                                                    }
                                                }
                                                adapter.notifyDataSetChanged();
                                                mScrollView.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        mScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                                                    }
                                                }, 200);
                                            }
                                        });

                            } else {
                                keyId = mFirebaseHelper.getMyRef().push().getKey();
                                final CommentRatingMerge comment = new CommentRatingMerge(
                                        keyId,
                                        textComment,
                                        mFirebaseHelper.getAuth().getCurrentUser().getUid(),
                                        mFirebaseHelper.getSharedPreference().getUserInfo().getUsername(),
                                        mFirebaseHelper.getSharedPreference().getUserInfo().getAvatar_img_link(),
                                        userRated
                                );
                                Toast.makeText(mContext, "sending your comment..", Toast.LENGTH_SHORT).show();
                                mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_comments_rating))
                                        .child(artSales.getId())
                                        .child(keyId)
                                        .setValue(comment, new DatabaseReference.CompletionListener() {
                                            @Override
                                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {


                                                mCommentList.add(comment);

                                                adapter.notifyDataSetChanged();
                                                mScrollView.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        mScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                                                    }
                                                }, 200);
                                            }
                                        });
                            }

                            inputReview.setText("");
                        }
                    });
                    String keyId = mFirebaseHelper.getMyRef().push().getKey();

                }

                closeKeyboard();
            }
        });
    }

    private void setupCommentsList() {

        mFirebaseHelper.getCommentRatingList(artSales.getId(), new CommentRatingListener() {
            @Override
            public void onLoaded(List<CommentRatingMerge> mComments) {
                mCommentList.addAll(mComments);
                adapter.notifyDataSetChanged();
            }
        });


    }

    private void setupToolbarAndStickyLayout() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(artSales.getTitle());

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    //
    private void getDataFromIntent() {
        Intent intent = getIntent();
        artSales = intent.getParcelableExtra(getString(R.string.callingSalesDetail));
        if (artSales == null) {
            finish();
        }
    }

    /**
     * for saving the state of the activity on rotation changes
     *
     * @param outState
     */
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntArray("ARTICLE_SCROLL_POSITION",
                new int[]{mScrollView.getScrollX(), mScrollView.getScrollY()});
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final int[] position = savedInstanceState.getIntArray("ARTICLE_SCROLL_POSITION");

        mScrollView.post(new Runnable() {
            public void run() {
                mScrollView.scrollTo(position[0], position[1]);
            }
        });
    }


    private void setupFirebase() {
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
    }

    private void init() {

        tvTitle.setText(artSales.getTitle());


        UniversalImageLoader.setImage(artSales.getImagePath(),
                image_art_sale, null, "");
        tvPrice.setText("Rs. " + artSales.getPrice());

        //check availablity
        if (artSales.getAvailable()) {
            labelArtSale.setText(getString(R.string.for_sale));
            labelArtSale.setBootstrapBrand(DefaultBootstrapBrand.INFO);

        } else {
            labelArtSale.setText(getString(R.string.not_for_sale));
            labelArtSale.setBootstrapBrand(DefaultBootstrapBrand.DANGER);
        }

        //check if user is author
        if (mFirebaseHelper.getAuth().getCurrentUser().getUid().equals(artSales.getUser_id())) {
            btnAuthorArtAvail.setVisibility(View.VISIBLE);
            btnAuthorArtAvail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertForArtAvailablity();
                }
            });
        } else {
            btnAuthorArtAvail.setVisibility(View.GONE);
        }

        tv_author_name.setText(artSales.getUsername());

        tvAuthorCreatedDate.setText(
                TextDataUtils.getTimestampDifference2(artSales.getCreated_date()));

        //set user image
        UniversalImageLoader.setImage(mFirebaseHelper.getSharedPreference().getUserInfo().getAvatar_img_link(),
                imageUser, null, "");


        //press comments link
        layout_interested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mScrollView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        mScrollView.fullScroll(ScrollView.FOCUS_DOWN);
//                        inputReview.getParent().requestChildFocus(inputReview,inputReview);
                        scrollToView(mScrollView, inputReview);
                    }
                }, 200);
            }
        });


        //click on image to zoom
        image_art_sale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onImagePressed();
            }
        });


    }

    private void alertForArtAvailablity() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialog);
        builder.setTitle("Is art title " + artSales.getTitle() + " sold?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_art_sales))
                        .child(artSales.getId())
                        .child("available")
                        .setValue(false, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                labelArtSale.setBootstrapBrand(DefaultBootstrapBrand.DANGER);
                                labelArtSale.setText("Sold");
                                Toast.makeText(mContext, "Art (" + artSales.getTitle() + ") is set to sold", Toast.LENGTH_SHORT).show();
                            }
                        });

                dialog.cancel();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_art_sales))
                        .child(artSales.getId())
                        .child("available")
                        .setValue(true, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                labelArtSale.setBootstrapBrand(DefaultBootstrapBrand.INFO);
                                labelArtSale.setText("For sale");
                                Toast.makeText(mContext, "Art (" + artSales.getTitle() + ") is set to for sale", Toast.LENGTH_SHORT).show();
                            }
                        });

                dialog.cancel();
            }
        });
        builder.show();
    }

    /**
     * Used to scroll to the given view.
     *
     * @param scrollViewParent Parent ScrollView
     * @param view             View to which we need to scroll.
     */
    private void scrollToView(final ScrollView scrollViewParent, final View view) {
        // Get deepChild Offset
        Point childOffset = new Point();
        getDeepChildOffset(scrollViewParent, view.getParent(), view, childOffset);
        // Scroll to child.
        scrollViewParent.smoothScrollTo(0, childOffset.y);
    }

    /**
     * Used to get deep child offset.
     * <p/>
     * 1. We need to scroll to child in scrollview, but the child may not the direct child to scrollview.
     * 2. So to get correct child position to scroll, we need to iterate through all of its parent views till the main parent.
     *
     * @param mainParent        Main Top parent.
     * @param parent            Parent.
     * @param child             Child.
     * @param accumulatedOffset Accumulated Offset.
     */
    private void getDeepChildOffset(final ViewGroup mainParent, final ViewParent parent, final View child, final Point accumulatedOffset) {
        ViewGroup parentGroup = (ViewGroup) parent;
        accumulatedOffset.x += child.getLeft();
        accumulatedOffset.y += child.getTop();
        if (parentGroup.equals(mainParent)) {
            return;
        }
        getDeepChildOffset(mainParent, parentGroup.getParent(), parentGroup, accumulatedOffset);
    }


    private void popupForDelete(CommentRatingMerge commentUserMerge) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialog);
        builder.setTitle("Delete comment  " + commentUserMerge.getComment_desc() + " ?");
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_comments_rating))
                        .child(artSales.getId())
                        .child(commentUserMerge.getComment_id())
                        .removeValue(new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                mCommentList.remove(commentUserMerge);
                                adapter.notifyDataSetChanged();
                                Toast.makeText(mContext, "Deleted artSales: " + artSales.getTitle(), Toast.LENGTH_SHORT).show();
                            }
                        });

                dialog.cancel();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void popupForDeleteItem() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialog);
        builder.setTitle("Delete art item: " + artSales.getTitle() + " ?");
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mFirebaseHelper.getmStorageReference().child(FilePaths.FIREBASE_ART_SALE_STORAGE + artSales.getImageTitle())
                        .delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "onSuccess: File deleted succesfully.");
                        mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_art_sales))
                                .child(artSales.getId())
                                .removeValue(new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                        Toast.makeText(mContext, "Success!", Toast.LENGTH_SHORT).show();

                                        mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_comments_rating))
                                                .child(artSales.getId())
                                                .removeValue();
                                        mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_complain))
                                                .child(artSales.getId())
                                                .removeValue();
                                        finish();
                                    }
                                });

                        dialog.cancel();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Toast.makeText(mContext, "Error occured", Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.post_details_menu, menu);
//        menu.findItem(R.id.edit_post_action).setVisible(false);
//        menu.findItem(R.id.delete_post_action).setVisible(false);
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!artSales.getUser_id().equals(mFirebaseHelper.getAuth().getCurrentUser().getUid())) {
            menu.findItem(R.id.edit_post_action).setVisible(false);
            menu.findItem(R.id.delete_post_action).setVisible(false);
        }
        if (mFirebaseHelper.getSharedPreference().getUserInfo().getUser_type() == UserTypes.ADMIN) {
            menu.findItem(R.id.edit_post_action).setVisible(false);
            menu.findItem(R.id.delete_post_action).setVisible(true);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.complain_action:
                Intent intent = new Intent(this, ComplainActivity.class);
                intent.putExtra(mContext.getString(R.string.calling_complain__sales_intent), artSales);
                startActivity(intent);

                return true;
            case R.id.edit_post_action:
                Intent editIntent = new Intent(this, ArtSaleAddActivity.class);
                editIntent.putExtra(mContext.getString(R.string.callingSalesDetail), artSales);
                startActivity(editIntent);

                return true;
            case R.id.delete_post_action:
                popupForDeleteItem();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupWidgets() {

        commentBuilder = new AlertDialog.Builder(mContext);
        commentBuilder.setTitle("Delete comment?");

        mScrollView = findViewById(R.id.mScrollView);
        image_art_sale = findViewById(R.id.image_art_sale);
        tvTitle = findViewById(R.id.tvTitle);
        tvPrice = findViewById(R.id.tvPrice);
        labelArtSale = findViewById(R.id.labelArtSale);

        layout_wishlist = findViewById(R.id.layout_wishlist);
        imageWishListOff = findViewById(R.id.imageWishListOff);
        imageWishListOn = findViewById(R.id.imageWishListOn);
        textWishlist = findViewById(R.id.textWishlist);

        layout_interested = findViewById(R.id.layout_interested);

        tv_author_name = findViewById(R.id.tv_author_name);
        tvAuthorCreatedDate = findViewById(R.id.tvAuthorCreatedDate);
        tv_desc = findViewById(R.id.tvDesc);
        btnAuthorArtAvail = findViewById(R.id.btnAuthorArtAvail);

        imageUser = findViewById(R.id.imageUser);
        smile_rating = findViewById(R.id.smile_rating);

        inputReview = findViewById(R.id.inputReview);
        btn_send = findViewById(R.id.btn_send);

        recyclerView = findViewById(R.id.recyclerView);


    }

    private void setupAdapter() {
        mCommentList = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerView);
        manager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, true);

        recyclerView.setNestedScrollingEnabled(false);

        recyclerView.setLayoutManager(manager);

        adapter = new CommentsRatingRecyclerAdapter(mContext, mCommentList, new CommentsRatingRecyclerAdapter.OnCommentRemoveListener() {
            @Override
            public void onCommentRemove(CommentRatingMerge commentUserMerge) {
                popupForDelete(commentUserMerge);
            }
        });

        recyclerView.setAdapter(adapter);


    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void onImagePressed() {
        Intent intent = new Intent(this, ImageDetailActivity.class);
        intent.putExtra(ImageDetailActivity.IMAGE_URL_EXTRA_KEY, artSales.getImagePath());
        startActivity(intent);
    }

    public interface CommentRatingListener {
        void onLoaded(List<CommentRatingMerge> mCommentList);
    }

    public interface CheckRatingExistListener {
        void onResult(Boolean ratingForUserExist, String postId);
    }

    public interface GetUserRatingListener {
        void onResult(int userRating);
    }

}
