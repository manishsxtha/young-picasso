package yp.soul.com.youngpicasso.models;

import android.os.Parcel;
import android.os.Parcelable;

import static yp.soul.com.youngpicasso.Utils.TextDataUtils.getCurrentTimeStamp;

public class Notice implements Parcelable {
    public String notice_id;

    public String admin_name;

    public String notice_title;
    public String notice_desc;
    public String notice_date;

    public Notice() {
    }

    public Notice(String notice_id, String admin_name, String notice_title, String notice_desc) {

        this.notice_id = notice_id;
        this.admin_name = admin_name;
        this.notice_title = notice_title;
        this.notice_desc = notice_desc;
        this.notice_date = getCurrentTimeStamp();
    }

    protected Notice(Parcel in) {
        notice_id = in.readString();
        admin_name = in.readString();
        notice_title = in.readString();
        notice_desc = in.readString();
        notice_date = in.readString();
    }

    public static final Creator<Notice> CREATOR = new Creator<Notice>() {
        @Override
        public Notice createFromParcel(Parcel in) {
            return new Notice(in);
        }

        @Override
        public Notice[] newArray(int size) {
            return new Notice[size];
        }
    };

    public String getAdmin_name() {
        return admin_name;
    }

    public void setAdmin_name(String admin_name) {
        this.admin_name = admin_name;
    }

    public String getNotice_id() {
        return notice_id;
    }

    public void setNotice_id(String notice_id) {
        this.notice_id = notice_id;
    }

    public String getNotice_title() {
        return notice_title;
    }

    public void setNotice_title(String notice_title) {
        this.notice_title = notice_title;
    }

    public String getNotice_desc() {
        return notice_desc;
    }

    public void setNotice_desc(String notice_desc) {
        this.notice_desc = notice_desc;
    }

    public String getNotice_date() {
        return notice_date;
    }

    public void setNotice_date(String notice_date) {
        this.notice_date = notice_date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(notice_id);
        parcel.writeString(admin_name);
        parcel.writeString(notice_title);
        parcel.writeString(notice_desc);
        parcel.writeString(notice_date);
    }
}
