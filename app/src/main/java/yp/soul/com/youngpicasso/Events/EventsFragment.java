package yp.soul.com.youngpicasso.Events;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beardedhen.androidbootstrap.TypefaceProvider;

import yp.soul.com.youngpicasso.Home.HomeActivity;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.helpers.BaseFragment;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper;

import static yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper.getInstance;

public class EventsFragment extends BaseFragment {

    private static final String TAG = "NewsFragment";

    public static final int UPCOMING = 0;
    public static final int FINISHED = 1;

    private View view;
    private Context mContext;

    private LinearLayoutManager manager;

    private FirebaseHelper mFirebaseHelper;
    private FloatingActionButton fab_options;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_events, container, false);
        mContext = getContext();
        TypefaceProvider.registerDefaultIconSets();
        setupToolbar();
        setupViewPager();
        setupFirebase();

//
//        setupNewsList();
//
        setupFAB();

        return view;
    }

    private void setupViewPager() {
        EventsPagerAdapter adapter = new EventsPagerAdapter(getChildFragmentManager());

        adapter.addFragment(UpcomingEventsFragment.newInstance()); //index 0
        adapter.addFragment(FinishedEventsFragment.newInstance()); //index 1

        ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager_container);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setText("Upcoming");
        tabLayout.getTabAt(1).setText("Finished");

    }


    private void setupToolbar() {
        ((HomeActivity) mContext).getSupportActionBar().setTitle(getTitle());
    }

    private void setupFAB() {
        SharedPreferenceHelper preferenceHelper = getInstance(mContext);

        fab_options = view.findViewById(R.id.fab_filter);
        if ((int) preferenceHelper.getUserInfo().getUser_type() == 0) {

            fab_options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //start the add news activity
                    startActivity(new Intent(mContext, EventAddActivity.class));
                }
            });


        } else {
            fab_options.hide();
        }


    }


    private void setupFirebase() {
        mFirebaseHelper = new FirebaseHelper(mContext);
    }


    @Override
    public String getTitle() {
        return "Events";
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }
}
