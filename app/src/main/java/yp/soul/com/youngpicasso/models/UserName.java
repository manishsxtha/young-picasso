package yp.soul.com.youngpicasso.models;

public class UserName {
    public String user_id;
    public String username;

    public UserName() {
    }

    public String getUser_id() {

        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserName(String user_id, String username) {

        this.user_id = user_id;
        this.username = username;
    }
}
