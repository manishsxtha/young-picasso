package yp.soul.com.youngpicasso.Search;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.api.defaults.DefaultBootstrapBrand;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.models.User;


public class UserListAdapter extends ArrayAdapter<User> {

    private static final String TAG = "UserListAdapter";


    private LayoutInflater mInflater;
    private List<User> mUsers = null;
    private int layoutResource;
    private Context mContext;

    public UserListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<User> objects) {
        super(context, resource, objects);
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutResource = resource;
        this.mUsers = objects;
    }

    private String getUserTypeString(long userType, ViewHolder holder) {
        if (userType == 0) {
            holder.userType.setBootstrapBrand(DefaultBootstrapBrand.DANGER);
            return "Admin";
        }
        if (userType == 1) {
            holder.userType.setBootstrapBrand(DefaultBootstrapBrand.DANGER);
            return "Staff";
        }
        if (userType == 2) {
            holder.userType.setBootstrapBrand(DefaultBootstrapBrand.WARNING);
            return "Student";
        }
        if (userType == 3) {
            holder.userType.setBootstrapBrand(DefaultBootstrapBrand.INFO);
            return "Unregistered";
        }
        return "Unregistered";
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        final ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(layoutResource, parent, false);
            holder = new ViewHolder();


            holder.username = (TextView) convertView.findViewById(R.id.username);
            holder.userType = convertView.findViewById(R.id.userType);
            holder.email = (TextView) convertView.findViewById(R.id.email);
            holder.profileImage = (CircleImageView) convertView.findViewById(R.id.profile_image);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.username.setText(getItem(position).getUsername());
        long userType = getItem(position).getUser_type();

        holder.userType.setText(getUserTypeString(userType, holder));

        holder.email.setText(getItem(position).getEmail());

        ImageLoader imageLoader = ImageLoader.getInstance();

        imageLoader.displayImage(getItem(position).getAvatar_img_link(),
                holder.profileImage);


        return convertView;
    }

    private static class ViewHolder {

        BootstrapButton userType;
        TextView username, email;
        CircleImageView profileImage;
    }
}

























