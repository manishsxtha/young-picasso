package yp.soul.com.youngpicasso.models;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    public String user_id;
    public String username;
    public String avatar_img_link;
    public long user_type;
    public long postsCount;
    public long wishlistCount;



    public User(String user_id, String username, String avatar_img_link, long user_type, long postsCount, long wishlistCount, String email) {
        this.user_id = user_id;
        this.username = username;
        this.avatar_img_link = avatar_img_link;
        this.user_type = user_type;
        this.postsCount = postsCount;
        this.wishlistCount = wishlistCount;
        this.email = email;
    }

    public User(String user_id, String username, String avatar_img_link, String email, long userType) {

        this.user_id = user_id;
        this.username = username;
        this.avatar_img_link = avatar_img_link;
        this.email = email;
        this.user_type = userType;
        this.postsCount = 0;
        this.wishlistCount = 0;

    }

    protected User(Parcel in) {
        user_id = in.readString();
        username = in.readString();
        avatar_img_link = in.readString();
        user_type = in.readLong();
        postsCount = in.readLong();
        wishlistCount = in.readLong();
        email = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public long getPostsCount() {
        return postsCount;
    }

    public void setPostsCount(long postsCount) {
        this.postsCount = postsCount;
    }


    public long getUser_type() {
        return user_type;
    }

    public void setUser_type(long user_type) {
        this.user_type = user_type;
    }

    public String email;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User() {

    }

    public String getAvatar_img_link() {
        return avatar_img_link;
    }

    public void setAvatar_img_link(String avatar_img_link) {
        this.avatar_img_link = avatar_img_link;
    }

    public long getWishlistCount() {
        return wishlistCount;
    }

    public void setWishlistCount(long wishlistCount) {
        this.wishlistCount = wishlistCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(user_id);
        parcel.writeString(username);
        parcel.writeString(avatar_img_link);
        parcel.writeLong(user_type);
        parcel.writeLong(postsCount);
        parcel.writeLong(wishlistCount);
        parcel.writeString(email);
    }
}
