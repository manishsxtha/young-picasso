package yp.soul.com.youngpicasso.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import yp.soul.com.youngpicasso.Utils.TextDataUtils;

public class Event implements Parcelable {
    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };
    public String user_id;
    public String username;
    public String event_id;
    public String event_title;
    public String event_description;
    public String event_picture;
    public String event_start_date;
    public String event_end_date;
    public String start_time;
    public String end_time;
    public List<Like> likeList;
    public List<Comment> comment_list;

    public Event() {
    }

    public Event(String user_id, String username, String event_id, String event_title,
                 String event_description, String event_picture, String event_start_date, String event_end_date,
                 String start_time,
                 String end_time) {
        this.user_id = user_id;
        this.username = username;
        this.event_id = event_id;
        this.event_title = event_title;
        this.event_description = event_description;
        this.event_picture = event_picture;
        this.event_start_date = event_start_date;
        this.event_end_date = event_end_date;
        this.start_time = start_time;
        this.end_time = end_time;

    }

    public Event(String user_id, String username, String event_id, String event_title, String event_description, String event_picture) {
        this.user_id = user_id;
        this.username = username;
        this.event_id = event_id;
        this.event_title = event_title;
        this.event_description = event_description;
        this.event_picture = event_picture;
        event_start_date = TextDataUtils.getCurrentTimeStamp();
        event_end_date = TextDataUtils.getCurrentTimeStamp();
    }

    protected Event(Parcel in) {
        user_id = in.readString();
        username = in.readString();
        event_id = in.readString();
        event_title = in.readString();
        event_description = in.readString();
        event_picture = in.readString();
        event_start_date = in.readString();
        event_end_date = in.readString();
        start_time = in.readString();
        end_time = in.readString();

        likeList = in.createTypedArrayList(Like.CREATOR);
        comment_list = in.createTypedArrayList(Comment.CREATOR);
    }

    public static Creator<Event> getCREATOR() {
        return CREATOR;
    }

    public String getUser_id() {

        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEvent_id() {

        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getEvent_title() {
        return event_title;
    }

    public void setEvent_title(String event_title) {
        this.event_title = event_title;
    }

    public String getEvent_description() {
        return event_description;
    }

    public void setEvent_description(String event_description) {
        this.event_description = event_description;
    }

    public String getEvent_picture() {
        return event_picture;
    }

    public void setEvent_picture(String event_picture) {
        this.event_picture = event_picture;
    }

    public String getEvent_start_date() {
        return event_start_date;
    }

    public void setEvent_start_date(String event_start_date) {
        this.event_start_date = event_start_date;
    }

    public String getEvent_end_date() {
        return event_end_date;
    }

    public void setEvent_end_date(String event_end_date) {
        this.event_end_date = event_end_date;
    }

    public List<Like> getlikeList() {
        return likeList;
    }

    public void setlikeList(List<Like> likeList) {
        this.likeList = likeList;
    }

    public List<Comment> getComment_list() {
        return comment_list;
    }

    public void setComment_list(List<Comment> comment_list) {
        this.comment_list = comment_list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(user_id);
        parcel.writeString(username);
        parcel.writeString(event_id);
        parcel.writeString(event_title);
        parcel.writeString(event_description);
        parcel.writeString(event_picture);
        parcel.writeString(event_start_date);
        parcel.writeString(event_end_date);
        parcel.writeString(start_time);
        parcel.writeString(end_time);
        parcel.writeTypedList(likeList);
        parcel.writeTypedList(comment_list);
    }
}
