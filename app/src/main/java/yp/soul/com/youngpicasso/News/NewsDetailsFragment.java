package yp.soul.com.youngpicasso.News;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.helpers.BaseFragment;

public class NewsDetailsFragment extends BaseFragment {

    private View view;
    private Context mContext;
    private Bundle bundle;

    private TextView tv_notice_name, tv_date, tv_desc;


    public NewsDetailsFragment() {
        super();
        setArguments(new Bundle());

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.news_activity_detail, container, false);
        mContext = getContext();

//        setupWidgets();
//        if (checkBundle()){
//            Notice notice = bundle.getParcelable(mContext.getString(R.string.fragment_notice));
//
//
//            assert notice != null;
////            toolbar_notice_name.setText(notice.getNotice_title());
//            tv_notice_name.setText(notice.getNotice_title());
//            tv_desc.setText(notice.getNotice_desc());
//            tv_date.setText(notice.getNotice_date());
//
//
//
//        }

        return view;
    }

    private void setupWidgets() {
//        toolbar_notice_name = view.findViewById(R.id.toolbar_notice_name);
        tv_date = view.findViewById(R.id.notice_date);
        tv_desc = view.findViewById(R.id.notice_desc);
        tv_notice_name = view.findViewById(R.id.notice_title);

    }

    private boolean checkBundle() {
        try {

            bundle = getArguments();
            if (bundle != null) {
                return true;
            } else {
                Toast.makeText(mContext, "Got no bundle", Toast.LENGTH_SHORT).show();
                return false;
            }
        } catch (Exception e) {
            Toast.makeText(mContext, "Exception in bundle", Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    @Override
    public String getTitle() {
        return "Notice";
    }
}
