package yp.soul.com.youngpicasso.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import yp.soul.com.youngpicasso.Utils.TextDataUtils;

public class News implements Parcelable {
    public String user_id;

    public String news_id;
    public String news_title;
    public String news_desc;
    public String news_photo;
    public String dated_added;
    public List<Like> mUserLikes;

    public List<Comment> comment_list;
    public News() {
    }


    protected News(Parcel in) {
        user_id = in.readString();
        news_id = in.readString();
        news_title = in.readString();
        news_desc = in.readString();
        news_photo = in.readString();
        dated_added = in.readString();
        mUserLikes = in.createTypedArrayList(Like.CREATOR);
        comment_list = in.createTypedArrayList(Comment.CREATOR);
    }

    public static final Creator<News> CREATOR = new Creator<News>() {
        @Override
        public News createFromParcel(Parcel in) {
            return new News(in);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };

    public String getNews_id() {

        return news_id;
    }

    public void setNews_id(String news_id) {
        this.news_id = news_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNews_title() {
        return news_title;
    }

    public void setNews_title(String news_title) {
        this.news_title = news_title;
    }

    public String getNews_desc() {
        return news_desc;
    }

    public void setNews_desc(String news_desc) {
        this.news_desc = news_desc;
    }

    public String getNews_photo() {
        return news_photo;
    }

    public void setNews_photo(String news_photo) {
        this.news_photo = news_photo;
    }

    public String getDated_added() {
        return dated_added;
    }

    public void setDated_added(String dated_added) {
        this.dated_added = dated_added;
    }


    public List<Comment> getComment_list() {
        return comment_list;
    }

    public void setComment_list(List<Comment> comment_list) {
        this.comment_list = comment_list;
    }

    public List<Like> getmUserLikes() {
        return mUserLikes;
    }

    public void setmUserLikes(List<Like> mUserLikes) {
        this.mUserLikes = mUserLikes;
    }

    public News(String news_id, String news_title, String news_desc, String news_photo, String user_id) {

        this.news_id = news_id;
        this.news_title = news_title;
        this.news_desc = news_desc;
        this.news_photo = news_photo;
        this.dated_added = TextDataUtils.getCurrentTimeStamp();
        this.user_id = user_id;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(user_id);
        parcel.writeString(news_id);
        parcel.writeString(news_title);
        parcel.writeString(news_desc);
        parcel.writeString(news_photo);
        parcel.writeString(dated_added);
        parcel.writeTypedList(mUserLikes);
        parcel.writeTypedList(comment_list);
    }
}
