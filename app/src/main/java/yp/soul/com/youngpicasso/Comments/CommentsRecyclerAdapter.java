package yp.soul.com.youngpicasso.Comments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.UniversalImageLoader;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.helpers.OnCommentRemoveListener;
import yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper;
import yp.soul.com.youngpicasso.models.CommentUserMerge;

import static yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper.getInstance;

public class CommentsRecyclerAdapter extends RecyclerView.Adapter<CommentsRecyclerAdapter.ViewHolder> {


    private final FirebaseHelper mFirebaseHelper;
    private final SharedPreferenceHelper sharedPreferences;

    private Context mContext;

    private List<CommentUserMerge> mCommentsList;
    private OnCommentRemoveListener onCommentRemoveListener;

    public CommentsRecyclerAdapter(Context mContext, List<CommentUserMerge> mCommentsList,

                                   OnCommentRemoveListener listener) {

        this.mContext = mContext;
        this.mCommentsList = mCommentsList;

        onCommentRemoveListener = listener;

        sharedPreferences = getInstance(mContext);
        mFirebaseHelper = new FirebaseHelper(mContext);

    }

    @NonNull
    @Override
    public CommentsRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_comment, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final CommentsRecyclerAdapter.ViewHolder holder, final int position) {

        UniversalImageLoader.setImage(mCommentsList.get(position).getAvatar_img_link(),
                holder.mUserProfilePhoto, null, "");

        holder.mUsername.setText(mCommentsList.get(position).getUsername());
        holder.mComment.setText(mCommentsList.get(position).getComment_desc());
        holder.mUserCommentDate.setText(mCommentsList.get(position).getDate_created());

        if (!mCommentsList.get(position).getUser_id().equals(mFirebaseHelper.getAuth().getCurrentUser().getUid())) {
            holder.mUserCommentDelete.setVisibility(View.GONE);
        } else {
            holder.mUserCommentDelete.setVisibility(View.VISIBLE);
            holder.mUserCommentDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onCommentRemoveListener.onCommentRemoveListener(mCommentsList.get(holder.getAdapterPosition()));
                }
            });

        }
    }


    @Override
    public int getItemCount() {
        if (mCommentsList != null) {
            return mCommentsList.size();
        }
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mUsername, mComment, mUserCommentDate, mUserCommentDelete;
        private CircleImageView mUserProfilePhoto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mUsername = itemView.findViewById(R.id.username);
            mComment = itemView.findViewById(R.id.userComment);
            mUserCommentDate = itemView.findViewById(R.id.userCommentDate);

            mUserProfilePhoto = itemView.findViewById(R.id.profileImage);

            mUserCommentDelete = itemView.findViewById(R.id.userDeleteComment);

        }

    }
}
