package yp.soul.com.youngpicasso.helpers;

import yp.soul.com.youngpicasso.models.News;


public interface OnNewsSettingClickListener {
    void onNewsSettingClickListener(News news);
}
