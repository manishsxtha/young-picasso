package yp.soul.com.youngpicasso.helpers;

import yp.soul.com.youngpicasso.models.CommentUserMerge;

public interface OnCommentRemoveListener {
    public void onCommentRemoveListener(CommentUserMerge comment);
}
