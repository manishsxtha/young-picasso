package yp.soul.com.youngpicasso.Search;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import yp.soul.com.youngpicasso.Home.HomeActivity;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.helpers.BaseFragment;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.models.User;


public class SearchFragment extends BaseFragment {

    private static final String TAG = "SearchActivity";
    private static final int ACTIVITY_NUM = 1;

    private Context mContext;

    //widgets
    private EditText mSearchParam;
//    private ListView mListView;

    //vars
    private List<User> mUserList, mAllUserList;
    //    private UserListAdapter mAdapter;
    private View view;
    private FirebaseHelper mFirebaseHelper;
    private FloatingActionButton fab_filter;

    private String fragTitle = "SearchFragment";
    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    private UserRecylcerAdapter adapter;

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((HomeActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle("Search");


    }

    @Override
    public String getTitle() {
        return fragTitle;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateview: started.");
        view = inflater.inflate(R.layout.activity_search, container, false);

        mContext = getContext();
        mSearchParam = view.findViewById(R.id.search);
//        mListView = view.findViewById(R.id.listView);

        fab_filter = view.findViewById(R.id.fab_filter);

        ((HomeActivity) mContext).hideSoftKeyboard();

        initTextListener();


        setupFirebase();
        setupAdapter();
        initShowUsers();
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
//        mUserList.clear();
//        mAllUserList.clear();
    }

    private void setupFirebase() {
        mFirebaseHelper = new FirebaseHelper(mContext);
    }


    private void initShowUsers() {

        Query query = mFirebaseHelper.getMyRef().child("users");
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                    Log.d(TAG, "onDataChange: found user:" + singleSnapshot.getValue(User.class).toString());
                    if (!singleSnapshot.getValue(User.class).getUser_id().equals(mFirebaseHelper.
                            getAuth().getCurrentUser().getUid())) {

                        mUserList.add(singleSnapshot.getValue(User.class));
                    }
                    //update the users list view

                }

                mAllUserList.addAll(mUserList);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setupAdapter() {
        Log.d(TAG, "updateUsersList: updating users list");

        mAllUserList = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
//        manager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        manager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(manager);

        adapter = new UserRecylcerAdapter(mContext, mUserList);

        recyclerView.setAdapter(adapter);
//        mAllUserList = new ArrayList<>();
//        mAdapter = new UserListAdapter(mContext, R.layout.layout_user_listitem, mAllUserList);
//
//        mListView.setAdapter(mAdapter);
//
//        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
////                ProfileFragment profileFragment = new ProfileFragment();
////                Bundle bundle = new Bundle();
////                bundle.putParcelable("User",mAllUserList.get(position));
////                profileFragment.setArguments(bundle);
////                ((HomeActivity)mContext).addFragments(profileFragment);
//
//                Intent intent = new Intent(mContext, ProfileActivity.class);
//                intent.putExtra(mContext.getString(R.string.activity_profile), mAllUserList.get(position));
//                startActivity(intent);
//            }
//        });
    }


    private void initTextListener() {
        Log.d(TAG, "initTextListener: initializing");

        mUserList = new ArrayList<>();

        mSearchParam.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = mSearchParam.getText().toString().toLowerCase(Locale.getDefault());
                searchForMatch(text);
                adapter.notifyDataSetChanged();

            }
        });
    }

    private void searchForMatch(String keyword) {
        Log.d(TAG, "searchForMatch: searching for a match: " + keyword);
        //update the users list view
        if (keyword.length() == 0) {
            mAllUserList.addAll(mUserList);
        } else {
            for (User user :
                    mAllUserList) {
                if (!user.getUsername().contains(keyword) || !user.getEmail().contains(keyword)) {
                    mAllUserList.remove(user);
                }
            }
        }
    }


}
