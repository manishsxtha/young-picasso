package yp.soul.com.youngpicasso.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Like implements Parcelable {
    public String user_id;
    public String username;
    public String like_id;

    public Like() {
    }

    public Like(String user_id, String username, String like_id) {

        this.user_id = user_id;
        this.username = username;
        this.like_id = like_id;
    }

    protected Like(Parcel in) {
        user_id = in.readString();
        username = in.readString();
        like_id = in.readString();
    }

    public static final Creator<Like> CREATOR = new Creator<Like>() {
        @Override
        public Like createFromParcel(Parcel in) {
            return new Like(in);
        }

        @Override
        public Like[] newArray(int size) {
            return new Like[size];
        }
    };

    public String getUser_id() {

        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLike_id() {
        return like_id;
    }

    public void setLike_id(String like_id) {
        this.like_id = like_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(user_id);
        dest.writeString(username);
        dest.writeString(like_id);
    }
}
