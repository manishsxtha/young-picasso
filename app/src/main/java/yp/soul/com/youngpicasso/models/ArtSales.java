package yp.soul.com.youngpicasso.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.github.thunder413.datetimeutils.DateTimeUtils;

import java.util.Date;

public class ArtSales implements Parcelable {
    public static final Creator<ArtSales> CREATOR = new Creator<ArtSales>() {
        @Override
        public ArtSales createFromParcel(Parcel in) {
            return new ArtSales(in);
        }

        @Override
        public ArtSales[] newArray(int size) {
            return new ArtSales[size];
        }
    };
    public String id;
    public String title;
    public String description;
    public String created_date;

    public String imageTitle;

    protected ArtSales(Parcel in) {
        id = in.readString();
        title = in.readString();
        description = in.readString();
        created_date = in.readString();
        user_id = in.readString();
        username = in.readString();
        imagePath = in.readString();
        imageTitle = in.readString();
        price = in.readString();
        itemType = in.readLong();
        byte tmpIsAvailable = in.readByte();
        isAvailable = tmpIsAvailable == 0 ? null : tmpIsAvailable == 1;
        byte tmpHasComplain = in.readByte();
        hasComplain = tmpHasComplain == 0 ? null : tmpHasComplain == 1;
        watchersCount = in.readLong();
        commentsCount = in.readLong();
    }

    public String user_id;
    public String username;
    public String imagePath;
    public ArtSales(String id, String title, String description,
                    String user_id, String username, String price, String imagePath, String imageTitle,
                    long itemType) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.user_id = user_id;
        this.username = username;
        this.price = price;
        this.imagePath = imagePath;
        this.imageTitle = imageTitle;
        this.itemType = itemType;
        this.watchersCount = 0;
        this.commentsCount = 0;
        this.hasComplain = false;
        isAvailable = true;
        this.created_date = DateTimeUtils.formatDate(new Date());

    }

    public String price;
    public long itemType;
    public Boolean isAvailable;
    public Boolean hasComplain;
    public long watchersCount;
    public long commentsCount;

    public ArtSales() {
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public long getWatchersCount() {
        return watchersCount;
    }

    public void setWatchersCount(long watchersCount) {
        this.watchersCount = watchersCount;
    }

    public long getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(long commentsCount) {
        this.commentsCount = commentsCount;
    }

    public Boolean getHasComplain() {
        return hasComplain;
    }

    public void setHasComplain(Boolean hasComplain) {
        this.hasComplain = hasComplain;
    }

    public long getItemType() {
        return itemType;
    }

    public void setItemType(long itemType) {
        this.itemType = itemType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(created_date);
        parcel.writeString(user_id);
        parcel.writeString(username);
        parcel.writeString(imagePath);
        parcel.writeString(imageTitle);

        parcel.writeString(price);
        parcel.writeLong(itemType);
        parcel.writeByte((byte) (isAvailable == null ? 0 : isAvailable ? 1 : 2));
        parcel.writeByte((byte) (hasComplain == null ? 0 : hasComplain ? 1 : 2));
        parcel.writeLong(watchersCount);
        parcel.writeLong(commentsCount);
    }
}

