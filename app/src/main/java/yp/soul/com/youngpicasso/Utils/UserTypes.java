package yp.soul.com.youngpicasso.Utils;

public class UserTypes {
    public static final int ADMIN = 0;
    public static final int STAFF = 1;
    public static final int STUDENT = 2;
    public static final int NORMAL = 3;

    public static final String STR_STAFF = "STAFF";
    public static final String STR_STUDENT = "Student";
    public static final String STR_NORMAL = "Normal User";
}
