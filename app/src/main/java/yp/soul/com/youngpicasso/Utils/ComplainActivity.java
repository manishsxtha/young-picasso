package yp.soul.com.youngpicasso.Utils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;

import yp.soul.com.youngpicasso.Home.HomeActivity;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.models.ArtPost;
import yp.soul.com.youngpicasso.models.ArtSales;

public class ComplainActivity extends AppCompatActivity {

    private static final String TAG = "ComplainActivity";
    private Context mContext = ComplainActivity.this;
    private FirebaseHelper mFirebaseHelper;
    private TextView textViewPostTitle;
    private BootstrapButton btn_send;
    private EditText editTextComplainDesc;
    private ArtPost artPost;
    private ArtSales artSale;
    private int setComplainFrom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complain);

        setupFirebase();
        setupWidgets();
        getDataFromIntent();
        setupToolbar();


        sendComplaint();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Send Complaint");

        toolbar.setNavigationOnClickListener(view -> finish());
    }

    private void sendComplaint() {
        btn_send.setOnClickListener(view -> {
            String complaintDesc = editTextComplainDesc.getText().toString();

            if (TextDataUtils.isEmpty(complaintDesc)) {
                Toast.makeText(mContext, "Complaint field cannot be empty", Toast.LENGTH_SHORT).show();

            } else {

                if (setComplainFrom == HomeActivity.ART_GALLERY_FRAGMENT) {
                    mFirebaseHelper.savePostComplain(artPost, complaintDesc, isSuccess -> {
                        if (isSuccess) {
                            Toast.makeText(mContext, "Your complaint has been logged and will be inspected by the admin", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    mFirebaseHelper.savePostComplainSale(artSale, complaintDesc, isSuccess -> {
                        if (isSuccess) {
                            Toast.makeText(mContext, "Your complaint has been logged and will be inspected by the admin", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }

        });
    }

    private void setupWidgets() {
        textViewPostTitle = findViewById(R.id.textViewPostTitle);
        editTextComplainDesc = findViewById(R.id.editTextComplainDesc);
        btn_send = findViewById(R.id.btn_send);
    }

    private void setupFirebase() {
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
    }

    private void getDataFromIntent() {
        Intent in = getIntent();

        if (in.hasExtra(mContext.getString(R.string.calling_complain_intent))) {
            setComplainFrom = HomeActivity.ART_GALLERY_FRAGMENT;
            artPost = in.getParcelableExtra(mContext.getString(R.string.calling_complain_intent));
            Log.e(TAG, "getDataFromIntent: " + artPost.getTitle());
            textViewPostTitle.setText(artPost.getTitle().toString());

        } else if (in.hasExtra(mContext.getString(R.string.calling_complain__sales_intent))) {
            setComplainFrom = HomeActivity.ART_SALES_FRAGMENT;
            artSale = in.getParcelableExtra(mContext.getString(R.string.calling_complain__sales_intent));
            Log.e(TAG, "getDataFromIntent: " + artSale.getTitle());
            textViewPostTitle.setText(artSale.getTitle().toString());
        } else {
            Toast.makeText(mContext, mContext.getString(R.string.error_general), Toast.LENGTH_SHORT).show();
            finish();
        }
    }


    public interface OnComplainPostListener {
        void onComplete(boolean isSuccess);
    }
}
