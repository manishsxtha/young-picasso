package yp.soul.com.youngpicasso.News;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import de.hdodenhof.circleimageview.CircleImageView;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.FilePaths;
import yp.soul.com.youngpicasso.Utils.ImageManager;
import yp.soul.com.youngpicasso.Utils.Permissions;
import yp.soul.com.youngpicasso.Utils.TextDataUtils;
import yp.soul.com.youngpicasso.Utils.UniversalImageLoader;
import yp.soul.com.youngpicasso.Utils.VerifyPermissions;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.models.News;

public class NewsAddActivity extends AppCompatActivity {
    private static final String TAG = "NewsAddActivity";
    private static final String NEWS_TITLE = "Add News";

    private Context mContext = NewsAddActivity.this;

    private EditText inputNewsTitle, inputNewsDesc;
    private CircleImageView newsImage;
    private SwitchCompat switch_new_notes_notify;
    private AppCompatButton btnNewsImage, btnImageClear, btn_send;
    private RelativeLayout relImageSelection;
    private View dark_background;
    private ProgressBar progressBar;

    private boolean hasImage = false;
    String news_title, news_desc, news_photo;
    public static final String TEMP_IMAGE = "https://firebasestorage.googleapis.com/v0/b/young-picasso.appspot.com/o/news%2F10636111_317544761750405_4932389066159121159_n.jpg?alt=media&token=7d11e9d5-de97-42e6-94a3-e934a1ba8a12";
    private FirebaseHelper mFirebaseHelper;
    private int mPhotoUploadProgress = 0;

    private boolean isEditNews = false;
    private News news;
    private String keyId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_add);

        setupFirebase();
        setupToolbar();
        setupWidgets();

        checkCallingIntent();
        initImageSwitch();

        initGetImageFromGallery();
        initPostNews();

    }

    private void checkCallingIntent() {
        Intent in = getIntent();
        if (in.hasExtra(mContext.getString(R.string.calling_news_intent))) {

            news = in.getParcelableExtra(mContext.getString(R.string.calling_news_intent));
            inputNewsTitle.setText(news.getNews_title());
            inputNewsDesc.setText(news.getNews_desc());
            switch_new_notes_notify.setChecked(false);
            relImageSelection.setVisibility(View.VISIBLE);

            UniversalImageLoader.setImage(news.getNews_photo(), newsImage, null, "");

            isEditNews = true;
            hasImage = true;
        }
    }

    private void setupFirebase() {
        mFirebaseHelper = new FirebaseHelper(mContext);
    }

    private void initGetImageFromGallery() {
        btnNewsImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VerifyPermissions verifyPermissions = new VerifyPermissions(mContext, NewsAddActivity.this);
                if (verifyPermissions.checkPermissionsArray(Permissions.PERMISSIONS)) {
                    Log.d(TAG, "onClick: permission granted");
                    Intent intent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 0);
                } else {
                    Log.d(TAG, "onClick: permission not granted, verifying..");
                    verifyPermissions.verifyPermissionsArray(Permissions.PERMISSIONS);
                }

            }
        });

        btnImageClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newsImage.setImageDrawable(null);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Uri targetUri = data.getData();
            news_photo = targetUri.toString();
            try {
                Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(targetUri));
                newsImage.setImageBitmap(bitmap);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void initPostNews() {
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                news_title = inputNewsTitle.getText().toString();
                news_desc = inputNewsDesc.getText().toString();
                if (validateForm()) {

                    if (hasImage) {
                        dark_background.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.VISIBLE);
                        saveNewsPost(news_photo);
                    } else {
                        saveNewsPost(TEMP_IMAGE);
                    }
                } else {
                    Toast.makeText(mContext, "Some fields are not set correctly.", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void saveNewsPost(final String imageUrl) {
        //check if it is new or old
        if (isEditNews) {
            keyId = news.getNews_id();
        } else {
            keyId = mFirebaseHelper.getMyRef().push().getKey();
        }
        //check if contains image is checked if not checked then put temp image url
        if (!hasImage) {
            News news = new News(keyId, news_title, news_desc, TEMP_IMAGE,
                    mFirebaseHelper.getAuth().getCurrentUser().getUid());
            mFirebaseHelper.getMyRef().child("news")
                    .child(keyId)
                    .setValue(news, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                            Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
                            dark_background.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            finish();
                        }
                    });
        } else {


            Bitmap bitmap = ((BitmapDrawable) newsImage.getDrawable()).getBitmap();
            //first upload photo to firebase and get its url
            final StorageReference newsStorage = mFirebaseHelper.getmStorageReference().child(FilePaths.FIREBASE_NEWS_STORAGE
                    + "/photo-" + keyId);

//        Bitmap bitmap = null;
//        try {
//
//            File file = new File(imageUrl);
//            bitmap = new Resizer(mContext)
//                    .setTargetLength(400)
//                    .setSourceImage(file)
//                    .getResizedBitmap();
//        } catch (IOException e) {
//            e.printStackTrace();
//            dark_background.setVisibility(View.GONE);
//            progressBar.setVisibility(View.GONE);
//        }
            byte[] data = ImageManager.getBytesFromBitmap(bitmap, 90);
            UploadTask uploadTask = newsStorage.putBytes(data);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    Toast.makeText(mContext, "Image upload failed", Toast.LENGTH_SHORT).show();
                    dark_background.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                    if (progress - 30 > mPhotoUploadProgress) {
                        Toast.makeText(mContext, "Photo upload progress: " + String.format("%.0f", progress), Toast.LENGTH_SHORT).show();
                    }
                    Log.d(TAG, "onProgress: progress: " + String.format("%.0f", progress) + " % done!");

                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                    // ...
                    Task<Uri> firebaseUrl = newsStorage.getDownloadUrl();
                    while (!firebaseUrl.isSuccessful()) ;

                    Uri downloadUrl = firebaseUrl.getResult();
                    //after uploading photo
                    News news = new News(keyId, news_title, news_desc, downloadUrl.toString(),
                            mFirebaseHelper.getAuth().getCurrentUser().getUid());
                    mFirebaseHelper.getMyRef().child("news")
                            .child(keyId)
                            .setValue(news, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                    Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
                                    dark_background.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.GONE);
                                    finish();
                                }
                            });
                }
            });
        }

    }

    private boolean validateForm() {
        boolean valid = true;

        if (TextDataUtils.isEmpty(news_title)) {
            inputNewsTitle.setHighlightColor(ContextCompat.getColor(mContext, R.color.red_normal));
            valid = false;
        }


        if (TextDataUtils.isEmpty(news_desc)) {
            inputNewsTitle.setBackgroundColor(ContextCompat.getColor(mContext, R.color.red_normal));
            valid = false;
        }
        if (hasImage) {
            if (newsImage.getDrawable() == null) {

                valid = false;
            }
        }


        return valid;
    }


    private void initImageSwitch() {
        switch_new_notes_notify.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    relImageSelection.setVisibility(View.VISIBLE);
                    hasImage = true;
                } else {
                    relImageSelection.setVisibility(View.GONE);
                    hasImage = false;
                }
            }
        });
    }

    private void setupWidgets() {
        inputNewsTitle = findViewById(R.id.inputNewsTitle);
        inputNewsDesc = findViewById(R.id.inputNewsDesc);

        relImageSelection = findViewById(R.id.relImageSelection);
        switch_new_notes_notify = findViewById(R.id.switch_new_notes_notify);
        btnNewsImage = findViewById(R.id.btnNewsImage);
        newsImage = findViewById(R.id.newsImage);
        btnImageClear = findViewById(R.id.btnImageClear);

        btn_send = findViewById(R.id.btn_send);
        dark_background = findViewById(R.id.dark_background);
        progressBar = findViewById(R.id.progressBar);

        switch_new_notes_notify.setChecked(false);
        relImageSelection.setVisibility(View.GONE);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(NEWS_TITLE);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

}
