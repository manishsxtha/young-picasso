package yp.soul.com.youngpicasso.UserProfile;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.helpers.BaseFragment;

public class ProfileFragment extends BaseFragment {

    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        return view;
    }

    @Override
    public String getTitle() {
        return "Profile";
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("User Profile");
    }
}
