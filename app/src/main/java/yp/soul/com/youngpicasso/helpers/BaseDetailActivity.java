package yp.soul.com.youngpicasso.helpers;

import android.support.v7.app.AppCompatActivity;

public abstract class BaseDetailActivity extends AppCompatActivity {

    protected abstract void onImagePressed();
}
