package yp.soul.com.youngpicasso.Utils;

import android.os.Environment;

public class FilePaths {
    public static final String FIREBASE_EVENT_STORAGE = "events/";
    public static final String FIREBASE_ART_SALE_STORAGE = "art_sales/";
    public static String FIREBASE_ART_STORAGE = "art_posts/";
    //storage/emulated/0
    public String ROOT_DIR = Environment.getExternalStorageDirectory().getPath();

    public String PICTURES = ROOT_DIR + "/Pictures";
    public String CAMERA = ROOT_DIR + "/DCIM/";

    public static final String FIREBASE_NEWS_STORAGE = "news/";

    public String STORIES = ROOT_DIR + "/Stories";

    public String FIREBASE_STORY_STORAGE = "stories/users";
    public String FIREBASE_IMAGE_STORAGE = "photos/users/";

}
