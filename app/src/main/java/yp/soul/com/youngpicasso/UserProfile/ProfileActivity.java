package yp.soul.com.youngpicasso.UserProfile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.CircularImageView;
import yp.soul.com.youngpicasso.Utils.UniversalImageLoader;
import yp.soul.com.youngpicasso.Utils.UserTypes;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.models.User;

//import butterknife.BindView;
//import butterknife.ButterKnife;

public class ProfileActivity extends AppCompatActivity {
    private static final String TAG = "ProfileActivity";
    private Context mContext = ProfileActivity.this;


    //    @BindView(R.id.profile_image)
    private CircularImageView profile_image;
    //    @BindView(R.id.username)
    private TextView username;
    private TextView tvPosts, tvLikes;
    private RelativeLayout relAdminApproveLayout;
    //    @BindView(R.id.btn_staff)
    private Button btn_staff;
    //    @BindView(R.id.btn_student)
    private Button btn_student;
    //    @BindView(R.id.btn_normal_user)
    private Button btn_normal_user;


    private FirebaseHelper firebaseHelper;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_v2);

//        ButterKnife.bind(this);

        setupToolbar();

        Intent intent = getIntent();

        if (intent != null) {
            firebaseHelper = new FirebaseHelper(mContext);
            setupWidgets();
            if (intent.hasExtra(mContext.getString(R.string.activity_profile))) {
                user = intent.getParcelableExtra(mContext.getString(R.string.activity_profile));
                getSupportActionBar().setTitle(user.getUsername());


            }
        }

        checkIfAdmin();
        init();
    }

    private void checkIfAdmin() {
        if (firebaseHelper.getSharedPreference().getUserInfo().getUser_type() != UserTypes.ADMIN) {
            relAdminApproveLayout.setVisibility(View.GONE);
        }


    }


    private void init() {

        UniversalImageLoader.setImage(user.getAvatar_img_link(), profile_image, null, "");
        username.setText(user.getUsername());

        firebaseHelper.getUserPostsCount(user.getUser_id(), new PostsCount() {
            @Override
            public void onResult(long count) {
                tvPosts.setText(String.valueOf(count));
            }
        });

        firebaseHelper.getUserLikesCount(user.getUser_id(), new LikesCount() {
            @Override
            public void onResult(long count) {
                tvLikes.setText(String.valueOf(count));
            }
        });
        switch ((int) user.getUser_type()) {
            case UserTypes
                    .STAFF:
                resetBtn();
                setUserButton(btn_staff, UserTypes.STR_STAFF);
                break;
            case UserTypes
                    .STUDENT:
                resetBtn();
                setUserButton(btn_student, UserTypes.STR_STUDENT);
                break;
            case UserTypes
                    .NORMAL:
                resetBtn();
                setUserButton(btn_normal_user, UserTypes.STR_NORMAL);
                break;

        }
        btn_staff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firebaseHelper.setUserAsStaff(user.getUser_id());
                resetBtn();
                setUserButton(btn_staff, UserTypes.STR_STAFF);
            }
        });

        btn_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firebaseHelper.setUserAsStudent(user.getUser_id());
                resetBtn();
                setUserButton(btn_student, UserTypes.STR_STUDENT);
            }
        });

        btn_normal_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firebaseHelper.setUserAsNormal(user.getUser_id());
                resetBtn();
                setUserButton(btn_normal_user, UserTypes.STR_NORMAL);
            }
        });
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void setupWidgets() {
        profile_image = findViewById(R.id.profile_image);
        username = findViewById(R.id.username);

        tvPosts = findViewById(R.id.tvPosts);
        tvLikes = findViewById(R.id.tvLikes);

        relAdminApproveLayout = findViewById(R.id.relAdminApproveLayout);

        btn_staff = findViewById(R.id.btn_staff);
        btn_student = findViewById(R.id.btn_student);
        btn_normal_user = findViewById(R.id.btn_normal_user);

    }

    private void setUserButton(Button btn, String user) {
        btn.setBackgroundColor(ContextCompat.getColor(mContext, R.color.green));
        btn.setText("User is " + user);
    }

    private void resetBtn() {
        btn_normal_user.setBackgroundColor(ContextCompat.getColor(mContext, R.color.blue));
        btn_staff.setBackgroundColor(ContextCompat.getColor(mContext, R.color.blue));
        btn_student.setBackgroundColor(ContextCompat.getColor(mContext, R.color.blue));

        btn_staff.setText("Staff");
        btn_student.setText("Student");
        btn_normal_user.setText("Normal User");
    }

    public interface PostsCount {
        public void onResult(long count);
    }

    public interface LikesCount {
        void onResult(long count);
    }
}
