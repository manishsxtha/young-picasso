package yp.soul.com.youngpicasso.helpers;

import yp.soul.com.youngpicasso.models.Event;


public interface OnEventSettingClickListener {
    void onEventSettingClickListener(Event event);
}
