package yp.soul.com.youngpicasso.News;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.thunder413.datetimeutils.DateTimeUtils;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;

import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.UniversalImageLoader;
import yp.soul.com.youngpicasso.Utils.UserTypes;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.helpers.OnNewsClickListener;
import yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper;
import yp.soul.com.youngpicasso.models.Comment;
import yp.soul.com.youngpicasso.models.Like;
import yp.soul.com.youngpicasso.models.News;
import yp.soul.com.youngpicasso.models.User;

import static yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper.getInstance;

public class NewsRecyclerAdapter extends RecyclerView.Adapter<NewsRecyclerAdapter.ViewHolder> {


    private final FirebaseHelper mFirebaseHelper;
    public final SharedPreferenceHelper sharedPreferences;
    private final List<User> mNewsByUserList;
    private List<News> mNewsList;
    private Context context;

    private OnNewsClickListener onNewsClickListener;
    private OnItemSelectedListener itemSelectedListener;
    private List<Like> mLikeList;
    private List<Comment> comment;


    public NewsRecyclerAdapter(Context context, List<News> mNewsList,
                               List<User> mNewsByUserList, OnNewsClickListener listener,
                               OnItemSelectedListener itemSelectedListener) {

        this.context = context;
        this.mNewsList = mNewsList;
        this.mNewsByUserList = mNewsByUserList;

        this.onNewsClickListener = listener;
        this.itemSelectedListener = itemSelectedListener;

        mFirebaseHelper = new FirebaseHelper(context);
        sharedPreferences = getInstance(context);

    }

    @NonNull
    @Override
    public NewsRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_news_feed, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final NewsRecyclerAdapter.ViewHolder holder, final int position) {

        final News news = mNewsList.get(holder.getAdapterPosition());
        holder.tv_news_title.setText(news.getNews_title());
        UniversalImageLoader.setImage(news.getNews_photo(),
                holder.post_image, null, "");
        holder.tv_news_desc.setText(news.getNews_desc());
        holder.image_time_posted.setText(
                DateTimeUtils.getTimeAgo(context, news.getDated_added()));


        prepareLikes(holder, position);
        prepareComments(holder, position);
    }

    private void prepareComments(ViewHolder holder, final int position) {
        //prepare comments count
        comment = mNewsList.get(position).getComment_list();

        if (comment.size() != 0) {
            int comment_size = comment.size();
            holder.tv_comment_count.setText(String.valueOf(comment_size));
            holder.image_comments_link.setText("View all " + comment_size + " comments");
        } else {
            holder.tv_comment_count.setText("0");
            holder.image_comments_link.setText("Be the first to comment.");
        }

    }

    private void prepareLikes(final ViewHolder holder, final int position) {
        //check likes and prepare likes string
        Boolean mLikedByCurrentUser = false;
        StringBuilder mUsersName = new StringBuilder();
        mLikeList = mNewsList.get(position).getmUserLikes();
        //check if there are no likes for that news
        if (mLikeList.size() != 0) {

            holder.tv_likes_count.setText(String.format("%d", mNewsList.get(position).getmUserLikes().size()));
            for (Like like :
                    mNewsList.get(position).getmUserLikes()) {
                mUsersName.append(like.getUsername());
                mUsersName.append(",");
                if (mFirebaseHelper.getAuth().getCurrentUser().getUid().equals(like.getUser_id())) {
                    mLikedByCurrentUser = true;
                }
            }
            if (mLikedByCurrentUser) {
                holder.image_heart_red.setVisibility(View.VISIBLE);
                holder.image_heart.setVisibility(View.GONE);
                holder.tv_likes_count.setTextColor(ContextCompat.getColor(context, R.color.red_normal));
            } else {
                holder.image_heart_red.setVisibility(View.GONE);
                holder.image_heart.setVisibility(View.VISIBLE);
                holder.tv_likes_count.setTextColor(ContextCompat.getColor(context, R.color.black));
            }

            holder.image_likes.setText(getLikesString(mUsersName));
        } else {
            mLikeList = new ArrayList<>();
            holder.tv_likes_count.setText("0");
            holder.image_heart_red.setVisibility(View.GONE);
            holder.image_heart.setVisibility(View.VISIBLE);

            holder.image_likes.setText("No one has liked it yet.");
        }


        // handle likes on click events

        holder.image_heart_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //database access and if complete then toggle red heart to normal
                holder.image_heart_red.setClickable(false);
                holder.image_heart.setClickable(false);
                News news = mNewsList.get(position);
                Like userLike = new Like();
                for (Like like :
                        news.getmUserLikes()) {
                    if (like.getUser_id().equals(mFirebaseHelper.getAuth().getCurrentUser().getUid())) {
                        userLike = like;
                        break;
                    }
                }
                final Like usertemp = userLike;
                mFirebaseHelper.getMyRef().child(context.getString(R.string.db_news))
                        .child(mNewsList.get(position).getNews_id())
                        .child(context.getString(R.string.db_field_likes))
                        .child(usertemp.getLike_id())
                        .removeValue(new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                holder.image_heart.setClickable(true);
                                holder.image_heart_red.setVisibility(View.GONE);
                                holder.image_heart.setVisibility(View.VISIBLE);
                                mNewsList.get(position).getmUserLikes().remove(usertemp);
                                holder.tv_likes_count.setTextColor(ContextCompat.getColor(context, R.color.black));
                                holder.tv_likes_count.setText(String.format("%d",
                                        mNewsList.get(position).getmUserLikes().size()));

                                StringBuilder mUsersName = new StringBuilder();
                                for (Like like :
                                        mNewsList.get(position).getmUserLikes()) {
                                    mUsersName.append(like.getUsername());
                                    mUsersName.append(",");
                                }
                                holder.image_likes.setText(getLikesString(mUsersName));
                            }
                        });

            }
        });

        holder.image_heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //database access and if complete then toggle normal heart to red
                holder.image_heart_red.setClickable(false);
                holder.image_heart.setClickable(false);
                String keyId = mFirebaseHelper.getMyRef().push().getKey();
                User user = sharedPreferences.getUserInfo();
                final Like like = new Like(user.getUser_id(),
                        user.getUsername(),
                        keyId
                );

                mFirebaseHelper.getMyRef().child(context.getString(R.string.db_news))
                        .child(mNewsList.get(position).getNews_id())
                        .child(context.getString(R.string.db_field_likes))
                        .child(keyId)
                        .setValue(like, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                mNewsList.get(position).getmUserLikes().add(like);
                                holder.image_heart_red.setVisibility(View.VISIBLE);
                                holder.image_heart.setVisibility(View.GONE);
                                holder.tv_likes_count.setTextColor(ContextCompat.getColor(context, R.color.red_normal));
                                holder.tv_likes_count.setText(String.format("%d",
                                        mNewsList.get(position).getmUserLikes().size()));

                                StringBuilder mUsersName = new StringBuilder();
                                for (Like like :
                                        mNewsList.get(position).getmUserLikes()) {
                                    mUsersName.append(like.getUsername());
                                    mUsersName.append(",");
                                }
                                holder.image_likes.setText(getLikesString(mUsersName));

                                holder.image_heart_red.setClickable(true);

                            }
                        });

            }
        });

    }

    private String getLikesString(StringBuilder mUsersName) {
        if (mUsersName.toString().isEmpty()) {
            return context.getString(R.string.no_comment_sample);
        }
        String[] splitUsers = mUsersName.toString().split(",");
        String mLikesString = "";
        int length = splitUsers.length;
        if (length == 0) {
            mLikesString = context.getString(R.string.no_comment_sample);
        } else if (length == 1) {
            mLikesString = "Liked by " + splitUsers[0];
        } else if (length == 2) {
            mLikesString = "Liked by " + splitUsers[0]
                    + " and " + splitUsers[1];
        } else if (length == 3) {
            mLikesString = "Liked by " + splitUsers[0]
                    + ", " + splitUsers[1]
                    + " and " + splitUsers[2];

        } else if (length == 4) {
            mLikesString = "Liked by " + splitUsers[0]
                    + ", " + splitUsers[1]
                    + ", " + splitUsers[2]
                    + " and " + splitUsers[3];
        } else if (length > 4) {
            mLikesString = "Liked by " + splitUsers[0]
                    + ", " + splitUsers[1]
                    + ", " + splitUsers[2]
                    + " and " + (splitUsers.length - 3) + " others";
        }
        return mLikesString;
    }

    @Override
    public int getItemCount() {
        if (mNewsList != null) {
            return mNewsList.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout relNewsDesc, relComments;

        TextView tv_news_title, tv_news_desc, tv_notice_date, tv_notice_desc;
        AppCompatImageView post_image, speech_bubble;
        TextView tv_likes_count, tv_comment_count;
        AppCompatImageView btn_more, image_heart_red, image_heart;
        TextView image_likes, image_comments_link, image_time_posted;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);

            btn_more = itemView.findViewById(R.id.btn_more);
            tv_news_title = itemView.findViewById(R.id.event_title);

            relNewsDesc = itemView.findViewById(R.id.relLayoutNewsDesc);
            relComments = itemView.findViewById(R.id.relComments);

            post_image = itemView.findViewById(R.id.post_image);
            tv_news_desc = itemView.findViewById(R.id.news_desc);

            image_heart_red = itemView.findViewById(R.id.image_heart_red);
            image_heart = itemView.findViewById(R.id.image_heart);
            tv_likes_count = itemView.findViewById(R.id.tv_likes_count);
            speech_bubble = itemView.findViewById(R.id.speech_bubble);
            tv_comment_count = itemView.findViewById(R.id.tv_comment_count);

            image_likes = itemView.findViewById(R.id.image_likes);
            image_comments_link = itemView.findViewById(R.id.image_comments_link);
            image_time_posted = itemView.findViewById(R.id.image_time_posted);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onNewsClickListener.onNewsClickListener(mNewsList.get(getAdapterPosition()));
                }
            });

            if (sharedPreferences.getUserType() == UserTypes.ADMIN) {
                btn_more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopupMenu popup = new PopupMenu(btn_more.getContext(), btn_more);
                        popup.inflate(R.menu.news_options_menu);
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                News object = mNewsList.get(getAdapterPosition());
                                itemSelectedListener.onMenuAction(object, item);

                                return false;
                            }
                        });
                        // here you can inflate your menu

                        popup.show();
                    }
                });

            } else {
                btn_more.setVisibility(View.GONE);
            }
        }


    }

    public interface OnItemSelectedListener {

        void onMenuAction(News news, MenuItem item);
    }
}
