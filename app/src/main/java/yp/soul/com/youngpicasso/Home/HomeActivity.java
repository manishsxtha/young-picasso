package yp.soul.com.youngpicasso.Home;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import yp.soul.com.youngpicasso.ArtSales.ArtSalesFragment;
import yp.soul.com.youngpicasso.Events.EventsFragment;
import yp.soul.com.youngpicasso.Gallery.ArtPostFragment;
import yp.soul.com.youngpicasso.Login.LoginActivity;
import yp.soul.com.youngpicasso.News.NewsFragment;
import yp.soul.com.youngpicasso.Notice.NoticeDetailsFragment;
import yp.soul.com.youngpicasso.Notice.NoticeFragment;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Search.SearchFragment;
import yp.soul.com.youngpicasso.SettingsActivity;
import yp.soul.com.youngpicasso.Utils.UniversalImageLoader;
import yp.soul.com.youngpicasso.Utils.UserTypes;
import yp.soul.com.youngpicasso.helpers.BaseFragment;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.models.User;

import static yp.soul.com.youngpicasso.Utils.AdminUID.ADMIN_UID;
import static yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper.getInstance;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "HomeActivity";

    public static final int NEWS_FRAGMENT = 0;
    public static final int SEARCH_FRAGMENT = 1;
    public static final int NOTICE_FRAGMENT = 2;
    public static final int NOTICE_DETAIL_FRAGMENT = 3;
    public static final int EVENTS_FRAGMENT = 4;
    public static final int ART_GALLERY_FRAGMENT = 5;
    public static final int ART_SALES_FRAGMENT = 6;
    public static final int PROFILE_FRAGMENT = 7;
    private Context mContext = HomeActivity.this;

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private boolean exitApp = false;
    private android.support.v4.app.FragmentTransaction fragmentTransaction;

    //vars
    private List<BaseFragment> mFragments;

    private UniversalImageLoader universalImageLoader;
    private FirebaseHelper mFirebaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setupFirebase();
        checkInternetConnection();
//
//
        initImageLoader();
        setupNavigationDrawer();
        setupFragments();
    }

    private void checkInternetConnection() {
        if (hasInternetConnection()) {
            if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                mFirebaseHelper.getMyRef().child("users")
                        .child(mFirebaseHelper.getAuth().getCurrentUser().getUid())
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    User user = dataSnapshot.getValue(User.class);
                                    mFirebaseHelper.getSharedPreference().saveUserInfo(user);

                                    Log.d(TAG, "onDataChange: user: " +
                                            mFirebaseHelper.getSharedPreference().getUserInfo().getUser_type());
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

            } else {
                Intent intent = new Intent(this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

        } else {
            Toast.makeText(mContext, "You are not connected to the internet.", Toast.LENGTH_SHORT).show();

        }
    }

    public boolean hasInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    /**
     * initialize firebase
     */
    private void setupFirebase() {
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
    }

    /**
     * setup list of fragments in the navigation view
     */
    private void setupFragments() {
        mFragments = new ArrayList<>();
        mFragments.add(new NewsFragment());

        mFragments.add(new SearchFragment());
        mFragments.add(new NoticeFragment());
        mFragments.add(new NoticeDetailsFragment());
        mFragments.add(new EventsFragment());
        mFragments.add(new ArtPostFragment());
        mFragments.add(new ArtSalesFragment());


        addFragments(mFragments.get(NEWS_FRAGMENT));
        getSupportActionBar().setTitle(mFragments.get(NEWS_FRAGMENT).getTitle());
    }

    public void addFragments(BaseFragment fragment) {
        //don't add a fragment of the same type on top of itself.
        BaseFragment currentFragment = getCurrentFragment();
        if (currentFragment != null) {
            if (currentFragment.getClass() == fragment.getClass()) {
                Log.w("Fragment Manager", "Tried to add a fragment of the same type to the backstack. This may be done on purpose in some circumstances but generally should be avoided.");
                return;
            }
        }
        fragmentTransaction = getSupportFragmentManager()
                .beginTransaction();
        fragmentTransaction
                .replace(R.id.container, fragment)
                .addToBackStack(fragment.getTitle())
                .commit();
    }

    private BaseFragment getCurrentFragment() {
        if (mFragments.get(NEWS_FRAGMENT).isVisible() && mFragments.get(NEWS_FRAGMENT) != null) {
            return mFragments.get(NEWS_FRAGMENT);
        } else if (mFragments.get(NOTICE_FRAGMENT).isVisible() && mFragments.get(NOTICE_FRAGMENT) != null) {
            return mFragments.get(NOTICE_FRAGMENT);
        } else if (mFragments.get(SEARCH_FRAGMENT).isVisible() && mFragments.get(SEARCH_FRAGMENT) != null) {
            return mFragments.get(SEARCH_FRAGMENT);
        } else if (mFragments.get(ART_SALES_FRAGMENT).isVisible() && mFragments.get(ART_SALES_FRAGMENT) != null) {
            return mFragments.get(ART_SALES_FRAGMENT);
        } else if (mFragments.get(ART_GALLERY_FRAGMENT).isVisible() && mFragments.get(ART_GALLERY_FRAGMENT) != null) {
            return mFragments.get(ART_GALLERY_FRAGMENT);
        }

        return null;

    }

    private void initImageLoader() {
        universalImageLoader = new UniversalImageLoader(mContext);
        ImageLoader.getInstance().init(universalImageLoader.getConfig());
    }

    private void setupNavigationDrawer() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        User user = getInstance(mContext).getUserInfo();
        Uri uri = Uri.parse(user.getAvatar_img_link());
        View header = navigationView.getHeaderView(0);
        CircleImageView profileImage = header.findViewById(R.id.nav_profile_image);
        UniversalImageLoader.setImage(uri.toString(), profileImage, null, "");
        TextView tvUsername = header.findViewById(R.id.nav_username);
        tvUsername.setText(user.getUsername());

        TextView tvEmail = header.findViewById(R.id.nav_email);
        tvEmail.setText(user.getEmail());

        //admin
        //for showing/hiding the admin navigation drawer menu
        Log.d(TAG, "setupNavigationDrawer: adminid:" + ADMIN_UID);
//        Log.d(TAG, "setupNavigationDrawer: current user id:" + mFirebaseHelper.getAuth().getCurrentUser().getUid());
        if (user.getUser_type() == UserTypes.ADMIN) {
            Toast.makeText(this, "admin mode", Toast.LENGTH_SHORT).show();
//            navigationView.getMenu().findItem(R.id.nav_item_admin).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_admin_news).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_admin_events).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_search_users).setVisible(true);

        } else {
//            navigationView.getMenu().findItem(R.id.nav_item_admin).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_admin_news).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_admin_events).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_search_users).setVisible(false);
        }

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                int index = getSupportFragmentManager().getBackStackEntryCount() - 1;
                FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
                String tag = backEntry.getName();


                if (tag == "Notice") {
                    toggle.setDrawerIndicatorEnabled(false);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);// show back button
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                        }
                    });
                }


//                else if{
//
//                }
//
                else {
                    //show hamburger
                    toggle.setDrawerIndicatorEnabled(true);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    toggle.syncState();
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            drawer.openDrawer(GravityCompat.START);
                        }
                    });
                }
            }
        });

    }

    /**
     * check login of user
     */
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        updateUI(FirebaseAuth.getInstance().getCurrentUser());
    }

    private void updateUI(FirebaseUser currentUser) {
        if (currentUser != null) {
//            Snackbar.make(findViewById(R.id.main_layout), "Welcome " + currentUser.getDisplayName(), Snackbar.LENGTH_SHORT).show();
        } else {
            Log.d(TAG, "updateUI: User not signed in!");
            Snackbar.make(findViewById(R.id.main_layout), "You are not signed in.", Snackbar.LENGTH_SHORT).show();
            startActivity(new Intent(mContext, LoginActivity.class));
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            startActivity(new Intent(mContext, SettingsActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_search_users) {
            //don't add a fragment of the same type on top of itself.
            addFragments(mFragments.get(SEARCH_FRAGMENT));

        } else if (id == R.id.nav_notices
                ) {
            //don't add a fragment of the same type on top of itself.
            addFragments(mFragments.get(NOTICE_FRAGMENT));
        } else if (id == R.id.nav_logout) {
            popUpForLogOut();
        } else if (id == R.id.nav_feedback) {

            try {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + "manish.xshrestha@gmail.com"));
                intent.putExtra(Intent.EXTRA_SUBJECT, "");
                intent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(mContext, "Mail not found", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_rating) {
            Toast.makeText(mContext, "Navigating to the app store..", Toast.LENGTH_SHORT).show();
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        }
//        admin
        else if (id == R.id.nav_admin_news) {

            addFragments(mFragments.get(NEWS_FRAGMENT));
        } else if (id == R.id.nav_admin_events) {
            addFragments(mFragments.get(EVENTS_FRAGMENT));
//            startActivity(new Intent(this, NewStoryActivity.class));
        } else if (id == R.id.nav_admin_events) {
            addFragments(mFragments.get(EVENTS_FRAGMENT));
//            startActivity(new Intent(this, NewStoryActivity.class));
        } else if (id == R.id.nav_art_posts) {
            addFragments(mFragments.get(ART_GALLERY_FRAGMENT));
//            startActivity(new Intent(this, NewStoryActivity.class));
        } else if (id == R.id.nav_art_sales) {
            addFragments(mFragments.get(ART_SALES_FRAGMENT));
//            startActivity(new Intent(this, NewStoryActivity.class));
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * user presses log out btn on navigation drawer
     */
    private void popUpForLogOut() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialog);
        builder.setTitle("Log out?");

        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mFirebaseHelper.getSharedPreference().saveUserInfo(new User());
                mFirebaseHelper.signOut();
                dialog.cancel();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    /**
     * setting the double press back button during 2 sec to exit the app
     * the first fragment is set to the activity on start
     */
    @Override
    public void onBackPressed() {

        int fragments = getSupportFragmentManager().getBackStackEntryCount();
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (fragments == 1) {
                if (exitApp) {
                    finish();
                }
                Toast.makeText(this, "Press back again to exit.", Toast.LENGTH_SHORT).show();
                exitApp = true;
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        exitApp = false;
                    }
                }, 2000);


            } else {
                if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                    getSupportFragmentManager().popBackStack();
                } else {
                    super.onBackPressed();
                }
            }
        }
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
