package yp.soul.com.youngpicasso.Events.Detail;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amar.library.ui.StickyScrollView;
import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.api.defaults.DefaultBootstrapBrand;
import com.github.thunder413.datetimeutils.DateTimeUnits;
import com.github.thunder413.datetimeutils.DateTimeUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import yp.soul.com.youngpicasso.Comments.CommentsRecyclerAdapter;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.TextDataUtils;
import yp.soul.com.youngpicasso.Utils.UniversalImageLoader;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.helpers.OnCommentRemoveListener;
import yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper;
import yp.soul.com.youngpicasso.models.Comment;
import yp.soul.com.youngpicasso.models.CommentUserMerge;
import yp.soul.com.youngpicasso.models.Event;
import yp.soul.com.youngpicasso.models.Like;
import yp.soul.com.youngpicasso.models.User;

import static yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper.getInstance;

public class EventDetailActivity extends AppCompatActivity implements OnCommentRemoveListener {
    private static final String TAG = "EventDetailActivity";
    //vars
    List<Like> mLikeList;
    List<CommentUserMerge> mCommentList;
    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    private CommentsRecyclerAdapter adapter;
    private ArrayList<Comment> comments;
    private Comment comment;
    private Button ivCommentSend;
    private EditText etCommentText;
    private RelativeLayout relComments;
    private AlertDialog.Builder commentBuilder;
    private TextView event_desc;
    private TextView event_title, event_date;
    private ImageView post_image, speech_bubble;
    private TextView tv_likes_count, tv_comment_count;
    private ImageView btn_more, image_heart_red, image_heart;
    private TextView image_likes;
    private BootstrapButton event_days_left;
    private Event event;
    private FirebaseHelper mFirebaseHelper;
    private Context mContext = EventDetailActivity.this;
    private SharedPreferenceHelper sharedPreferences;
    private StickyScrollView mScrollView;
    private AppBarLayout appBar;

    @Override
    public void onCommentRemoveListener(final CommentUserMerge comment) {


        commentBuilder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                CommentUserMerge cm = mCommentsList.get(postion);
//                Comment comment = new Comment(cm.getComment_id(),
//                        cm.getComment_desc(),
//                        cm.getDate_created(),
//                        cm.getUser_id(),
//                        cm.getUsername());
                dialog.cancel();
                mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_events))
                        .child(event.getEvent_id())
                        .child("comment_list")
                        .child(comment.getComment_id())
                        .removeValue(new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
                                mCommentList.remove(comment);
                                adapter.notifyDataSetChanged();
                            }
                        });

            }
        });
        commentBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        commentBuilder.show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);

        getDataFromIntent();

        setupWidgets();
        setupFirebase();
        setupAdapter();
        setupCommentsList();
//        tempData();

        initSendComment();

        setupToolbarAndStickyLayout();

        init();

    }

    private void initSendComment() {
        ivCommentSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked add comment btn");
                String textComment = etCommentText.getText().toString();
                if (!textComment.equals("")) {
                    String keyId = mFirebaseHelper.getMyRef().push().getKey();
                    final Comment comment = new Comment(
                            keyId,
                            textComment,
                            mFirebaseHelper.getAuth().getCurrentUser().getUid(),
                            mFirebaseHelper.getSharedPreference().getUserInfo().getUsername()
                    );
                    Toast.makeText(mContext, "sending your comment..", Toast.LENGTH_SHORT).show();
                    mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_events))
                            .child(event.getEvent_id())
                            .child("comment_list")
                            .child(keyId)
                            .setValue(comment, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {


                                    mCommentList.add(getCommentUserMergeModel(comment, ""));
                                    adapter.notifyDataSetChanged();
                                    mScrollView.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            mScrollView.fullScroll(NestedScrollView.FOCUS_DOWN);
                                        }
                                    }, 200);
                                }
                            });
                    etCommentText.setText("");
                }

                closeKeyboard();
            }
        });
    }

    private void tempData() {
        CommentUserMerge userMerge = new CommentUserMerge(
                "1",
                "This is a comment",
                "1D",
                "1",
                "babu",
                "https://lh5.googleusercontent.com/-q4lp0Z5FV5U/AAAAAAAAAAI/AAAAAAAAABU/zoqmTuUeL1M/s96-c/photo.jpg"

        );

        CommentUserMerge userMerge1 = new CommentUserMerge(
                "1",
                "This is a comment",
                "1D",
                "1",
                "babu",
                "https://lh5.googleusercontent.com/-q4lp0Z5FV5U/AAAAAAAAAAI/AAAAAAAAABU/zoqmTuUeL1M/s96-c/photo.jpg"

        );

        CommentUserMerge userMerge2 = new CommentUserMerge(
                "1",
                "This is a comment",
                "1D",
                "1",
                "babu",
                "https://lh5.googleusercontent.com/-q4lp0Z5FV5U/AAAAAAAAAAI/AAAAAAAAABU/zoqmTuUeL1M/s96-c/photo.jpg"

        );

        mCommentList.add(userMerge);
        mCommentList.add(userMerge1);
        mCommentList.add(userMerge2);

        adapter.notifyDataSetChanged();
    }

    private void setupCommentsList() {

        for (final Comment comment :
                event.getComment_list()) {
            mFirebaseHelper.getMyRef().child("users")
                    .orderByChild("user_id")
                    .equalTo(comment.getUser_id())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (DataSnapshot s :
                                    dataSnapshot.getChildren()) {

                                mCommentList.add(getCommentUserMergeModel(comment,
                                        s.getValue(User.class).getAvatar_img_link()
                                ));
                            }
                            adapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
        }


    }

    private CommentUserMerge getCommentUserMergeModel(Comment comment, String userImage) {
        if (userImage.isEmpty()) {
            userImage = sharedPreferences.getUserInfo().getAvatar_img_link();
        }
        CommentUserMerge userMerge = new CommentUserMerge(
                comment.getComment_id(),
                comment.getComment_desc(),
                TextDataUtils.getTimestampDifference2(comment.getDate_created()),
                comment.getUser_id(),
                comment.getUsername(),
                userImage

        );
        return userMerge;
    }

    private void setupToolbarAndStickyLayout() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(event.getEvent_title());

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        appBar = (AppBarLayout) findViewById(R.id.app_bar);
//        collapsingToolbar = findViewById(R.id.toolbar_layout);
//        collapsingToolbar.setTitle(event.getEvent_title()());
//
//        View anchor = findViewById(R.id.anchor);
//        RelativeLayout button = findViewById(R.id.rel_add_likes_layout);
//        ((CoordinatorLayout.LayoutParams) button.getLayoutParams()).setBehavior(
//                new StickyBottomBehavior(R.id.anchor, getResources().getDimensionPixelOffset(R.dimen.margins)));
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        event = intent.getParcelableExtra(getString(R.string.calling_event_intent));
        if (event == null) {
            finish();
        }
    }

    /**
     * for saving the state of the activity on rotation changes
     *
     * @param outState
     */
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntArray("ARTICLE_SCROLL_POSITION",
                new int[]{mScrollView.getScrollX(), mScrollView.getScrollY()});
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final int[] position = savedInstanceState.getIntArray("ARTICLE_SCROLL_POSITION");
        if (position != null)
            appBar.setExpanded(false);
        mScrollView.post(new Runnable() {
            public void run() {
                mScrollView.scrollTo(position[0], position[1]);
            }
        });
    }

    private void setupFirebase() {
        mFirebaseHelper = new FirebaseHelper(mContext);
    }

    private void init() {
        sharedPreferences = getInstance(mContext);
        UniversalImageLoader.setImage(event.getEvent_picture(),
                post_image, null, "");
        event_title.setText(event.getEvent_title());

        event_desc.setText(event.getEvent_description());


        prepareLikes();
        prepareComments();
        prepareDate();

        //press comments link
        relComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mScrollView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mScrollView.fullScroll(NestedScrollView.FOCUS_DOWN);
                    }
                }, 200);
            }
        });


    }

    private void prepareDate() {
        DateTimeUtils.setTimeZone("Asia/Kathmandu");
        int dateDiff = Math.abs(DateTimeUtils.getDateDiff(event.getEvent_start_date(),
                event.getEvent_end_date(), DateTimeUnits.DAYS));
        int diff;
        if (dateDiff == 0) {
            diff = 1;
        } else {
            diff = dateDiff + 1;
        }
        String duration_in_date = String.format("Event Start date:\n" +
                        " %s \n" +
                        "Event End date:\n" +
                        " %s\n" +
                        "Start time: %s\n" +
                        "End time: %s\n" +
                        "For %s day(s)",
                event.getEvent_start_date(),
                event.getEvent_end_date(),
                event.getStart_time(),
                event.getEnd_time(),
                diff);

        event_date.setText(duration_in_date);

        String str_event_days_left = TextDataUtils.getTimestampDifference2(event.getEvent_end_date());

        if (str_event_days_left.equals("TODAY")) {

            event_days_left.setBootstrapBrand(DefaultBootstrapBrand.DANGER);
            event_days_left.setText(str_event_days_left);
        } else if (str_event_days_left.contains("AGO")) {
            event_days_left.setBootstrapBrand(DefaultBootstrapBrand.INFO);
            event_days_left.setText(str_event_days_left);
        } else {

            event_days_left.setBootstrapBrand(DefaultBootstrapBrand.REGULAR);
            event_days_left.setText(str_event_days_left);
        }
    }

    private void prepareLikes() {
        //check likes and prepare likes string
        Boolean mLikedByCurrentUser = false;
        StringBuilder mUsersName = new StringBuilder();
        mLikeList = event.getlikeList();
        //check if there are no likes for that event
        if (mLikeList.size() != 0) {

            tv_likes_count.setText(String.format("%d", event.getlikeList().size()));
            for (Like like :
                    event.getlikeList()) {
                mUsersName.append(like.getUsername());
                mUsersName.append(",");
                if (mFirebaseHelper.getAuth().getCurrentUser().getUid().equals(like.getUser_id())) {
                    mLikedByCurrentUser = true;
                }
            }
            if (mLikedByCurrentUser) {
                image_heart_red.setVisibility(View.VISIBLE);
                image_heart.setVisibility(View.GONE);
                tv_likes_count.setTextColor(ContextCompat.getColor(mContext, R.color.red_normal));
            } else {
                image_heart_red.setVisibility(View.GONE);
                image_heart.setVisibility(View.VISIBLE);
                tv_likes_count.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            }

            image_likes.setText(getLikesString(mUsersName));
        } else {
            mLikeList = new ArrayList<>();
            tv_likes_count.setText("0");
            image_heart_red.setVisibility(View.GONE);
            image_heart.setVisibility(View.VISIBLE);

            image_likes.setText("No one has liked it yet.");
        }


        // handle likes on click events

        image_heart_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //database access and if complete then toggle red heart to normal
                image_heart_red.setClickable(false);
                image_heart.setClickable(false);

                Like userLike = new Like();
                for (Like like :
                        mLikeList) {
                    if (like.getUser_id().equals(mFirebaseHelper.getAuth().getCurrentUser().getUid())) {
                        userLike = like;
                        break;
                    }
                }
                final Like usertemp = userLike;
                mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_events))
                        .child(event.getEvent_id())
                        .child(mContext.getString(R.string.db_field_likes))
                        .child(usertemp.getLike_id())
                        .removeValue(new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                image_heart.setClickable(true);
                                image_heart_red.setVisibility(View.GONE);
                                image_heart.setVisibility(View.VISIBLE);
                                event.getlikeList().remove(usertemp);
                                tv_likes_count.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                                tv_likes_count.setText(String.format("%d",
                                        event.getlikeList().size()));

                                StringBuilder mUsersName = new StringBuilder();
                                for (Like like :
                                        event.getlikeList()) {
                                    mUsersName.append(like.getUsername());
                                    mUsersName.append(",");
                                }
                                image_likes.setText(getLikesString(mUsersName));
                            }
                        });

            }
        });

        image_heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //database access and if complete then toggle normal heart to red
                image_heart_red.setClickable(false);
                image_heart.setClickable(false);
                String keyId = mFirebaseHelper.getMyRef().push().getKey();
                User user = sharedPreferences.getUserInfo();
                final Like like = new Like(user.getUser_id(),
                        user.getUsername(),
                        keyId
                );

                mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_events))
                        .child(event.getEvent_id())
                        .child(mContext.getString(R.string.db_field_likes))
                        .child(keyId)
                        .setValue(like, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                event.getlikeList().add(like);
                                image_heart_red.setVisibility(View.VISIBLE);
                                image_heart.setVisibility(View.GONE);
                                tv_likes_count.setTextColor(ContextCompat.getColor(mContext, R.color.red_normal));
                                tv_likes_count.setText(String.format("%d",
                                        event.getlikeList().size()));

                                StringBuilder mUsersName = new StringBuilder();
                                for (Like like :
                                        event.getlikeList()) {
                                    mUsersName.append(like.getUsername());
                                    mUsersName.append(",");
                                }
                                image_likes.setText(getLikesString(mUsersName));

                                image_heart_red.setClickable(true);

                            }
                        });

            }
        });

    }

    private String getLikesString(StringBuilder mUsersName) {
        if (mUsersName.toString().isEmpty()) {
            return mContext.getString(R.string.no_event_commnent_sample);
        }
        String[] splitUsers = mUsersName.toString().split(",");
        String mLikesString = "";
        int length = splitUsers.length;
        if (length == 0) {
            mLikesString = mContext.getString(R.string.no_event_commnent_sample);
        } else if (length == 1) {
            if (splitUsers[0].equals(mFirebaseHelper.getSharedPreference().getUserInfo().getUsername())) {

                mLikesString = "You are interested in this event";
            } else {
                mLikesString = splitUsers[0] + " is interested in this event";

            }
        } else if (length == 2) {
            mLikesString = splitUsers[0]
                    + " and " + splitUsers[1] + " are interested in this event";
        } else if (length == 3) {
            mLikesString = splitUsers[0]
                    + ", " + splitUsers[1]
                    + " and " + splitUsers[2] + " are interested in this event";

        } else if (length == 4) {
            mLikesString = splitUsers[0]
                    + ", " + splitUsers[1]
                    + ", " + splitUsers[2]
                    + " and " + splitUsers[3] + " are interested in this event";
        } else if (length > 4) {
            mLikesString = "Liked by " + splitUsers[0]
                    + ", " + splitUsers[1]
                    + ", " + splitUsers[2]
                    + " and " + (splitUsers.length - 3) + " others are interested in this event";
        }
        return mLikesString;
    }

    private void prepareComments() {
        //prepare comments count
        List<Comment> comment = event.getComment_list();

        if (comment.size() != 0) {
            int comment_size = comment.size();
            tv_comment_count.setText(String.valueOf(comment_size));

        } else {
            tv_comment_count.setText("0");

        }

    }

    private void setupWidgets() {

        commentBuilder = new AlertDialog.Builder(mContext);
        commentBuilder.setTitle("Delete comment?");

        mScrollView = (StickyScrollView) findViewById(R.id.scrollView);
        appBar = findViewById(R.id.app_bar);

        btn_more = findViewById(R.id.btn_more);
        event_title = findViewById(R.id.event_title);
        event_desc = findViewById(R.id.event_desc);
        post_image = findViewById(R.id.post_image);


        image_heart_red = findViewById(R.id.image_heart_red);
        image_heart = findViewById(R.id.image_heart);
        tv_likes_count = findViewById(R.id.tv_likes_count);
        speech_bubble = findViewById(R.id.speech_bubble);
        tv_comment_count = findViewById(R.id.tv_comment_count);

        image_likes = findViewById(R.id.image_likes);
        event_date = findViewById(R.id.event_date);
        event_days_left = findViewById(R.id.event_days_left);

        image_heart_red = findViewById(R.id.image_heart_red);
        image_heart = findViewById(R.id.image_heart);
        tv_likes_count = findViewById(R.id.tv_likes_count);

        relComments = findViewById(R.id.relComment);
        tv_comment_count = findViewById(R.id.tv_comment_count);

        image_likes = findViewById(R.id.image_likes);

        ivCommentSend = (Button) findViewById(R.id.comment_send);
        etCommentText = findViewById(R.id.et_comment_text);
    }

    private void setupAdapter() {
        mCommentList = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerView);
        manager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, true);
        recyclerView.setNestedScrollingEnabled(false);

        recyclerView.setLayoutManager(manager);

        adapter = new CommentsRecyclerAdapter(mContext, mCommentList, this);

        recyclerView.setAdapter(adapter);


    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
