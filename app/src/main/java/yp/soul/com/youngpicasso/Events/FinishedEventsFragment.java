package yp.soul.com.youngpicasso.Events;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;
import yp.soul.com.youngpicasso.Events.Detail.EventDetailActivity;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.helpers.BaseFragment;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.helpers.OnEventClickListener;
import yp.soul.com.youngpicasso.models.Comment;
import yp.soul.com.youngpicasso.models.Event;
import yp.soul.com.youngpicasso.models.Like;

public class FinishedEventsFragment extends BaseFragment implements OnEventClickListener, EventsRecyclerAdapter.OnItemSelectedListener {
    private View view;
    private List<Event> mEventsList;

    private static final String TAG = "FinishedEventsFragment";
    private ValueEventListener eventListener;
    private Context mContext;
    private RecyclerView recyclerView;
    private WaveSwipeRefreshLayout refreshLayout;
    private LinearLayoutManager manager;
    private FirebaseHelper mFirebaseHelper;
    private EventsRecyclerAdapter adapter;

    public static FinishedEventsFragment newInstance() {
        FinishedEventsFragment fragment = new FinishedEventsFragment();

        return fragment;
    }

    @Override
    public void onMenuAction(Event event, MenuItem item) {
        Log.e(TAG, "onMenuAction: news:" + event);
        switch (item.getItemId()) {
            case R.id.menu_edit:
                Intent intent = new Intent(mContext, EventAddActivity.class);
                intent.putExtra(mContext.getString(R.string.calling_event_intent), event);
                startActivity(intent);
                break;
            case R.id.menu_delete:
                popupForDelete(event);


//                setupObjectList();
                break;
        }
    }

    @Override
    public void onEventClickListener(Event event) {
        Intent intent = new Intent(mContext, EventDetailActivity.class);
        intent.putExtra(mContext.getString(R.string.calling_event_intent), event);
        startActivity(intent);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recycler_view_container, container, false);
        mContext = getContext();

        setupFirebase();
        setupWidgets();
        setupAdapter();
        setupEventsList();

        return view;
    }

    private void setupWidgets() {

        refreshLayout = view.findViewById(R.id.refresh);
        refreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);
        refreshLayout.setWaveColor(Color.argb(100, 255, 0, 0));

        refreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Do work to refresh the list here.

                mEventsList.clear();
                setupEventsList();
                refreshLayout.setWaveColor(0xFF000000 + new Random().nextInt(0xFFFFFF));
            }
        });
    }

    private void popupForDelete(final Event event) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialog);
        builder.setTitle("Delete news " + event.getEvent_title() + " ?");
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mFirebaseHelper.getMyRef().child("events")
                        .child(event.getEvent_id())
                        .removeValue(new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                mEventsList.remove(event);
                                adapter.notifyDataSetChanged();
                                Toast.makeText(mContext, "Deleted news: " + event.getEvent_title(), Toast.LENGTH_SHORT).show();
                            }
                        });

                dialog.cancel();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void setupEventsList() {
//         Event event = new Event(
//                "1",
//                "username",
//                "id",
//                "title",
//                "desc",
//                "pic"
//        );
//        List<Comment> comments = new ArrayList<>();
//        List<Like> likes = new ArrayList<>();
//        event.setComment_list(comments);
//        event.setlikeList(likes);
//        mEventsList.add(event);
//        mEventsList.add(event);
        refreshLayout.setRefreshing(true);
        eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mEventsList.clear();
                for (DataSnapshot ds :
                        dataSnapshot.getChildren()) {
                    Event event = new Event();
                    Map<String, Object> objectMap = (HashMap<String, Object>) ds.getValue();

                    if (!isEventGone(objectMap.get("event_end_date").toString())) continue;

                    event.setUser_id(objectMap.get("user_id").toString());
                    event.setUsername(objectMap.get("username").toString());
                    event.setEvent_id(objectMap.get("event_id").toString());
                    event.setEvent_title(objectMap.get("event_title").toString());
                    event.setEvent_description(objectMap.get("event_description").toString());
                    event.setEvent_picture(objectMap.get("event_picture").toString());
                    event.setEvent_start_date(objectMap.get("event_start_date").toString());
                    event.setEvent_end_date(objectMap.get("event_end_date").toString());
                    event.setStart_time(objectMap.get("start_time").toString());
                    event.setEnd_time(objectMap.get("end_time").toString());

                    List<Comment> comments = new ArrayList<>();
                    for (DataSnapshot sss :
                            ds.child("comment_list").getChildren()) {
                        Comment comment = new Comment();
                        Map<String, Object> objectMap1 = (HashMap<String, Object>) sss.getValue();
                        comment.setComment_id(objectMap1.get("comment_id").toString());
                        comment.setComment_desc(objectMap1.get("comment_desc").toString());
                        comment.setDate_created(objectMap1.get("date_created").toString());
                        comment.setUser_id(objectMap1.get("user_id").toString());
                        comment.setUsername(objectMap1.get("username").toString());

                        comments.add(comment);

                    }
                    event.setComment_list(comments);

                    List<Like> likes = new ArrayList<>();
                    for (DataSnapshot likesSnap :
                            ds.child("mUserLikes").getChildren()) {
                        Like like = new Like();
                        Map<String, Object> objectMap1 = (HashMap<String, Object>) likesSnap.getValue();
                        like.setUser_id(objectMap1.get("user_id").toString());
                        like.setLike_id(objectMap1.get("like_id").toString());
                        like.setUsername(objectMap1.get("username").toString());

                        likes.add(like);

                    }
                    event.setlikeList(likes);
                    mEventsList.add(event);


                }

                Collections.sort(mEventsList, new Comparator<Event>() {
                    @Override
                    public int compare(Event o1, Event o2) {
                        return o2.getEvent_start_date().compareTo(o1.getEvent_start_date());
                    }
                });
                adapter.notifyDataSetChanged();
//                manager.scrollToPosition(mEventsList.size() - 1);
                if (refreshLayout.isRefreshing()) {

                    refreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_events))
                .addListenerForSingleValueEvent(eventListener);

    }

    private boolean isEventGone(String event_end_date) {

        int difference;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",
                Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kathmandu"));
        Date today = c.getTime();
        sdf.format(today);
        Date timestamp;

        try {
            timestamp = sdf.parse(event_end_date);
            difference = (int) (((today.getTime() - timestamp.getTime())) / 1000 / 60 / 60 / 24);
            if (difference > 0) {
                return true;
            } else return false;
        } catch (ParseException e) {
            Log.e(TAG, "getTimestampDifference: ParseException: " + e.getMessage());
            return false;
        }
    }

    private void setupAdapter() {
        mEventsList = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
        manager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);


        recyclerView.setLayoutManager(manager);

        adapter = new EventsRecyclerAdapter(mContext, mEventsList, this, this);

        recyclerView.setAdapter(adapter);


    }


    private void setupFirebase() {
        mFirebaseHelper = new FirebaseHelper(mContext);
    }

    @Override
    public String getTitle() {
        return "Upcoming Events";
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        mEventsList.clear();
    }
}
