package yp.soul.com.youngpicasso.Events;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TimePicker;
import android.widget.Toast;

import com.github.thunder413.datetimeutils.DateTimeUtils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.FilePaths;
import yp.soul.com.youngpicasso.Utils.ImageManager;
import yp.soul.com.youngpicasso.Utils.Permissions;
import yp.soul.com.youngpicasso.Utils.TextDataUtils;
import yp.soul.com.youngpicasso.Utils.UniversalImageLoader;
import yp.soul.com.youngpicasso.Utils.VerifyPermissions;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.models.Event;

public class EventAddActivity extends AppCompatActivity {
    public static final String TEMP_IMAGE = "";
    private static final String TAG = "EventAddActivity";
    private static final String EVENT_TITLE = "Add Events";

    private static final int STARTDATEPICKER = 1;
    private static final int ENDDATEPICKER = 2;
    private static final int STARTTIMEPICKER = 3;
    private static final int ENDTIMEPICKER = 4;

    private Context mContext = EventAddActivity.this;
    private EditText inputEventTitle, inputEventDesc;
    private CircleImageView EventImage;

    private Button btnStartDatePicker, btnEndDatePicker, btnStartTimePicker, btnEndTimePicker;
    private SwitchCompat switch_new_notes_notify;
    private AppCompatButton btnEventImage, btnImageClear, btn_send;

    private RelativeLayout relImageSelection;
    private View dark_background;
    private ProgressBar progressBar;
    private boolean hasImage = false;
    private String event_title, event_desc, event_picture;
    private FirebaseHelper mFirebaseHelper;
    private int mPhotoUploadProgress = 0;

    private boolean isEditEvent = false;
    private Event event;
    private String keyId;

    //date picker dialog
    private DatePickerDialog datePickerDialog1;
    private DatePickerDialog datePickerDialog2;
    private TimePickerDialog timePickerDialog1;
    private TimePickerDialog timePickerDialog2;


    private String event_end_date, event_start_date, startTime, endTime;

    //current date
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private int tempYear;
    private int tempMonth;
    private int tempDay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_add);

        setupFirebase();
        setupToolbar();
        setupWidgets();

        checkCallingIntent();
        initImageSwitch();

        initGetImageFromGallery();
        initPostEvent();

        initDateDuration();

    }

    private void checkCallingIntent() {
        Intent in = getIntent();
        if (in.hasExtra(mContext.getString(R.string.calling_event_intent))) {

            event = in.getParcelableExtra(mContext.getString(R.string.calling_event_intent));
            inputEventTitle.setText(event.getEvent_title());
            inputEventDesc.setText(event.getEvent_description());

            event_start_date = event.getEvent_start_date();
            event_end_date = event.getEvent_end_date();
            startTime = event.getStart_time();
            endTime = event.getEnd_time();

            btnStartDatePicker.setText(event_start_date);
            btnEndDatePicker.setText(event_end_date);
            btnStartTimePicker.setText(startTime);
            btnEndTimePicker.setText(endTime);

            switch_new_notes_notify.setChecked(false);
            relImageSelection.setVisibility(View.VISIBLE);

            UniversalImageLoader.setImage(event.getEvent_picture(), EventImage, null, "");

            isEditEvent = true;
            hasImage = true;
        }
    }

    private void setupFirebase() {
        mFirebaseHelper = new FirebaseHelper(mContext);
    }

    private void initGetImageFromGallery() {
        btnEventImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VerifyPermissions verifyPermissions = new VerifyPermissions(mContext, EventAddActivity.this);
                if (verifyPermissions.checkPermissionsArray(Permissions.PERMISSIONS)) {
                    Log.d(TAG, "onClick: permission granted");
                    Intent intent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 0);
                } else {
                    Log.d(TAG, "onClick: permission not granted, verifying..");
                    verifyPermissions.verifyPermissionsArray(Permissions.PERMISSIONS);
                }

            }
        });

        btnImageClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventImage.setImageDrawable(null);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Uri targetUri = data.getData();
            event_picture = targetUri.toString();
            try {
                Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(targetUri));
                EventImage.setImageBitmap(bitmap);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void initPostEvent() {
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                event_title = inputEventTitle.getText().toString();
                event_desc = inputEventDesc.getText().toString();
                if (validateForm()) {

                    if (hasImage) {
                        dark_background.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.VISIBLE);
                        saveEventPost(event_picture);
                    } else {
                        saveEventPost(TEMP_IMAGE);
                    }
                } else {
                    Toast.makeText(mContext, "Some fields are not set correctly.", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void saveEventPost(final String imageUrl) {
        //check if it is new or old
        if (isEditEvent) {
            keyId = event.getEvent_id();
        } else {
            keyId = mFirebaseHelper.getMyRef().push().getKey();
        }
        //check if contains image is checked if not checked then put temp image url
        if (!hasImage) {
            Event event = new Event(mFirebaseHelper.getAuth().getCurrentUser().getUid(),
                    mFirebaseHelper.getSharedPreference().getUserInfo().getUsername(),
                    keyId, event_title, event_desc, TEMP_IMAGE, event_start_date, event_end_date,
                    startTime, endTime);
            mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_events))
                    .child(keyId)
                    .setValue(event, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                            Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
                            dark_background.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            finish();
                        }
                    });
        } else {


            Bitmap bitmap = ((BitmapDrawable) EventImage.getDrawable()).getBitmap();
            //first upload photo to firebase and get its url
            final StorageReference EventStorage = mFirebaseHelper.getmStorageReference().child(FilePaths.FIREBASE_EVENT_STORAGE
                    + "/photo-" + keyId);

//        Bitmap bitmap = null;
//        try {
//
//            File file = new File(imageUrl);
//            bitmap = new Resizer(mContext)
//                    .setTargetLength(400)
//                    .setSourceImage(file)
//                    .getResizedBitmap();
//        } catch (IOException e) {
//            e.printStackTrace();
//            dark_background.setVisibility(View.GONE);
//            progressBar.setVisibility(View.GONE);
//        }
            byte[] data = ImageManager.getBytesFromBitmap(bitmap, 90);
            UploadTask uploadTask = EventStorage.putBytes(data);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    Toast.makeText(mContext, "Image upload failed", Toast.LENGTH_SHORT).show();
                    dark_background.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                    if (progress - 30 > mPhotoUploadProgress) {
                        Toast.makeText(mContext, "Photo upload progress: " + String.format("%.0f", progress), Toast.LENGTH_SHORT).show();
                    }
                    Log.d(TAG, "onProgress: progress: " + String.format("%.0f", progress) + " % done!");

                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                    // ...
                    Task<Uri> firebaseUrl = EventStorage.getDownloadUrl();
                    while (!firebaseUrl.isSuccessful()) ;

                    Uri downloadUrl = firebaseUrl.getResult();
                    //after uploading photo
                    Event event = new Event(
                            mFirebaseHelper.getSharedPreference().getUserInfo().getUser_id(),
                            mFirebaseHelper.getSharedPreference().getUserInfo().getUsername(),
                            keyId, event_title, event_desc, downloadUrl.toString(),
                            event_start_date, event_end_date,
                            startTime, endTime);
                    mFirebaseHelper.getMyRef().child(mContext.getString(R.string.db_events))
                            .child(keyId)
                            .setValue(event, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                    Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
                                    dark_background.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.GONE);
                                    finish();
                                }
                            });
                }
            });
        }

    }

    private boolean validateForm() {
        boolean valid = true;

        if (TextDataUtils.isEmpty(event_title)) {
            inputEventTitle.setHighlightColor(ContextCompat.getColor(mContext, R.color.red_normal));
            valid = false;
        }


        if (TextDataUtils.isEmpty(event_desc)) {
            inputEventTitle.setBackgroundColor(ContextCompat.getColor(mContext, R.color.red_normal));
            valid = false;
        }
        if (hasImage) {
            if (EventImage.getDrawable() == null) {

                valid = false;
            }
        }

        if (btnStartDatePicker.getText().equals(mContext.getString(R.string.txt_event_start_date))) {
            valid = false;
        }
        if (btnEndDatePicker.getText().equals(mContext.getString(R.string.set_event_end_date))) {
            valid = false;
        }
        if (btnStartTimePicker.getText().equals(mContext.getString(R.string.set_starting_time_of_event))) {
            valid = false;
        }
        if (btnEndTimePicker.getText().equals(mContext.getString(R.string.set_ending_time_of_event))) {
            valid = false;
        }


        return valid;
    }


    private void initImageSwitch() {
        switch_new_notes_notify.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    relImageSelection.setVisibility(View.VISIBLE);
                    hasImage = true;
                } else {
                    relImageSelection.setVisibility(View.GONE);
                    hasImage = false;
                }
            }
        });
    }

    public DatePickerDialog datePickerDialog(int datePickerId) {

        year = TextDataUtils.currentYear();
        month = TextDataUtils.currentMonth();
        day = TextDataUtils.currentDay();


        switch (datePickerId) {
            case STARTDATEPICKER:

                if (datePickerDialog1 == null) {
                    datePickerDialog1 = new DatePickerDialog(this, startDateListener(), year, month, day);
                }
                datePickerDialog1.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                return datePickerDialog1;

            case ENDDATEPICKER:

                if (datePickerDialog2 == null) {
                    datePickerDialog2 = new DatePickerDialog(this, endDateListener(), year, month, day);
                }
                datePickerDialog2.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                return datePickerDialog2;

        }
        return null;
    }

    private TimePickerDialog timePickerDialog(int timePickerId) {
        hour = TextDataUtils.currentHour();
        minute = TextDataUtils.currentMin();
        switch (timePickerId) {
            case STARTTIMEPICKER:
                if (timePickerDialog1 == null) {
                    timePickerDialog1 = new TimePickerDialog(this, startTimeListener(),
                            hour, minute, false);
                }
                return timePickerDialog1;
            case ENDTIMEPICKER:
                if (timePickerDialog2 == null) {
                    timePickerDialog2 = new TimePickerDialog(this, endTimeListener(),
                            hour, minute, false);
                }
                return timePickerDialog2;
        }
        return null;
    }

    private void initDateDuration() {
        btnStartDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog(STARTDATEPICKER).show();
            }
        });

        btnEndDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog(ENDDATEPICKER).show();
            }
        });

        btnStartTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timePickerDialog(STARTTIMEPICKER).show();
            }
        });

        btnEndTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timePickerDialog(ENDTIMEPICKER).show();
            }
        });
    }

    private TimePickerDialog.OnTimeSetListener startTimeListener() {
        return new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {

                startTime = i + " : " + i1;
                btnStartTimePicker.setText(startTime);

            }
        };
    }

    private TimePickerDialog.OnTimeSetListener endTimeListener() {
        return new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {

                endTime = i + " : " + i1;

                btnEndTimePicker.setText(endTime);

            }
        };
    }

    public DatePickerDialog.OnDateSetListener startDateListener() {
        return new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int startYear, int startMonth, int startDay) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",
                        Locale.ENGLISH);
                sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kathmandu"));
                try {
                    String date = "" + startYear + "-" + (startMonth + 1) + "-" + startDay;
                    Date start_date = sdf.parse(date);
                    event_start_date = DateTimeUtils.formatDate(start_date);
                } catch (ParseException e) {
                    e.printStackTrace();
                    event_start_date = "";
                }
                btnStartDatePicker.setText(TextDataUtils.formatDate(startYear, startMonth, startDay));

            }
        };
    }

    public DatePickerDialog.OnDateSetListener endDateListener() {
        return new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int startYear, int startMonth, int startDay) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",
                        Locale.ENGLISH);
                sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kathmandu"));
                try {
                    String date = "" + startYear + "-" + (startMonth + 1) + "-" + startDay;
                    Date start_date = sdf.parse(date);
                    event_end_date = DateTimeUtils.formatDate(start_date);
                } catch (ParseException e) {
                    e.printStackTrace();
                    event_end_date = "";
                }
                btnEndDatePicker.setText(TextDataUtils.formatDate(startYear, startMonth, startDay));

            }
        };
    }
    private void setupWidgets() {
        inputEventTitle = findViewById(R.id.inputEventTitle);
        inputEventDesc = findViewById(R.id.inputEventDesc);

        relImageSelection = findViewById(R.id.relImageSelection);
        switch_new_notes_notify = findViewById(R.id.switch_new_notes_notify);
        btnEventImage = findViewById(R.id.btnEventImage);
        EventImage = findViewById(R.id.EventImage);
        btnImageClear = findViewById(R.id.btnImageClear);

        btn_send = findViewById(R.id.btn_send);
        dark_background = findViewById(R.id.dark_background);
        progressBar = findViewById(R.id.progressBar);

        switch_new_notes_notify.setChecked(false);
        relImageSelection.setVisibility(View.GONE);

        btnStartDatePicker = findViewById(R.id.btnStartDatePicker);
        btnEndDatePicker = findViewById(R.id.btnEndDatePicker);
        btnStartTimePicker = findViewById(R.id.btnStartTimePicker);
        btnEndTimePicker = findViewById(R.id.btnEndTimePicker);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(EVENT_TITLE);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

}
