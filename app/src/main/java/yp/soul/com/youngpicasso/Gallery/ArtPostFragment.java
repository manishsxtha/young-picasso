package yp.soul.com.youngpicasso.Gallery;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beardedhen.androidbootstrap.TypefaceProvider;

import yp.soul.com.youngpicasso.Home.HomeActivity;
import yp.soul.com.youngpicasso.R;
import yp.soul.com.youngpicasso.Utils.UserTypes;
import yp.soul.com.youngpicasso.helpers.BaseFragment;
import yp.soul.com.youngpicasso.helpers.FirebaseHelper;
import yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper;

import static yp.soul.com.youngpicasso.helpers.SharedPreferenceHelper.getInstance;

public class ArtPostFragment extends BaseFragment {

    public static final int FEATURED = 0;
    public static final int STUDENT = 1;
    private static final String TAG = "NewsFragment";
    private static final int ADMIN_FEATURED = 0;

    private View view;
    private Context mContext;

    private LinearLayoutManager manager;

    private FirebaseHelper mFirebaseHelper;
    private FloatingActionButton fab_options;
    private ArtPostPagerAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_art_posts, container, false);
        mContext = getContext();
        TypefaceProvider.registerDefaultIconSets();
        setupToolbar();
        setupViewPager();
        setupFirebase();

//

//        setupNewsList();
//
        setupFAB();

        return view;
    }

    private void setupViewPager() {
        adapter = new ArtPostPagerAdapter(getChildFragmentManager());

        adapter.addFragment(FeaturedArtPostFragment.newInstance()); //index 0
        adapter.addFragment(FreshArtPostFragment.newInstance()); //index 1

        ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager_container);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setText("Featured");
        tabLayout.getTabAt(1).setText("Fresh");

    }


    private void setupToolbar() {
        ((HomeActivity) mContext).getSupportActionBar().setTitle(getTitle());
    }

    private void setupFAB() {
        SharedPreferenceHelper preferenceHelper = getInstance(mContext);

        fab_options = view.findViewById(R.id.fab_filter);


        if ((int) preferenceHelper.getUserInfo().getUser_type() == UserTypes.ADMIN) {

            fab_options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //start the add news activity
                    Intent intent = new Intent(mContext, ArtPostAddActivity.class);
                    intent.putExtra(mContext.getString(R.string.calling_art_post_fab), UserTypes.ADMIN);
                    startActivity(intent);
                }
            });


        } else if ((int) preferenceHelper.getUserInfo().getUser_type() == UserTypes.STUDENT) {
            fab_options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //start the add news activity
                    Intent intent = new Intent(mContext, ArtPostAddActivity.class);
                    intent.putExtra(mContext.getString(R.string.calling_art_post_fab), UserTypes.STUDENT);
                    startActivity(intent);
                }
            });
        } else {
            fab_options.hide();
        }


    }


    private void setupFirebase() {
        mFirebaseHelper = FirebaseHelper.getFirebaseInstance(mContext);
    }


    @Override
    public String getTitle() {
        return "Art Posts";
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }
}
